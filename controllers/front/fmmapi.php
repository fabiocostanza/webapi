<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class WebApiWebApiModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        parent::init();
        $error = array();
        $authenticate = $this->getBearerToken();
        if ($authenticate == null) {
            header($_SERVER['SERVER_PROTOCOL'].' 401 Unauthorized');
            die('401 Unauthorized');
        } elseif ($authenticate) {
            $result = $this->authorization($authenticate);
            if ($result == 0) {
                die('Invalid KEY, 401 Unauthorized');
            } else {
                $class = Tools::getValue('resource');
                $folder_name = Tools::substr($class, 3, 8);
                $class_name = Tools::ucfirst($class);

                $class_name = 'Api'.$class_name;
                $module_controller_dir = _PS_MODULE_DIR_. 'webapi/';
                $module_controller_dir = $module_controller_dir.'classes/'.$folder_name.'/';
                $current_url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

                if (file_exists($module_controller_dir.$class_name.'.php')) {
                    require_once $module_controller_dir.$class_name.'.php';
                    $class_name = Tools::ucwords($class_name);
                    $request = new $class_name($current_url);
                    $response = $request->target();
                    
                    die(Tools::jsonEncode($response));
                } else {
                    $class = Tools::getValue('resource');
                    $folder_name = Tools::substr($class, 3, 7);
                    $class_name = Tools::ucfirst($class);

                    $class_name = 'Api'.$class_name;
                    $module_controller_dir = _PS_MODULE_DIR_. 'webapi/';
                    $module_controller_dir = $module_controller_dir.'classes/'.$folder_name.'/';
                    $current_url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                    if (file_exists($module_controller_dir.$class_name.'.php')) {
                        require_once $module_controller_dir.$class_name.'.php';
                        $class_name = Tools::ucwords($class_name);
                        $request = new $class_name($current_url);
                        $response = $request->target();
                        
                        die(Tools::jsonEncode($response));
                    } else {
                        $error[] = $this->l('Class Not Exists');
                    }
                }
                if ($error) {
                    die(Tools::jsonEncode($error));
                }
            }
        }
        exit;
    }

    private function authorization($authenticate)
    {
        $key = ApiModel::getKeys();
        $user = $authenticate;
        $key_result = 0;
        foreach ($key as $value) {
            if (in_array($user, $value, true)) {
                $key_result = 1;
            }
        }
        return $key_result;
    }

    private function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(
                array_map('ucwords', array_keys($requestHeaders)),
                array_values($requestHeaders)
            );
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    private function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}
