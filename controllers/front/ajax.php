<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class WebApiAjaxModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
    }
    public function init()
    {
        parent::init();
    }
    
    public function displayAjaxUpdateRecord()
    {
        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $valid_token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $token = Tools::getValue('token');
        if ($valid_token != $token) {
            $error = 'Error - token not valid';
            $result = Tools::jsonEncode($error);
            $this->ajaxDie($result);
        }
        $id= Tools::getValue('id');
        $key= Tools::getValue('key');
        $description= Tools::getValue('description');
        $status= Tools::getValue('status');

        if (!$key) {
            $return_msg = $this->l('You Must Fill All Field(s).');
            $return_msg = 0;
            $result = Tools::jsonEncode($return_msg);
        } else {
            if ($id) {
                $result = ApiModel::updateRecord($id, $key, $description, $status);
                if ($result) {
                    $return_msg = $this->l('Update Record!');
                    $return_msg = 2;
                } else {
                    $return_msg = $this->l('There is issue Please Try Later');
                }
            } else {
                $result = ApiModel::insertRecord($key, $description, $status);
                if ($result) {
                    $return_msg = $this->l('New Record Added!');
                    $return_msg = 1;
                } else {
                    $return_msg = $this->l('There is issue Please Try Later');
                }
            }
            
            $result = Tools::jsonEncode($return_msg);
        }

        $this->ajaxDie($result);
    }

    public function displayAjaxDeleteRecord()
    {
        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $valid_token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $token = Tools::getValue('token');
        if ($valid_token != $token) {
            $error = 'Error - token not valid';
            
            $result = Tools::jsonEncode($error);
            $this->ajaxDie($result);
        }
        $id= Tools::getValue('id');
        $result = ApiModel::deleteRecord($id);

        $result = Tools::jsonEncode($result);
        $this->ajaxDie($result);
    }

    public function displayAjaxDelLogoImg()
    {
        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $valid_token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $token = Tools::getValue('token');
        if ($valid_token != $token) {
            $error = 'Error - token not valid';
            $result = Tools::jsonEncode($error);
            $this->ajaxDie($result);
        }
        $result = Configuration::updateValue('WEB_API_LOGO', '');
        $result = Tools::jsonEncode($result);
        $this->ajaxDie($result);
    }

    public function displayAjaxDelGridImg()
    {
        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $valid_token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $token = Tools::getValue('token');
        if ($valid_token != $token) {
            $error = 'Error - token not valid';
            
            $result = Tools::jsonEncode($error);
            $this->ajaxDie($result);
        }
        $img_id= Tools::getValue('img_id');
        if ($img_id == 1) {
            $result = Configuration::updateValue('WEB_API_BANNER_FILE_1', '');
        } elseif ($img_id == 2) {
            $result = Configuration::updateValue('WEB_API_BANNER_FILE_2', '');
        } elseif ($img_id == 3) {
            $result = Configuration::updateValue('WEB_API_BANNER_FILE_3', '');
        } elseif ($img_id == 4) {
            $result = Configuration::updateValue('WEB_API_BANNER_FILE_4', '');
        }
        $result = Tools::jsonEncode($result);
        $this->ajaxDie($result);
    }

    public function displayAjaxUpdateCatTree()
    {
        $cat_slider_status= Tools::getValue('cat_slider_status');
        $category_box = '';
        $category_box = (Tools::getValue('favorite')) ? implode(',', Tools::getValue('favorite')) : '';
        Configuration::updateValue('WEB_API_CAT_BOX', $category_box);
        Configuration::updateValue('WEB_API_CAT_SLIDER_STATUS', $cat_slider_status);

        $result = Tools::jsonEncode(1);
        $this->ajaxDie($result);
    }

    public function displayAjaxSavePosition()
    {
        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $valid_token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $token = Tools::getValue('token');
        if ($valid_token != $token) {
            $error = 'Error - token not valid';
            
            $result = Tools::jsonEncode($error);
            $this->ajaxDie($result);
        }
        $p_one= Tools::getValue('one');
        $p_two= Tools::getValue('two');
        $p_three= Tools::getValue('three');
        $p_four= Tools::getValue('four');
        $p_five= Tools::getValue('five');
        $p_six= Tools::getValue('six');
        $p_seven= Tools::getValue('seven');

        
        Configuration::updateValue('WEB_API_POSITION', $p_one.','.$p_two.','
            .$p_three.','.$p_four.','.$p_five.','.$p_six.','.$p_seven);

        $result = Tools::jsonEncode(1);
        $this->ajaxDie($result);
    }
    
    public function displayAjaxSaveSettings()
    {
        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $valid_token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $token = Tools::getValue('token');
        if ($valid_token != $token) {
            $error = 'Error - token not valid';
            
            $result = Tools::jsonEncode($error);
            $this->ajaxDie($result);
        }
           
        $new_products_status= Tools::getValue('new_products_status');
        $popular_products_status= Tools::getValue('popular_products_status');
        $best_products_status= Tools::getValue('best_products_status');
        $onsale_products_status= Tools::getValue('onsale_products_status');
        $advance_category_slider= Tools::getValue('advance_category_slider');


        $error_log_status= Tools::getValue('error_log_status');
        $slider_status= Tools::getValue('slider_status');
        $logo_status= Tools::getValue('logo_status');
        $grid_banner_status= Tools::getValue('grid_banner_status');
        //$logo_file= Tools::getValue('logo_file');
        $theme_Color= Tools::getValue('theme_Color');

        $cat_slider_status= Tools::getValue('cat_slider_status');
        $category_box = '';
        $category_box = (Tools::getValue('favorite')) ? implode(',', Tools::getValue('favorite')) : '';
        Configuration::updateValue('WEB_API_CAT_BOX', $category_box);
        Configuration::updateValue('WEB_API_CAT_SLIDER_STATUS', $cat_slider_status);

        // $con_logo = Configuration::updateValue('WEB_API_LOGO', $logo_file);
        Configuration::updateValue('WEB_API_THEME_COLOR', $theme_Color);
        Configuration::updateValue('WEB_API_ERROR_LOG', $error_log_status);
        Configuration::updateValue('WEB_API_SLIDER_STATUS', $slider_status);
        Configuration::updateValue('WEB_API_LOGO_STATUS', $logo_status);
        Configuration::updateValue('WEB_API_GRID_BANNER_STATUS', $grid_banner_status);
        Configuration::updateValue('WEB_API_NEW_PRODUCTS_STATUS', $new_products_status);
        Configuration::updateValue('WEB_API_POPULAR_PRODUCTS_STATUS', $popular_products_status);
        Configuration::updateValue('WEB_API_ONSALE_PRODUCTS_STATUS', $onsale_products_status);
        Configuration::updateValue('WEB_API_ADVANCE_CATEGORY_SLIDER_STATUS', $advance_category_slider);
        Configuration::updateValue('WEB_API_BEST_PRODUCTS_STATUS', $best_products_status);

        $result = 1;
        
        $result = Tools::jsonEncode($result);
        $this->ajaxDie($result);
    }

    public function displayAjaxUpdateList()
    {
        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $valid_token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $token = Tools::getValue('token');
        if ($valid_token != $token) {
            $error = 'Error - token not valid';
            
            $result = Tools::jsonEncode($error);
            $this->ajaxDie($result);
        }
        $result = ApiModel::getList();
        
        $url = $this->context->link->getAdminLink('AdminFCApi');
        $settings = _PS_MODULE_DIR_.'webapi/views/templates/admin/settings.tpl';
        $accounts = _PS_MODULE_DIR_.'webapi/views/templates/admin/accounts.tpl';

        $slider = _PS_MODULE_DIR_.'webapi/views/templates/admin/slider.tpl';
        $banner = _PS_MODULE_DIR_.'webapi/views/templates/admin/banner.tpl';
         
        $this->context->smarty->assign('record', $result);
        
        $this->context->smarty->assign('action_url', $url);
        $this->context->smarty->assign('settings', $settings);
        $this->context->smarty->assign('accounts', $accounts);

        $this->context->smarty->assign('slider', $slider);
        $this->context->smarty->assign('banner', $banner);
        $ajax_url = $this->context->link->getModuleLink('webapi', 'ajax');
        $this->context->smarty->assign('ajax_url', $ajax_url);

        $abc =  ($this->context->smarty->fetch(_PS_MODULE_DIR_ .'webapi/views/templates/admin/list_update.tpl'));
        $result = Tools::jsonEncode($abc);
        die($result);
    }

    public function displayAjaxTotalBill()
    {
        $sdate= Tools::getValue('sdate');
        $edate= Tools::getValue('edate');
        $price= Tools::getValue('price');

        $date1 = new DateTime($sdate);
        $date2 = new DateTime($edate);
        $diff = date_diff($date1, $date2);
        $total_days = $diff->format('%a');

        $total_bill = $total_days * $price;
        $total_bill = $total_bill + $price;

        $total_bill = Tools::jsonEncode($total_bill);
        $this->ajaxDie($total_bill);
    }
}
