<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiPsSearch extends Core
{
    public function getData()
    {
        $this->initContext();
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
            $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=context');
        } else {
            $id_language = Tools::getValue('id_language');
        }
        if (! Tools::getValue('expr')) {
            $expr = '';
            $this->writeLog('expr not Found');
        } else {
            $expr = Tools::getValue('expr');
        }
        if (! (int)Tools::getValue('page_number')) {
            $page_number = 1;
            $this->writeLog('page_number not Found default set 1');
        } else {
            $page_number = (int)Tools::getValue('page_number');
        }
        if (! (int)Tools::getValue('page_size')) {
            $page_size = 1;
            $this->writeLog('page_size not Found default set 1');
        } else {
            $page_size = (int)Tools::getValue('page_size');
        }
        if (! Tools::getValue('order_by')) {
            $order_by = 'position';
            $this->writeLog('order_by not Found default set position');
        } else {
            $order_by = Tools::getValue('order_by');
        }
        if (! Tools::getValue('order_way')) {
            $order_way = 'desc';
            $this->writeLog('order_way not Found default set desc');
        } else {
            $order_way = Tools::getValue('order_way');
        }

        $search = new Search();
        $result = $search->find($id_language, $expr, $page_number, $page_size, $order_by, $order_way);
        $new = array();
        foreach ($result as $key) {
            $key=$key;
            $new = $result['result'];
        }

        foreach ($new as $k => $value) {
            $value = $value;
            $product = new Product($new[$k]['id_product'], false, $id_language);

            $new[$k]['description'] = $product->description;
        }

        $all_products = $this->getExtraFields($new);
        $result = $this->getProductDetails($all_products, $id_language);
        $this->response['response'] = array(
            'status' => 'success',
            'message' => $this->l('data populated'),
            'data' => $result
        );

        return $this->fetchJSONResponse();
    }

    public function getExtraFields($all_products)
    {
        $imagesArray = array();
        foreach ($all_products as $k => $value) {
            unset($imagesArray);
            $id_product = $value['id_product'];
            $p = new Product($value['id_product']);

            $id_image = Product::getCover($id_product);
            $image = new Image($id_image['id_image']);
            $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
            $all_products[$k]['cover_image_url'] = $cover;
            $temp_images = $p->getImages((int) $this->context->language->id);

            foreach ($temp_images as $key => $we) {
                $key=$key;
                $image = new Image($we['id_image']);
                $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $imagesArray[] = $image_url;
            }

            $link = new Link();
            $url = $link->getProductLink($id_product);

            $all_products[$k]['images_link'] = $imagesArray;
            $all_products[$k]['url'] = $url;
            $all_products[$k]['default_currency_sign'] = $this->context->currency->sign;
            $all_products[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
            $all_products[$k]['default_currency_name'] = $this->context->currency->name;
        }
        return $all_products;
    }

    public function getProductDetails($all_products, $id_language)
    {
        foreach ($all_products as $k => $value) {
            $combination = array();
            $id_product = $value['id_product'];
            //$p = new Product($id_product);
            $attributes_options = array();
            $attributes = $this->getAttrGroup($id_product, $id_language);
            if ($attributes['groups'] != null) {
                $key_index = 0;
                foreach ($attributes['groups'] as $id_group => $g_k_value) {
                    $item = array();
                    $attributes_options[$key_index]['name'] = $g_k_value['name'];
                    $attributes_options[$key_index]['group_id'] = $id_group;
                    foreach ($g_k_value['attributes'] as $key => $attributes_items) {
                        if ($g_k_value['group_type'] == 'color') {
                            $color_code = '';
                            if (isset($attributes['colors'][$key]['value'])) {
                                $color_code = $attributes['colors'][$key]['value'];
                            }
                            $item[] = array(
                                'name' => $g_k_value['name'],
                                'id' => $key,
                                'value' => $attributes_items,
                                'code' => $color_code
                            );
                        } else {
                            $item[] = array(
                                'name' => $g_k_value['name'],
                                'id' => $key,
                                'value' => $attributes_items
                            );
                        }
                    }
                    $attributes_options[$key_index]['values'] = $item;
                    $key_index++;
                }
            }
            if ($attributes['combinations'] != null) {
                $key_index = 0;
                foreach ($attributes['combinations'] as $attr_id => $attr) {
                    $combination[$key_index]['quantity'] = $attr['quantity'];
                    $combination[$key_index]['price'] = $attr['price'];
                    $combination[$key_index]['id_product_attribute'] = $attr_id;
                    $combination[$key_index]['minimal_quantity'] = $attr['minimal_quantity'];
                    $listing_attr = '';
                    foreach ($attr['attributes'] as $attribute_id) {
                        $listing_attr .= (int) $attribute_id . '_';
                    }
                    $combination[$key_index]['combination_code'] = $listing_attr;
                    $key_index++;
                }
            }

            $all_products[$k]['combinations'] = $combination;
            $all_products[$k]['options'] = $attributes_options;
        }

        return $all_products;
    }
    
    public function getAttrGroup($id_product, $id_language)
    {
        $colors = array();
        $groups = array();
        $combination = array();
        $p = new Product($id_product);
        $groups_attr = $p->getAttributesGroups($id_language);
        $return = $this->getProductKeyVal($colors, $groups, $combination, $groups_attr, $p);
        return $return;
    }

    public function getProductKeyVal($colors, $groups, $combination, $groups_attr, $p)
    {
        if (is_array($groups_attr) && $groups_attr) {
            foreach ($groups_attr as $key) {
                if (isset($key['is_color_group'])
                    && $key['is_color_group']
                    && (isset($key['attribute_color']) && $key['attribute_color'])
                    || (file_exists(_PS_COL_IMG_DIR_ . $key['id_attribute'] . '.jpg'))) {
                    $colors[$key['id_attribute']]['value'] = $key['attribute_color'];
                    $colors[$key['id_attribute']]['name'] = $key['attribute_name'];
                    if (!isset($colors[$key['id_attribute']]['attributes_quantity'])) {
                        $colors[$key['id_attribute']]['attributes_quantity'] = 0;
                    }
                    $colors[$key['id_attribute']]['attributes_quantity'] += (int) $key['quantity'];
                }
                if (!isset($groups[$key['id_attribute_group']])) {
                    $groups[$key['id_attribute_group']] = array(
                        'group_name' => $key['group_name'],
                        'group_type' => $key['group_type'],
                        'name' => $key['public_group_name'],
                        'default' => -1,
                    );
                }
                $g_attr = $key['id_attribute_group'];

                $groups[$g_attr]['attributes'][$key['id_attribute']] = $key['attribute_name'];
                if ($key['default_on'] && $groups[$key['id_attribute_group']]['default'] == -1) {
                    $groups[$key['id_attribute_group']]['default'] = (int) $key['id_attribute'];
                }
                if (!isset($groups[$key['id_attribute_group']]['attributes_quantity'][$key['id_attribute']])) {
                    $groups[$key['id_attribute_group']]['attributes_quantity'][$key['id_attribute']] = 0;
                }
                $attr_idd = $key['id_attribute_group'];
                $groups[$attr_idd]['attributes_quantity'][$key['id_attribute']] += (int) $key['quantity'];

                $combination[$key['id_product_attribute']]['attributes'][] = (int) $key['id_attribute'];

                $priceDisplay = Product::getTaxCalculationMethod(0);
                if (!$priceDisplay || $priceDisplay == 2) {
                    $combination_price = $p->getPrice(true, $key['id_product_attribute']);
                } else {
                    $combination_price = $p->getPrice(false, $key['id_product_attribute']);
                }

                $combination[$key['id_product_attribute']]['quantity'] = (int) $key['quantity'];
                $combination[$key['id_product_attribute']]['price'] = $combination_price;
                $combination[$key['id_product_attribute']]['minimal_quantity'] = (int) $key['minimal_quantity'];
            }
            foreach ($combination as $id_product_attribute => $comb) {
                $listing_attr = '';
                foreach ($comb['attributes'] as $id_attribute) {
                    $listing_attr .= '\'' . (int) $id_attribute . '\',';
                }
                $combination[$id_product_attribute]['list'] = $listing_attr;
            }
        }
        $array = $this->makeArray($groups, $colors, $combination);
        return $array;
    }

    public function makeArray($groups, $colors, $combination)
    {
        return array(
            'groups' => $groups,
            'colors' => $colors,
            'combinations' => $combination
        );
    }
}
