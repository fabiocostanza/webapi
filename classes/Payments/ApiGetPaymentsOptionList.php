<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetPaymentsOptionList extends Core
{
    public function getData()
    {
        if (Tools::getValue('method') && Tools::getValue('method') == 'paysoncheckout1') {
            $paysoncheckout1_order_state_paid = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_PAID');
            $paysoncheckout1_log = Configuration::get('PAYSONCHECKOUT1_LOG');
            $paysoncheckout1_agentid = Configuration::get('PAYSONCHECKOUT1_AGENTID');
            $paysoncheckout1_apikey = Configuration::get('PAYSONCHECKOUT1_APIKEY');
            $paysoncheckout1_mode = Configuration::get('PAYSONCHECKOUT1_MODE');
            $paysoncheckout1_receipt = Configuration::get('PAYSONCHECKOUT1_RECEIPT');
            $paysoncheckout1_guarantee = Configuration::get('PAYSONCHECKOUT1_GUARANTEE');
            $paysoncheckout1_order_state_shipped = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_SHIPPED');
            $paysoncheckout1_order_state_cancel = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_CANCEL');
            $paysoncheckout1_order_state_credit = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_CREDIT');
            $paysoncheckout1_invoice_enabled = Configuration::get('PAYSONCHECKOUT1_INVOICE_ENABLED');
            $paysoncheckout1_payment_methods = Configuration::get('PAYSONCHECKOUT1_PAYMENT_METHODS');
            $paysoncheckout1_account_info = Configuration::get('PAYSONCHECKOUT1_ACCOUNT_INFO');
            $paysoncheckout1_invoice_fee = Configuration::get('PAYSONCHECKOUT1_INVOICE_FEE');
            $paysoncheckout1_agent_email = Configuration::get('PAYSONCHECKOUT1_AGENT_EMAIL');
            $paysoncheckout1_data = array(
                "PAYSONCHECKOUT1_ORDER_STATE_PAID" => $paysoncheckout1_order_state_paid,
                'PAYSONCHECKOUT1_LOG' => $paysoncheckout1_log,
                'PAYSONCHECKOUT1_AGENTID' => $paysoncheckout1_agentid,
                'PAYSONCHECKOUT1_APIKEY' => $paysoncheckout1_apikey,
                'PAYSONCHECKOUT1_MODE' => $paysoncheckout1_mode,
                'PAYSONCHECKOUT1_RECEIPT' => $paysoncheckout1_receipt,
                'PAYSONCHECKOUT1_GUARANTEE' => $paysoncheckout1_guarantee,
                'PAYSONCHECKOUT1_ORDER_STATE_SHIPPED' => $paysoncheckout1_order_state_shipped,
                'PAYSONCHECKOUT1_ORDER_STATE_CANCEL' => $paysoncheckout1_order_state_cancel,
                'PAYSONCHECKOUT1_ORDER_STATE_CREDIT' => $paysoncheckout1_order_state_credit,
                'PAYSONCHECKOUT1_INVOICE_ENABLED' => $paysoncheckout1_invoice_enabled,
                'PAYSONCHECKOUT1_PAYMENT_METHODS' => $paysoncheckout1_payment_methods,
                'PAYSONCHECKOUT1_ACCOUNT_INFO' => $paysoncheckout1_account_info,
                'PAYSONCHECKOUT1_INVOICE_FEE' => $paysoncheckout1_invoice_fee,
                'PAYSONCHECKOUT1_AGENT_EMAIL' => $paysoncheckout1_agent_email,
            );

            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('data populated'),
                'data' => $paysoncheckout1_data
            );
        } else if (Tools::getValue('method') && Tools::getValue('method') == 'paypal') {
            $marchant_id_live = Configuration::get('PAYPAL_MERCHANT_ID_LIVE');
            $sandbox_user_name = Configuration::get('PAYPAL_USERNAME_SANDBOX');
            $sandbox_password = Configuration::get('PAYPAL_PSWD_SANDBOX');
            $sandbox_signature = Configuration::get('PAYPAL_SIGNATURE_SANDBOX');
            $sandbox_access =Configuration::get('PAYPAL_SANDBOX_ACCESS');
            $username_live =Configuration::get('PAYPAL_USERNAME_LIVE');
            $live_password =Configuration::get('PAYPAL_PSWD_LIVE');
            $live_signature =Configuration::get('PAYPAL_SIGNATURE_LIVE');
            $live_access =Configuration::get('PAYPAL_LIVE_ACCESS');
            $sandbox =Configuration::get('PAYPAL_SANDBOX');
            $api_intent =Configuration::get('PAYPAL_API_INTENT');
            $api_advantages =Configuration::get('PAYPAL_API_ADVANTAGES');
            $api_card =Configuration::get('PAYPAL_API_CARD');
            $method =Configuration::get('PAYPAL_METHOD');
            $express_checkout_shortcut =Configuration::get('PAYPAL_EXPRESS_CHECKOUT_SHORTCUT');
            $express_checkout_shortcut_cart =Configuration::get('PAYPAL_EXPRESS_CHECKOUT_SHORTCUT_CART');
            $cron_time =Configuration::get('PAYPAL_CRON_TIME');
            $payby_braintree =Configuration::get('PAYPAL_BY_BRAINTREE');
            $express_checkout_context =Configuration::get('PAYPAL_EXPRESS_CHECKOUT_IN_CONTEXT');
            $vaulting =Configuration::get('PAYPAL_VAULTING');
            $requirments =Configuration::get('PAYPAL_REQUIREMENTS');
            $paypal_sandbox_client_id =Configuration::get('PAYPAL_SANDBOX_CLIENTID');
            if ($paypal_sandbox_client_id == false) {
                $paypal_sandbox_client_id = "";
            }
            $paypal_sandbox_secret =Configuration::get('PAYPAL_SANDBOX_SECRET');
            if ($paypal_sandbox_secret == false) {
                $paypal_sandbox_secret = "";
            }
            $paypal_live_client_id =Configuration::get('PAYPAL_LIVE_CLIENTID');
            if ($paypal_live_client_id == false) {
                $paypal_live_client_id = "";
            }
            $paypal_live_secret =Configuration::get('PAYPAL_LIVE_SECRET');
            if ($paypal_live_secret == false) {
                $paypal_live_secret = "";
            }

            $paypal_array = array();
            $data = array(
                "PAYPAL_MERCHANT_ID_LIVE" => $marchant_id_live,
                'PAYPAL_USERNAME_SANDBOX' => $sandbox_user_name,
                'PAYPAL_PSWD_SANDBOX' => $sandbox_password,
                'PAYPAL_SIGNATURE_SANDBOX' => $sandbox_signature,
                'PAYPAL_SANDBOX_ACCESS' => $sandbox_access,
                'PAYPAL_USERNAME_LIVE' => $username_live,
                'PAYPAL_PSWD_LIVE' => $live_password,
                'PAYPAL_SIGNATURE_LIVE' => $live_signature,
                'PAYPAL_LIVE_ACCESS' => $live_access,
                'PAYPAL_SANDBOX' => $sandbox,
                'PAYPAL_API_INTENT' => $api_intent,
                'PAYPAL_API_ADVANTAGES' => $api_advantages,
                'PAYPAL_API_CARD' => $api_card,
                'PAYPAL_METHOD' => $method,
                'PAYPAL_EXPRESS_CHECKOUT_SHORTCUT' => $express_checkout_shortcut,
                'PAYPAL_EXPRESS_CHECKOUT_SHORTCUT_CART' => $express_checkout_shortcut_cart,
                'PAYPAL_CRON_TIME' => $cron_time,
                'PAYPAL_BY_BRAINTREE' => $payby_braintree,
                'PAYPAL_EXPRESS_CHECKOUT_IN_CONTEXT' => $express_checkout_context,
                'PAYPAL_VAULTING' => $vaulting,
                'PAYPAL_REQUIREMENTS' => $requirments,
                'PAYPAL_SANDBOX_CLIENTID' => $paypal_sandbox_client_id,
                'PAYPAL_SANDBOX_SECRET' => $paypal_sandbox_secret,
                'PAYPAL_LIVE_CLIENTID' => $paypal_live_client_id,
                'PAYPAL_LIVE_SECRET' => $paypal_live_secret,
            );
            array_push($paypal_array, $data);
            $this->response['response'] = array(
                    'status' => 'success',
                    'message' => $this->l('data populated'),
                    'data' => $paypal_array
                );
        } else {
            if (!(Tools::getValue('id_cart')) || !(Tools::getValue('id_customer'))) {
                $this->writeLog('id_cart and id_customer required');
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('id_cart or id_customer Not Found - Class GetPaymentsOptionList')
                );
            } else {
                $id_cart = Tools::getValue('id_cart');
                $id_customer = Tools::getValue('id_customer');
                $customer = new Customer($id_customer);
                
                $this->context = Context::getContext();
                $this->context->customer = $customer;
                $this->context->cookie->id_customer = (int) $customer->id;
                $this->context->cookie->customer_lastname = $customer->lastname;
                $this->context->cookie->customer_firstname = $customer->firstname;
                $this->context->cookie->passwd = $customer->passwd;
                $this->context->cookie->logged = 1;
                $customer->logged = 1;
                $this->context->cart = new Cart($id_cart);
               
                $this->context->cookie->email = $customer->email;
                $this->context->cart->id_currency = $this->context->currency->id;
                $this->context->cart->secure_key = $customer->secure_key;
                $this->context->cart->save();

                $all_modules = Module::getPaymentModules();

                foreach ($all_modules as $k => $module) {
                    $obj = Module::getInstanceById($module['id_module']);
                    $all_modules[$k]['general_name'] =  $obj->displayName;
                    if ($all_modules[$k]['name'] == 'paysoncheckout1') {
                        $all_modules[$k]['configuration'] = $this->getConfigurationPayson();
                    }
                }
                $this->response['response'] = array(
                    'status' => 'success',
                    'message' => 'data populated',
                    'data' => $all_modules
                );
                return $this->fetchJSONResponse();
            }
        }
        return $this->fetchJSONResponse();
    }

    public function getConfigurationPayson()
    {
        $paysoncheckout1_order_state_paid = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_PAID');
        $paysoncheckout1_log = Configuration::get('PAYSONCHECKOUT1_LOG');
            $paysoncheckout1_agentid = Configuration::get('PAYSONCHECKOUT1_AGENTID');
            $paysoncheckout1_apikey = Configuration::get('PAYSONCHECKOUT1_APIKEY');
            $paysoncheckout1_mode = Configuration::get('PAYSONCHECKOUT1_MODE');
            $paysoncheckout1_receipt = Configuration::get('PAYSONCHECKOUT1_RECEIPT');
            $paysoncheckout1_guarantee = Configuration::get('PAYSONCHECKOUT1_GUARANTEE');
            $paysoncheckout1_order_state_shipped = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_SHIPPED');
            $paysoncheckout1_order_state_cancel = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_CANCEL');
            $paysoncheckout1_order_state_credit = Configuration::get('PAYSONCHECKOUT1_ORDER_STATE_CREDIT');
            $paysoncheckout1_invoice_enabled = Configuration::get('PAYSONCHECKOUT1_INVOICE_ENABLED');
            $paysoncheckout1_payment_methods = Configuration::get('PAYSONCHECKOUT1_PAYMENT_METHODS');
            $paysoncheckout1_account_info = Configuration::get('PAYSONCHECKOUT1_ACCOUNT_INFO');
            $paysoncheckout1_invoice_fee = Configuration::get('PAYSONCHECKOUT1_INVOICE_FEE');
            $paysoncheckout1_agent_email = Configuration::get('PAYSONCHECKOUT1_AGENT_EMAIL');
            $paysoncheckout1_data = array(
                "PAYSONCHECKOUT1_ORDER_STATE_PAID" => $paysoncheckout1_order_state_paid,
                'PAYSONCHECKOUT1_LOG' => $paysoncheckout1_log,
                'PAYSONCHECKOUT1_AGENTID' => $paysoncheckout1_agentid,
                'PAYSONCHECKOUT1_APIKEY' => $paysoncheckout1_apikey,
                'PAYSONCHECKOUT1_MODE' => $paysoncheckout1_mode,
                'PAYSONCHECKOUT1_RECEIPT' => $paysoncheckout1_receipt,
                'PAYSONCHECKOUT1_GUARANTEE' => $paysoncheckout1_guarantee,
                'PAYSONCHECKOUT1_ORDER_STATE_SHIPPED' => $paysoncheckout1_order_state_shipped,
                'PAYSONCHECKOUT1_ORDER_STATE_CANCEL' => $paysoncheckout1_order_state_cancel,
                'PAYSONCHECKOUT1_ORDER_STATE_CREDIT' => $paysoncheckout1_order_state_credit,
                'PAYSONCHECKOUT1_INVOICE_ENABLED' => $paysoncheckout1_invoice_enabled,
                'PAYSONCHECKOUT1_PAYMENT_METHODS' => $paysoncheckout1_payment_methods,
                'PAYSONCHECKOUT1_ACCOUNT_INFO' => $paysoncheckout1_account_info,
                'PAYSONCHECKOUT1_INVOICE_FEE' => $paysoncheckout1_invoice_fee,
                'PAYSONCHECKOUT1_AGENT_EMAIL' => $paysoncheckout1_agent_email,
            );
            return $paysoncheckout1_data;
    }
}
