<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetModules extends Core
{
    public function getData()
    {
        $this->initContext();
        $id_language = $this->context->language->id;
        if (!(Tools::getValue('name'))) {
            $this->writeLog('Name Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('module Not Found - Class GetModules')
            );
        } else {
            $module_name = Tools::getValue('name');
            $enable = Module::isInstalled($module_name);
            $data = array();
            $links = array();
            if ($module_name == 'privateshop' && $enable) {
                $enable_signup = Configuration::get('PRIVATE_SIGNUP');
                $restrict_new = Configuration::get('PRIVATE_SIGNUP_RESTRICT');
                $private_shop = Configuration::get('PRIVATIZE_SHOP');

                $private_products = Configuration::get('private_products');
                $private_cat = Configuration::get('categoryBox');
                $cms_pages = Configuration::get('cms_pages');
                $active = Module::isEnabled('privateshop');
                $birthday = Configuration::get('PRIVATE_BDAY');

                $group_enable = Configuration::get('PRIVATE_CUSTOMER_GROUP_STATE');
                $allow_groups = Configuration::get('PRIVATE_CUSTOMERS_GROUPS');
                $gender = Configuration::get('PRIVATE_GENDER_OPT');
                
                $ex =  explode(",", $private_cat);
                foreach ($ex as $key => $value) {
                    $id_cat = $value;
                    //$category = new Category($id_cat, $id_language);
                    $links[] = $this->context->link->getCategoryLink($id_cat);
                }
                $ex_p =  explode(",", $private_products);
                foreach ($ex_p as $key => $value_p) {
                    $id_pro = $value_p;
                    //$category = new Category($id_cat, $id_language);
                    $links[] = $this->context->link->getProductLink($id_pro);
                }

                $ex_cms =  explode(",", $cms_pages);
                foreach ($ex_cms as $key => $value_c) {
                    $id_cms = $value_c;

                    //$category = new Category($id_cat, $id_language);
                    $links[] = $this->context->link->getCMSLink($id_cms);
                }
                $data['active'] = $active;
                $data['enable_signup'] = $enable_signup;
                $data['restrict_new'] = $restrict_new;
                $data['private_shop'] = $private_shop;
                $data['private_products'] = $private_products;
                $data['private_cat'] = $private_cat;
                $data['cms_pages'] = $cms_pages;
                $data['birthday'] = $birthday;
                $data['gender'] = $gender;
                $data['group_enable'] = $group_enable;
                $data['allow_groups'] = $allow_groups;
            }
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('data populated'),
                'data' => $data,
                'links' => $links
            );
        }
        
        return $this->fetchJSONResponse();
    }
}
