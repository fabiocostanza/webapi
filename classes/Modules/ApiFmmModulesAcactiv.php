<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiModulesAcactiv extends Core
{
    public function getData()
    {
        $this->initContext();
        $error_count=0;

        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
        } else {
            $id_language = Tools::getValue('id_language');
            $id_language = $id_language;
        }
        if (! Tools::getValue('id_customer')) {
            $error_count=1;
            $this->writeLog('Customer ID not Found class=ModulesExtra');
        }
        //hookactionCustomerAccountAdd
        //Hook::exec('actionCustomerAccountAdd','',$id_module);
        $setting = array();
        $array_param = array();
        $isactive = '';
        if ($error_count == 0) {
            $module_name = 'acactiv';
            $id_customer = (int)Tools::getValue('id_customer');
            $customer = new Customer($id_customer);
            $array_param['newCustomer'] = $customer;
            $hook_all_modules = Hook::getHookModuleExecList('actionCustomerAccountAdd');
            foreach ($hook_all_modules as $key) {
                if ($key['module'] == 'acactiv') {
                    $id_hook = $key['id_hook'];
                    $id_hook = $id_hook;
                    $id_module = $key['id_module'];
                }
            }
            $isactive = Module::isEnabled($module_name);//acactiv
            if ($isactive) {
                $title = Configuration::get('ACACTIV_CTITLE');
                $email_toadmin = Configuration::get('ACACTIV_SENDTOME');
                $admin_title = Configuration::get('ACACTIV_ATITLE_1');
                array_push($setting, $title);
                array_push($setting, $email_toadmin);
                array_push($setting, $admin_title);
                $this->exeHook($array_param, $id_module);
            }
        }
        $this->response['response'] = array(
            'status' => 'success',
            'message' => $this->l('data populated'),
            'active' => $isactive,
            'setting' => $setting,
            'success-message' => $this->l('Your account is registered properly. Now we have to review it and manually activate. Thank you for your patience.')
        );

        return $this->fetchJSONResponse();
    }

    public function exeHook($array_param, $id_module)
    {
        Hook::exec('actionCustomerAccountAdd', $array_param, $id_module);
        return true;
    }
}
