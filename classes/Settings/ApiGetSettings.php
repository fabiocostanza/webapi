<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');
if (file_exists(_PS_MODULE_DIR_.'psgdpr/classes/GDPRConsent.php')) {
    require _PS_MODULE_DIR_.'psgdpr/classes/GDPRConsent.php';
}
class ApiGetSettings extends Core
{
    public function getData()
    {
        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
        } else {
            $id_language = Tools::getValue('id_language');
        }
        $data = array();
        $links = array();
        //module name private shop
        if (!(Tools::getValue('name'))) {
            $this->writeLog('Module Name Not Found');
        } else {
            $module_name = Tools::getValue('name');
            $enable = Module::isInstalled($module_name);

            if ($module_name == 'privateshop' && $enable) {
                $enable_signup = Configuration::get('PRIVATE_SIGNUP');
                $restrict_new = Configuration::get('PRIVATE_SIGNUP_RESTRICT');
                $private_shop = Configuration::get('PRIVATIZE_SHOP');

                $private_products = Configuration::get('private_products');
                $private_cat = Configuration::get('categoryBox');
                $cms_pages = Configuration::get('cms_pages');
                $active = Module::isEnabled('privateshop');
                $birthday = Configuration::get('PRIVATE_BDAY');

                $group_enable = Configuration::get('PRIVATE_CUSTOMER_GROUP_STATE');
                $allow_groups = Configuration::get('PRIVATE_CUSTOMERS_GROUPS');
                $gender = Configuration::get('PRIVATE_GENDER_OPT');
                

                if (!empty(trim($private_cat))) {
                    $ex =  explode(",", $private_cat);
                    foreach ($ex as $key => $value) {
                        $id_cat = $value;
                        //$category = new Category($id_cat, $id_language);
                        $links[] = $this->context->link->getCategoryLink($id_cat);
                    }
                }
                if (!empty(trim($private_products))) {
                    $ex_p =  explode(",", $private_products);
                    foreach ($ex_p as $key => $value_p) {
                        $id_pro = $value_p;
                        //$category = new Category($id_cat, $id_language);
                        $links[] = $this->context->link->getProductLink($id_pro);
                    }
                }

                if (!empty(trim($cms_pages))) {
                    $ex_cms =  explode(",", $cms_pages);
                    foreach ($ex_cms as $key => $value_c) {
                        $id_cms = $value_c;
                        //$category = new Category($id_cat, $id_language);
                        $links[] = $this->context->link->getCMSLink($id_cms);
                    }
                }
                $data['active'] = $active;
                $data['enable_signup'] = $enable_signup;
                $data['restrict_new'] = $restrict_new;
                $data['private_shop'] = $private_shop;
                $data['private_products'] = !empty($private_products)? explode(',', $private_products) : array();
                $data['private_cat'] = !empty($private_cat)? explode(',', $private_cat) : array();
                $data['cms_pages'] = !empty($cms_pages)? explode(',', $cms_pages) : array();
                $data['birthday'] = $birthday;
                $data['gender'] = $gender;
                $data['group_enable'] = $group_enable;
                $data['allow_groups'] = $allow_groups;
            }
        }



        $settings_array = array();
        $slider1 = Configuration::get('WEB_API_SLIDER1');
        $slider2 = Configuration::get('WEB_API_SLIDER2');
        $slider3 = Configuration::get('WEB_API_SLIDER3');
        $logo_img = Configuration::get('WEB_API_LOGO');

        $banner_file_1 = Configuration::get('WEB_API_BANNER_FILE_1');
        $banner_file_2 = Configuration::get('WEB_API_BANNER_FILE_2');
        $banner_file_3 = Configuration::get('WEB_API_BANNER_FILE_3');
        $banner_file_4 = Configuration::get('WEB_API_BANNER_FILE_4');

        $banner_url_1 = Configuration::get('WEB_API_BANNER_URL_1');
        $banner_url_2 = Configuration::get('WEB_API_BANNER_URL_2');
        $banner_url_3 = Configuration::get('WEB_API_BANNER_URL_3');
        $banner_url_4 = Configuration::get('WEB_API_BANNER_URL_4');

        $theme_color = Configuration::get('WEB_API_THEME_COLOR');
        $error_log_status_on = Configuration::get('WEB_API_ERROR_LOG');
        $cat_slider_status_on = Configuration::get('WEB_API_CAT_SLIDER_STATUS');
        $slider_status_on = Configuration::get('WEB_API_SLIDER_STATUS');
        $logo_status_on = Configuration::get('WEB_API_LOGO_STATUS');
        $grid_banner_status_on = Configuration::get('WEB_API_GRID_BANNER_STATUS');

        $new_products_status = Configuration::get('WEB_API_NEW_PRODUCTS_STATUS');
        $popular_products_status = Configuration::get('WEB_API_POPULAR_PRODUCTS_STATUS');
        $onsale_products_status = Configuration::get('WEB_API_ONSALE_PRODUCTS_STATUS');
        $best_products_status = Configuration::get('WEB_API_BEST_PRODUCTS_STATUS');

        $category_list = Configuration::get('WEB_API_CAT_BOX');
        if (!$category_list) {
            $category_list = '';
        }
        $category_new = array();
        $categoryname_array = array();
        $array_cat_list = explode(",", $category_list);
        
        if ($category_list != null) {
            foreach ($array_cat_list as $k => $value) {
                $value = $value;
                $id_category = $array_cat_list[$k];
                $categoryname_array['category_id'] = $id_category;
                $cat_name = new Category($id_category, $id_language);
                $cat_rewrite = $cat_name->link_rewrite;
                $link = new Link();
                $category_link = $link->getCategoryLink($id_category);
                $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
                if ($force_ssl == true) {
                    $http = 'https://';
                } else {
                    $http = 'http://';
                }
                $category_image_link = $http.$link->getCatImageLink($cat_rewrite, $id_category);
                
                $categoryname_array['category_link'] = $category_link;
                $categoryname_array['category_img_link'] = $category_image_link;
                
                $categoryname_array['category_name'] = $cat_name->name;

                array_push($category_new, $categoryname_array);
            }
        }
       // $category_new = implode(",",$category_new);

        $position_list= Configuration::get('WEB_API_POSITION');
        $position_new = array();
        $array_position = explode(",", $position_list);

        if ($position_list != null) {
            foreach ($array_position as $k => $value) {
                $value = $value;
                if ($array_position[$k] == 1) {
                    array_push($position_new, 'slider_status_on');
                } elseif ($array_position[$k] == 2) {
                     array_push($position_new, 'cat_slider_status_on');
                } elseif ($array_position[$k] == 3) {
                     array_push($position_new, 'new_products_status');
                } elseif ($array_position[$k] == 4) {
                     array_push($position_new, 'popular_products_status');
                } elseif ($array_position[$k] == 5) {
                     array_push($position_new, 'best_products_status');
                } elseif ($array_position[$k] == 6) {
                     array_push($position_new, 'grid_banner_status_on');
                } elseif ($array_position[$k] == 7) {
                     array_push($position_new, 'onsale_products_status');
                }
            }
        }
        $position_new = implode(",", $position_new);

        $slider = array();
        $slider_object = array();
        if ($slider1 != '0' && $slider1 != '') {
            $slider_1= _PS_BASE_URL_._PS_IMG_.'webapi/slider/'.$slider1;
            $slider_object['link'] = $slider_1;
            array_push($slider, $slider_object);
        }

        if ($slider2 != '0' && $slider2 != '') {
            $slider_2 = _PS_BASE_URL_._PS_IMG_.'webapi/slider/'.$slider2;
            $slider_object['link'] = $slider_2;
            array_push($slider, $slider_object);
        }
        if ($slider3 != '0' && $slider3 != '') {
            $slider_3 = _PS_BASE_URL_._PS_IMG_.'webapi/slider/'.$slider3;
            $slider_object['link'] = $slider_3;
            array_push($slider, $slider_object);
        }

        if ($logo_img != '0' && $logo_img != '') {
            $settings_array['logo_img'] =  _PS_BASE_URL_._PS_IMG_.'webapi/logo/'.$logo_img;
        } else {
            $settings_array['logo_img'] = "";
        }
        $banner = array();
        $banner_object = array();

        if ($banner_file_1 != '0' && $banner_file_1 != '') {
            $banner_1= _PS_BASE_URL_._PS_IMG_.'webapi/banner/'.$banner_file_1;
            $banner_object['image'] = $banner_1;
            $banner_object['link'] = $banner_url_1;
            array_push($banner, $banner_object);
        }

        if ($banner_file_2 != '0' && $banner_file_2 != '') {
            $banner_2 = _PS_BASE_URL_._PS_IMG_.'webapi/banner/'.$banner_file_2;
            $banner_object['image'] = $banner_2;
            $banner_object['link'] = $banner_url_2;
            array_push($banner, $banner_object);
        }

        if ($banner_file_3 != '0' && $banner_file_3 != '') {
            $banner_3= _PS_BASE_URL_._PS_IMG_.'webapi/banner/'.$banner_file_3;
            $banner_object['image'] = $banner_3;
            $banner_object['link'] = $banner_url_3;
            array_push($banner, $banner_object);
        }

        if ($banner_file_4 != '0' && $banner_file_4 != '') {
            $banner_4 = _PS_BASE_URL_._PS_IMG_.'webapi/banner/'.$banner_file_4;
            $banner_object['image'] = $banner_4;
            $banner_object['link'] = $banner_url_4;
            array_push($banner, $banner_object);
        }


        $settings_array['theme_color'] = $theme_color;
        $settings_array['error_log_status_on'] = $error_log_status_on;
        $settings_array['cat_slider_status_on'] = $cat_slider_status_on;
        $settings_array['slider_status_on'] = $slider_status_on;

        $settings_array['logo_status_on'] = $logo_status_on;
        $settings_array['grid_banner_status_on'] = $grid_banner_status_on;
        $settings_array['new_products_status'] = $new_products_status;
        $settings_array['popular_products_status'] = $popular_products_status;

        $settings_array['onsale_products_status'] = $onsale_products_status;
        $settings_array['category_list'] = $category_new;
        $settings_array['best_products_status'] = $best_products_status;
        $settings_array['position_list'] = $position_new;

        $settings_array['slider_links'] = $slider;
        $settings_array['banner_links'] = $banner;

        $gdpr = array();
        $gdpr = $gdpr;
        $gdpr_fields = array();
        $emailsubscription = $this->getGDPRConsent('ps_emailsubscription', $id_language);
        $contactform = $this->getGDPRConsent('contactform', $id_language);
        $gdpr_fields['ps_emailsubscription'] = $emailsubscription;
        $gdpr_fields['contactform'] = $contactform;

        $customer_form_active = Configuration::get('PSGDPR_CUSTOMER_FORM_SWITCH');
        $customer_form_label = Configuration::get('PSGDPR_CUSTOMER_FORM', $id_language);
        $gdpr_fields['PSGDPR_CUSTOMER_FORM_SWITCH'] = $customer_form_active;
        $gdpr_fields['PSGDPR_CUSTOMER_FORM'] = $customer_form_label;

        $creation_form_active = Configuration::get('PSGDPR_CREATION_FORM_SWITCH');
        $creation_form = Configuration::get('PSGDPR_CREATION_FORM', $id_language);
        $gdpr_fields['PSGDPR_CREATION_FORM_SWITCH'] = $creation_form_active;
        $gdpr_fields['PSGDPR_CREATION_FORM'] = $creation_form;
        $settings_array['GDPR'] = $gdpr_fields;

        $settings_array['private_data'] = $data;
        $settings_array['private_links'] = $links;

        $language = Language::getLanguages();
        $last_id = ApiModelTrans::getLastId();
        foreach ($language as $key => $we) {
            $key=$key;
            $app_trans = new ApiModelTrans($last_id, $we['id_lang']);
            $app_trans->context = '1';
            $settings_array['translations'][$we['iso_code']] = $app_trans;
        }
        $this->response['response'] = array(
            'status' => 'success',
            'message' => $this->l('data populated'),
            'data' => $settings_array
        );
        return $this->fetchJSONResponse();
    }

    public function getGDPRConsent($moduleName, $id_languages)
    {
        if (Module::isEnabled($moduleName) && Module::isEnabled('psgdpr')) {
            $module = Module::getInstanceByName($moduleName);
            if (GDPRConsent::getConsentActive($module->id)) {
                return GDPRConsent::getConsentMessage($module->id, $id_languages);
            }
        }
        return false;
    }
}
