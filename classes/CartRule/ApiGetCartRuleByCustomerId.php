<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');
class ApiGetCartRuleByCustomerId extends Core
{
    public function getData()
    {
        $this->initContext();

        if (!(int) Tools::getValue('id_language')) {
            $id_lang_default = $this->context->language->id;
        } else {
            $id_lang_default = Tools::getValue('id_language');
        }
        if (! (int)(Tools::getValue('id_customer'))) {
            $this->writeLog('id_customer Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_customer  Not Found - Class GetCartRule')
            );
        } else {
            $id_customer = Tools::getValue('id_customer');
            $cart_rules = CartRule::getCustomerCartRules($id_lang_default, $id_customer, true);
            
            $this->response['response'] = array(
                'status' => 'success',
                'message' => 'data populated',
                'data' => $cart_rules
            );
        }
        
        return $this->fetchJSONResponse();
    }
}
