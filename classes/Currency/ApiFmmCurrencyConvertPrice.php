<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiCurrencyConvertPrice extends Core
{
    public function getData()
    {
        $this->initContext();
        if (!(float) Tools::getValue('price')) {
            $this->writeLog('price not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('price not found'),
                'data' => null
            );
        } else {
            if (!(int) Tools::getValue('id_old_currency') || !(int) Tools::getValue('id_new_currency')) {
                $this->writeLog('id_currency not Found');
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('id_currency not found'),
                    'data' => null
                );
            } else {
                $id_old_currency = Tools::getValue('id_old_currency');
                $old_currency = new Currency($id_old_currency);

                $id_new_currency = Tools::getValue('id_new_currency');
                $new_currency = new Currency($id_new_currency);

                if (!Validate::isLoadedObject($old_currency) || !Validate::isLoadedObject($new_currency)) {
                    $this->response['response'] = array(
                        'status' => 'failure',
                        'message' => $this->l('id_currency Not valid'),
                        'data' => null
                    );
                } else {
                    $id_old_currency = (int)Tools::getValue('id_old_currency');
                    $price = (float)Tools::getValue('price');
                    $old_currency = new Currency($id_old_currency);

                    $id_new_currency = (int)Tools::getValue('id_new_currency');
                    $new_currency = new Currency($id_new_currency);

                    $result = Tools::convertPriceFull($price, $old_currency, $new_currency);
                    
                    $this->response['response'] = array(
                        'status' => 'success',
                        'message' => $this->l('successfully updated'),
                        'data' => $result
                    );
                }
            }
        }
        return $this->fetchJSONResponse();
    }
}
