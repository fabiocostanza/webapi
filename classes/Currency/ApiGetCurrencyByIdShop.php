<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCurrencyByIdShop extends Core
{
    public function getData()
    {
        $this->initContext();
        if (! (int)Tools::getValue('idShop')) {
            $this->writeLog('idShop not Found e.g &idShop=1 Module set default idShop=0');
            $idShop = 0;
        } else {
            $idShop = (int)Tools::getValue('idShop');
        }
        $this->response['response'] = $this->getCurrency($idShop);

        return $this->fetchJSONResponse();
    }

    public function getCurrency($idShop)
    {
        $child = Currency::getCurrenciesByIdShop($idShop);
        return $child;
    }
}
