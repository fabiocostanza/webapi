<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiSetCartDataAddressInvoice extends Core
{
    public function getData()
    {
        $this->initContext();
        $errors = array();
        $error_count=0;
        
        if (!(int) Tools::getValue('id_cart')) {
            array_push($errors, 'id_cart');
            $error_count=1;
            $this->writeLog('id_cart not Found');
        } else {
            $cart = new Cart(Tools::getValue('id_cart'));
            if (!Validate::isLoadedObject($cart)) {
                array_push($errors, 'id_cart Not Valid');
                $error_count=1;
                $this->writeLog('id_cart not Valid');
            }
        }
        if (!(int) Tools::getValue('id_address_invoice')) {
            array_push($errors, 'id_address_invoice');
            $error_count=1;
            $this->writeLog('id_address_invoice not Found');
        }
        if ($error_count == 1) {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Required fields are missing'),
                'missings' => $errors
            );
        } else {
            $id_cart = (int) Tools::getValue('id_cart');
            $id_address_invoice = (int) Tools::getValue('id_address_invoice');
            $this->saveCustomerAddress($id_cart, $id_address_invoice);
        }
        return $this->fetchJSONResponse();
    }

    public function saveCustomerAddress($id_cart, $id_address_invoice)
    {
        $sql = 'UPDATE `'._DB_PREFIX_.'cart`
            SET `id_address_invoice` = '.(int)$id_address_invoice.'
            WHERE `id_cart`   =   '.(int)$id_cart;

        $result = Db::getInstance()->execute($sql);

        if ($result) {
            return $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('id_address_invoice Updated'),
                'data' => $result
            );
        } else {
            return $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_address_invoice Not Updated'),
                'data' => $result
            );
        }
    }
}
