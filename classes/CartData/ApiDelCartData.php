<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiDelCartData extends Core
{
    public function getData()
    {
        $this->initContext();
        $context = Context::getContext();
        $context = $context;
        if (!(Tools::getValue('id_customer'))) {
            $this->writeLog('Customer id Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer id Not Found - Class DelCartData')
            );
        } elseif (!(Tools::getValue('id_cart'))) {
            $this->writeLog('id_cart Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_cart  Not Found - Class DelCartData')
            );
        } elseif (!(Tools::getValue('id_product'))) {
            $this->writeLog('id_product Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_product  Not Found - Class DelCartData')
            );
        } else {
            $id_language = $this->context->language->id;
            $id_product = Tools::getValue('id_product');
            $id_customer = Tools::getValue('id_customer');
            $id_cart = Tools::getValue('id_cart');
            $cus_exists = Customer::customerIdExistsStatic($id_customer);
            $pro_exists = new Product($id_product, false, $id_language);
            $car_exists = new Cart($id_cart);
            if ($cus_exists && Validate::isLoadedObject($pro_exists) && Validate::isLoadedObject($car_exists)) {
                $id_product_attribute = Tools::getValue('id_product_attribute');
                $quantity = Tools::getValue('quantity');
                if (!$id_product_attribute) {
                    $id_product_attribute = 0;
                }
                if (!$quantity) {
                    $quantity = 1;
                }
                $result = $this->delCartDetails($id_product, $id_product_attribute, $id_cart, $id_customer);
        
                $this->response['response'] = array(
                    'status' => 'success',
                    'message' => 'success',
                    'data' => $result
                );
            } else {
                $this->writeLog('id_cart, id_product Or id_customer Not Valid');
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('id_cart, id_product Or id_customer Not Valid - Class DelCartData')
                );
            }
        }
        return $this->fetchJSONResponse();
    }

    public function delCartDetails($id_product, $id_product_attribute, $id_cart, $id_customer)
    {
        $customer = new Customer($id_customer);
        $this->context->customer = $customer;
        $this->context->cart->id_customer = $id_customer;
        $this->context->cart->secure_key = $customer->secure_key;
        $this->context->cart = new Cart($id_cart);
        $operator = 'up';
        $operator = $operator;
        $id_custom = false;
        $id_add_dliv = 0;
        $this->context->cart->deleteProduct($id_product, $id_product_attribute, $id_custom, $id_add_dliv);
        
        return $this->l('Successfully remove');
    }
}
