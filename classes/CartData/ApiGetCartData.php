<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');
class ApiGetCartData extends Core
{
    public function getData()
    {
        $this->initContext();
        if (!(int) Tools::getValue('id_language')) {
            $id_lang_default = $this->context->language->id;
        } else {
            $id_lang_default = Tools::getValue('id_language');
        }
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        
        if (!(Tools::getValue('id_cart'))) {
            $this->writeLog('id_cart Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_cart  Not Found - Class GetCartData')
            );
        } else {
            $id_cart = Tools::getValue('id_cart');
            // $cart_details = $this->getCartDetails($id_cart);
            $objCart = new Cart($id_cart, $id_lang_default);
            $cartProducts = $objCart->getProducts();
            $imagesArray = array();
            foreach ($cartProducts as $k => $value) {
                unset($imagesArray);
                $id_product = $value['id_product'];
                $p = new Product($value['id_product']);

                $id_image = Product::getCover($id_product);
                $image = new Image($id_image['id_image']);
                $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $cartProducts[$k]['cover_image_url'] = $cover;
                $temp_images = $p->getImages((int) $this->context->language->id);

                foreach ($temp_images as $key => $we) {
                    $key = $key;
                    $image = new Image($we['id_image']);
                    $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                    $imagesArray[] = $image_url;
                }
                $link = new Link();
                $url = $link->getProductLink($id_product);
                $cartProducts[$k]['images_link'] = $imagesArray;
                $cartProducts[$k]['url'] = $url;
                $cartProducts[$k]['default_currency_sign'] = $this->context->currency->sign;
                $cartProducts[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
                $cartProducts[$k]['default_currency_name'] = $this->context->currency->name;
            }
            $this->response['response'] = array(
                'status' => 'success',
                'message' => 'data populated',
                'cart_total' => $objCart->getTotalCart($id_cart),
                'data' => $cartProducts
            );
        }
        
        return $this->fetchJSONResponse();
    }
}
