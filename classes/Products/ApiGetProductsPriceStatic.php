<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetProductsPriceStatic extends Core
{
    private $address = null;
    private $country = null;

    public function getData()
    {
        $this->initContext();

        if (!(int) Tools::getValue('id_product', 0)) {
            $this->writeLog('Product id not Found e.g &id_product=1 ');
            $this->response['product_result'] = array(
                'status' => 'failure',
                'message' =>$this->l('Product id not Found - class GetProductsPriceStatic')
            );
        } else {
            $this->product = new Product(Tools::getValue('id_product', 0));
            if (!Validate::isLoadedObject($this->product)) {
                $this->response['product_result'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Product not found - class GetProductsPriceStatic')
                );
            } else {
                $this->response['product_static_price'] = $this->getProductPrice();
            }
        }
        return $this->fetchJSONResponse();
    }

    public function getProductPrice()
    {
        if (!Tools::getValue('usetax')) {
            $usetax = false;
            $this->writeLog('usetax not Found e.g &usetax=1. Api set default usetax=1');
        } else {
            $usetax = Tools::getValue('usetax');
        }
        if (!(int) Tools::getValue('id_product_attribute', 0)) {
            $id_product_attribute = null;
            $this->writeLog('id_product_attribute not Found e.g &id_product_attribute=10.
                Api set default id_product_attribute=NULL');
        } else {
            $id_product_attribute = Tools::getValue('id_product_attribute');
        }
        if (!Tools::getValue('decimals')) {
            $decimals = 6;
            $this->writeLog('decimals not Found e.g &decimals=6. Api set default decimals=6');
        } else {
            $decimals = Tools::getValue('decimals');
        }
        if (!Tools::getValue('divisor')) {
            $divisor = null;
            $this->writeLog('divisor not Found e.g &divisor=null. Api set default divisor=null');
        } else {
            $divisor = Tools::getValue('divisor');
        }
        if (!Tools::getValue('only_reduc')) {
            $only_reduc = false;
            $this->writeLog('only_reduc not Found e.g &only_reduc=1. Api set default only_reduc=0');
        } else {
            $only_reduc = Tools::getValue('only_reduc');
        }
        if (!Tools::getValue('usereduc')) {
            $usereduc = true;
            $this->writeLog('usereduc not Found e.g &usereduc=1. Api set default usereduc=1');
        } else {
            $usereduc = Tools::getValue('usereduc');
        }
        if (!Tools::getValue('quantity')) {
            $quantity = true;
            $this->writeLog('quantity not Found e.g &quantity=2. Api set default quantity=1');
        } else {
            $quantity = (int)Tools::getValue('quantity');
        }
        if (!Tools::getValue('force_associated_tax')) {
            $force_associated_tax = false;
            $this->writeLog('force_associated_tax not Found e.g &force_associated_tax=0.
                Api set default force_associated_tax=0');
        } else {
            $force_associated_tax = Tools::getValue('force_associated_tax');
        }
        if (!Tools::getValue('id_customer')) {
            $id_customer = null;
            $this->writeLog('id_customer not Found e.g &id_customer=2. Api set default id_customer=null');
        } else {
            $id_customer = (int)Tools::getValue('id_customer');
        }
        if (! (int)Tools::getValue('id_cart')) {
            $id_cart = null;
            $this->writeLog('id_cart not Found e.g &id_cart=1. Api set default id_cart=null');
        } else {
            $id_cart = (int)Tools::getValue('id_cart');
        }
        if (!Tools::getValue('id_address')) {
            $id_address = null;
            $this->writeLog('id_address not Found e.g &id_address=2. Api set default id_address=null');
        } else {
            $id_address = (int)Tools::getValue('id_address');
        }
        if (!Tools::getValue('with_ecotax')) {
            $with_ecotax = true;
            $this->writeLog('with_ecotax not Found e.g &with_ecotax=1. Api set default with_ecotax=1');
        } else {
            $with_ecotax = Tools::getValue('with_ecotax');
        }
        if (!Tools::getValue('use_group_reduction')) {
            $use_group_reduction = true;
            $this->writeLog('use_group_reduction not Found e.g &use_group_reduction=1.
                Api set default use_group_reduction=1');
        } else {
            $use_group_reduction = Tools::getValue('use_group_reduction');
        }
        if (!Tools::getValue('use_customer_price')) {
            $use_customer_price = true;
            $this->writeLog('use_customer_price not Found e.g &use_customer_price=1.
                Api set default use_customer_price=1');
        } else {
            $use_customer_price = Tools::getValue('use_customer_price');
        }
        if (!Tools::getValue('id_customization')) {
            $id_customization = null;
            $this->writeLog('id_customization not Found e.g &id_customization=null.
                Api set default id_customization=null');
        } else {
            $id_customization = (int)Tools::getValue('id_customization');
        }
        $specific_price_output = null;
        $id_product = (int)Tools::getValue('id_product');
        $product_attribute = Product:: getPriceStatic(
            $id_product,
            $usetax,
            $id_product_attribute,
            $decimals,
            $divisor,
            $only_reduc,
            $usereduc,
            $quantity,
            $force_associated_tax,
            $id_customer,
            $id_cart,
            $id_address,
            $specific_price_output,
            $with_ecotax,
            $use_group_reduction,
            null,
            $use_customer_price,
            $id_customization
        );
        return $product_attribute;
    }
}
