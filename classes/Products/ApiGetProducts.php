<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetProducts extends Core
{
    public function getData()
    {
        $this->initContext();

        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
            $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=null');
        } else {
            $id_language = Tools::getValue('id_language');
        }
        if (!(int) Tools::getValue('start_no', 0)) {
            $start_no = 0;
            $this->writeLog('start_no not Found e.g &start_no=1. Api set default start_no=0');
        } else {
            $start_no = Tools::getValue('start_no');
        }
        if (!(int) Tools::getValue('limit', 0)) {
            $limit = null;
            $this->writeLog('limit not Found e.g &limit=10. Api set default limit=null');
        } else {
            $limit = Tools::getValue('limit');
        }
        if (!Tools::getValue('order_by')) {
            $order_by = 'id_product';
            $this->writeLog('order_by not Found e.g &order_by=id_product. Api set default order_by=id_product');
        } else {
            $order_by = Tools::getValue('order_by');
        }
        if (!Tools::getValue('order_way')) {
            $order_way = 'ASC';
            $this->writeLog('order_way not Found e.g &order_way=ASC. Api set default order_way=ASC');
        } else {
            $order_way = Tools::getValue('order_way');
        }
        if (! (int)Tools::getValue('id_category')) {
            $id_category = false;
            $this->writeLog('id_category not Found e.g &id_category=1. Api set default id_category=false');
        } else {
            $id_category = (int)Tools::getValue('id_category');
        }
        if (! (int)Tools::getValue('only_active')) {
            $only_active = false;
            $this->writeLog('only_active not Found e.g &only_active=true. Api set default only_active=false');
        } else {
            $only_active = (int)Tools::getValue('only_active');
        }

        $this->response['all_product_list'] = $this->getProducts(
            $id_language,
            $start_no,
            $limit,
            $order_by,
            $order_way,
            $id_category,
            $only_active
        );

        return $this->fetchJSONResponse();
    }
    
    public function getProducts(
        $id_language,
        $start_no,
        $limit,
        $order_by,
        $order_way,
        $id_category,
        $only_active
    ) {
        $all_products = Product::getProducts(
            $id_language,
            $start_no,
            $limit,
            $order_by,
            $order_way,
            $id_category,
            $only_active
        );
        $arraytest = array();
        foreach ($all_products as $k => $value) {
            unset($arraytest);
            $id_product = $value['id_product'];
            $p = new Product($value['id_product']);

            $id_image = Product::getCover($id_product);
            $image = new Image($id_image['id_image']);
            $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
            $all_products[$k]['cover_image_url'] = $cover;
            $temp_images = $p->getImages((int) $this->context->language->id);

            foreach ($temp_images as $key => $we) {
                $key =$key;
                $image = new Image($we['id_image']);
                $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $arraytest[] = $image_url;
            }
            $all_products[$k]['images_link'] = $arraytest;
            $all_products[$k]['default_currency_sign'] = $this->context->currency->sign;
            $all_products[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
            $all_products[$k]['default_currency_name'] = $this->context->currency->name;
        }
        return $all_products;
    }
}
