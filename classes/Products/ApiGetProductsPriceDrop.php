<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetProductsPriceDrop extends Core
{
    private $address = null;
    private $country = null;

    public function getData()
    {
        $this->initContext();
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        if (!(int) Tools::getValue('id_language')) {
            $id_language = 1;
            $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=1');
        } else {
            $id_language = Tools::getValue('id_language');
        }
        if (!(int) Tools::getValue('page_number', 0)) {
            $page_number = 0;
            $this->writeLog('page_number not Found e.g &page_number=0. Api set default page_number=0');
        } else {
            $page_number = Tools::getValue('page_number');
        }
        if (!(int) Tools::getValue('nb_products', 0)) {
            $nb_products = null;
            $this->writeLog('nb_products not Found e.g &nb_products=20. Api set default nb_products=10');
        } else {
            $nb_products = Tools::getValue('nb_products');
        }
        if (!Tools::getValue('count')) {
            $count = false;
            $this->writeLog('count not Found. Api set default count=false');
        } else {
            $count = Tools::getValue('count');
        }
        if (!Tools::getValue('order_by')) {
            $order_by = null;
            $this->writeLog('order_by not Found e.g &order_by=id_product. Api set default order_by=null');
        } else {
            $order_by = Tools::getValue('order_by');
        }
        if (!Tools::getValue('order_way')) {
            $order_way = null;
            $this->writeLog('order_way not Found e.g &order_way=ASC. Api set default order_way=null');
        } else {
            $order_way = (int)Tools::getValue('order_way');
        }
        if (!Tools::getValue('beginning')) {
            $beginning = false;
            $this->writeLog('beginning not Found e.g &beginning=2019-01-28 00:00:00. Api set default beginning=false');
        } else {
            $beginning = (int)Tools::getValue('beginning');
        }
        if (!Tools::getValue('ending')) {
            $ending = false;
            $this->writeLog('ending not Found e.g &ending=2019-01-29 00:00:00.
                Api set default ending=false');
        } else {
            $ending = (int)Tools::getValue('ending');
        }

        $this->response['all_drop_price_products'] = $this->getPriceProductDrop(
            $id_language,
            $page_number,
            $nb_products,
            $count,
            $order_by,
            $order_way,
            $beginning,
            $ending
        );
        return $this->fetchJSONResponse();
    }

    public function getPriceProductDrop(
        $id_language,
        $page_number,
        $nb_products,
        $count,
        $order_by,
        $order_way,
        $beginning,
        $ending
    ) {
        $arraytest = array();
        $drop_products = Product::getPricesDrop(
            $id_language,
            $page_number,
            $nb_products,
            $count,
            $order_by,
            $order_way,
            $beginning,
            $ending
        );
        foreach ($drop_products as $k => $value) {
            unset($arraytest);
            $id_product = $value['id_product'];
            $p = new Product($value['id_product']);

            $id_image = Product::getCover($id_product);
            $image = new Image($id_image['id_image']);
            $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
            $drop_products[$k]['cover_image_url'] = $cover;

            $temp_images = $p->getImages((int) $this->context->language->id);

            foreach ($temp_images as $key => $we) {
                $key = $key;
                $image = new Image($we['id_image']);
                $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $arraytest[] = $image_url;
            }
            $drop_products[$k]['images_link'] = $arraytest;
            $drop_products[$k]['default_currency_sign'] = $this->context->currency->sign;
            $drop_products[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
            $drop_products[$k]['default_currency_name'] = $this->context->currency->name;
        }
        return $drop_products;
    }
}
