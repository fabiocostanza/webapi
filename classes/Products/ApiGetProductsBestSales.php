<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetProductsBestSales extends Core
{
    public function getData()
    {
        $this->initContext();
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        if (!(int) Tools::getValue('id_language', 0)) {
            $id_language = $this->context->language->id;
            $this->response['product_bestsales'] = $this->getProductBest($id_language);
        } else {
            $id_language = Tools::getValue('id_language');
            $validate = Language::getLanguage($id_language);
            if ($validate) {
                $this->response['product_bestsales'] = $this->getProductBest($id_language);
            } else {
                $this->writeLog('language id not Valid');
                $this->response['product_result'] = array(
                    'status' => 'failure',
                    'message' =>$this->l('id Language not Valid - class GetProductsBestSales')
                );
            }
        }
        return $this->fetchJSONResponse();
    }
    
    public function getProductBest($id_language)
    {
        $products = ProductSale::getBestSales($id_language);
        $arraytest = array();
        foreach ($products as $k => $value) {
            unset($arraytest);
            $id_product = $value['id_product'];
            $p = new Product($value['id_product']);

            $id_image = Product::getCover($id_product);
            $image = new Image($id_image['id_image']);
            $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
            $products[$k]['cover_image_url'] = $cover;
            $temp_images = $p->getImages((int) $this->context->language->id);

            foreach ($temp_images as $key => $we) {
                $key = $key;
                $image = new Image($we['id_image']);
                $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $arraytest[] = $image_url;
            }
            $products[$k]['images_link'] = $arraytest;
            $products[$k]['default_currency_sign'] = $this->context->currency->sign;
            $products[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
            $products[$k]['default_currency_name'] = $this->context->currency->name;
        }
        return $products;
    }
}
