<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetProductsCategories extends Core
{
    private $address = null;
    private $country = null;

    public function getData()
    {
        $this->initContext();
        if (!(int) Tools::getValue('id_product', 0)) {
            $this->writeLog('Product id not Found e.g &id_product=1 ');
            $this->response['product_result'] = array(
                'status' => 'failure',
                'message' =>$this->l('Product id not Found - class GetProductsCategories')
            );
        } else {
            $this->product = new Product(Tools::getValue('id_product', 0));
            if (!Validate::isLoadedObject($this->product)) {
                $this->response['product_result'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Product not found - class GetProductsCategories')
                );
            } else {
                $this->response['product_categories'] = $this->getProductCategory();
            }
        }
            return $this->fetchJSONResponse();
    }

    public function getProductCategory()
    {
        $id = Tools::getValue('id_product', 0);
        $id_lang = (int)Tools::getValue('id_language');
        if (!$id_lang) {
            $id_lang= $this->context->language->id;
        }
        $product_category = Product::getProductCategoriesFull($id, $id_lang);
        return $product_category;
    }
}
