<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetProductsPretty extends Core
{
    public function getData()
    {
        $this->initContext();
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
            $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=null');
        } else {
            $id_language = Tools::getValue('id_language');
        }
        if (!(int) Tools::getValue('start_no', 0)) {
            $start_no = 0;
            $this->writeLog('start_no not Found e.g &start_no=1. Api set default start_no=0');
        } else {
            $start_no = Tools::getValue('start_no');
        }
        if (!(int) Tools::getValue('limit', 0)) {
            $limit = '18446744073709551615';
            $this->writeLog('limit not Found e.g &limit=10. Api set default limit=null');
        } else {
            $limit = Tools::getValue('limit');
        }
        if (!Tools::getValue('order_by')) {
            $order_by = 'id_product';
            $this->writeLog('order_by not Found e.g &order_by=id_product. Api set default order_by=id_product');
        } else {
            $order_by = Tools::getValue('order_by');
        }
        if (!Tools::getValue('order_way')) {
            $order_way = 'ASC';
            $this->writeLog('order_way not Found e.g &order_way=ASC. Api set default order_way=ASC');
        } else {
            $order_way = Tools::getValue('order_way');
        }
        if (! (int)Tools::getValue('id_category')) {
            $id_category = false;
            $this->writeLog('id_category not Found e.g &id_category=1. Api set default id_category=false');
        } else {
            $id_category = (int)Tools::getValue('id_category');
        }
        if (! (int)Tools::getValue('only_active')) {
            $only_active = false;
            $this->writeLog('only_active not Found e.g &only_active=true. Api set default only_active=false');
        } else {
            $only_active = (int)Tools::getValue('only_active');
        } if (!(int) Tools::getValue('page_number', 0)) {
            $page_number = 0;
            $this->writeLog('page_number not Found e.g &page_number=0. Api set default page_number=0');
        } else {
            $page_number = Tools::getValue('page_number');
        } if (!(int) Tools::getValue('nb_products', 0)) {
            $nb_products = null;
            $this->writeLog('nb_products not Found e.g &nb_products=20. Api set default nb_products=10');
        } else {
            $nb_products = Tools::getValue('nb_products');
        } if (!Tools::getValue('count')) {
            $count = false;
            $this->writeLog('count not Found. Api set default count=false');
        } else {
            $count = Tools::getValue('count');
        } if (!Tools::getValue('beginning')) {
            $beginning = false;
            $this->writeLog('beginning not Found e.g &beginning=2019-01-28 00:00:00. Api set default beginning=false');
        } else {
            $beginning = (int)Tools::getValue('beginning');
        }
        if (!Tools::getValue('ending')) {
            $ending = false;
            $this->writeLog('ending not Found e.g &ending=2019-01-29 00:00:00. Api set default ending=false');
        } else {
            $ending = (int)Tools::getValue('ending');
        }
        if (!Tools::getValue('type')) {
            $type = 'all';
            $this->writeLog('type not Found e.g &type="best". Api set default type="all"');
        } else {
            $type = Tools::getValue('type');
            if (!($type == 'all' || $type == 'best' || $type == 'sale' || $type == 'new')) {
                $this->writeLog('type is not Valid e.g &type="best". Api set default type="all"');
                $type = 'all';
            }
        }
        if ($type == 'all') {
            $all_products = $this->getAllPro(
                $id_language,
                $start_no,
                $limit,
                $order_by,
                $order_way,
                $id_category,
                $only_active
            );
            $all_products = Product::getProductsProperties($id_language, $all_products);
        } elseif ($type == 'best') {
            $all_products = ProductSale::getBestSales($id_language, $page_number, $nb_products, null, $order_way);
            $all_products = $this->getExtraFields($all_products);
        } elseif ($type == 'new') {
            $all_products = Product::getNewProducts(
                $id_language,
                $page_number,
                $nb_products,
                $count,
                $order_by,
                $order_way
            );
            $all_products = $this->getExtraFieldsNew($all_products);
        } elseif ($type == 'sale') {
            $all_products = Product::getPricesDrop(
                $id_language,
                $page_number,
                $nb_products,
                $count,
                $order_by,
                $order_way,
                $beginning,
                $ending
            );
            $all_products = $this->getExtraFields($all_products);
        }

        if (empty($all_products)) {
            $result = array();
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('No Product Found'),
                'data' => $result
            );
        } else {
            $result = $this->getProductDetails($all_products, $id_language);
         
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('data populated'),
                'data' => $result
            );
        }
        return $this->fetchJSONResponse();
    }

    public function getAllPro(
        $id_lang,
        $start,
        $limit,
        $order_by,
        $order_way,
        $id_category = false,
        $only_active = false
    ) {
        $only_active = true;
        $context = Context::getContext();
        $front = true;
        if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) {
            $front = false;
        }

        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            die(Tools::displayError());
        }
        if ($order_by == 'id_product' ||
            $order_by == 'price' ||
            $order_by == 'date_add' ||
            $order_by == 'date_upd') {
            $order_by_prefix = 'p';
        } elseif ($order_by == 'name') {
            $order_by_prefix = 'pl';
        } elseif ($order_by == 'position') {
            $order_by_prefix = 'c';
        }
        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by_prefix = $order_by[0];
            $order_by = $order_by[1];
        }
        $sql = 'SELECT p.*, product_shop.*, pl.* , m.`name` AS manufacturer_name, s.`name` AS supplier_name
                FROM `' . _DB_PREFIX_ . 'product` p
                ' . Shop::addSqlAssociation('product', 'p') . '
                LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (p.`id_product` = pl.`id_product` ' . Shop::addSqlRestrictionOnLang('pl') . ')
                LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
                LEFT JOIN `' . _DB_PREFIX_ . 'supplier` s ON (s.`id_supplier` = p.`id_supplier`)' .
                ($id_category ? 'LEFT JOIN `' . _DB_PREFIX_ . 'category_product` c ON (c.`id_product` = p.`id_product`)' : '') . '
                WHERE pl.`id_lang` = ' . (int) $id_lang .
                    ($id_category ? ' AND c.`id_category` = ' . (int) $id_category : '') .
                    ($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '') .
                    ($only_active ? ' AND product_shop.`active` = 1' : '') . '
                ORDER BY ' . (isset($order_by_prefix) ? pSQL($order_by_prefix) . '.' : '') . '`' . pSQL($order_by) . '` ' . pSQL($order_way) .
                ($limit > 0 ? ' LIMIT ' . (int) $start . ',' . (int) $limit : '');
        $rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ($order_by == 'price') {
            Tools::orderbyPrice($rq, $order_way);
        }
        foreach ($rq as &$row) {
            $row = Product::getTaxesInformations($row);
        }
        $imagesArray = array();
        foreach ($rq as $k => $value) {
            unset($imagesArray);
            $id_product = $value['id_product'];
            $p = new Product($value['id_product']);

            $id_image = Product::getCover($id_product);
            $image = new Image($id_image['id_image']);
            $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
            $rq[$k]['cover_image_url'] = $cover;
            $temp_images = $p->getImages((int) $this->context->language->id);

            foreach ($temp_images as $key => $we) {
                $key = $key;
                $image = new Image($we['id_image']);
                $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $imagesArray[] = $image_url;
            }

            $link = new Link();
            $url = $link->getProductLink($id_product);

            if (empty($imagesArray)) {
                $imagesArray = '';
            }
            $stock_status = StockAvailable::outOfStock($id_product);
            $default_stock = Configuration::get('PS_ORDER_OUT_OF_STOCK');

            if ($stock_status == 2) {
                $stock_status = Configuration::get('PS_ORDER_OUT_OF_STOCK');
            }
            // $p_qty = StockAvailable::getQuantityAvailableByProduct($id_product);
            // if ($p_qty < 1) {
            
            $rq[$k]['order_status'] = $stock_status;
            $rq[$k]['images_link'] = $imagesArray;
            $rq[$k]['url'] = $url;
            $rq[$k]['default_currency_sign'] = $this->context->currency->sign;
            $rq[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
            $rq[$k]['default_currency_name'] = $this->context->currency->name;
        }

        return $rq;
    }

    public function getProductDetails($all_products, $id_language)
    {
        foreach ($all_products as $k => $value) {
            $combination = array();
            $id_product = $value['id_product'];
            //$p = new Product($id_product);
            $attributes_options = array();
            $attributes = $this->getAttrGroup($id_product, $id_language);
            if ($attributes['groups'] != null) {
                $key_index = 0;
                foreach ($attributes['groups'] as $id_group => $g_k_value) {
                    $item = array();
                    $attributes_options[$key_index]['name'] = $g_k_value['name'];
                    $attributes_options[$key_index]['group_id'] = $id_group;
                    foreach ($g_k_value['attributes'] as $key => $attributes_items) {
                        if ($g_k_value['group_type'] != 'color') {
                            $item[] = array(
                                'name' => $g_k_value['name'],
                                'id' => $key,
                                'value' => $attributes_items
                            );
                        } else {
                            $color_code = '';
                            if (isset($attributes['colors'][$key]['value'])) {
                                $color_code = $attributes['colors'][$key]['value'];
                            }
                            $item[] = array(
                                'name' => $g_k_value['name'],
                                'id' => $key,
                                'value' => $attributes_items,
                                'code' => $color_code
                            );
                        }
                    }
                    $attributes_options[$key_index]['values'] = $item;
                    $key_index++;
                }
            }
            if ($attributes['combinations'] != null) {
                $key_index = 0;
                foreach ($attributes['combinations'] as $id_attributs => $attribuuts) {
                    $combination[$key_index]['quantity'] = $attribuuts['quantity'];
                    $combination[$key_index]['price'] = $attribuuts['price'];
                    $combination[$key_index]['id_product_attribute'] = $id_attributs;
                    $listing_attr = '';

                    foreach ($attribuuts['attributes'] as $id_attribute) {
                        $listing_attr .= (int) $id_attribute . '_';
                    }
                    $combination[$key_index]['combination_code'] = $listing_attr;
                    $key_index++;
                }
            }

            $all_products[$k]['combinations'] = $combination;
            $all_products[$k]['options'] = $attributes_options;
        }

        return $all_products;
    }

    public function getAttrGroup($id_product, $id_language)
    {
        $group = array();
        $p = new Product($id_product);
        $color = array();
        $combination = array();
        $groups_attr = $p->getAttributesGroups($id_language);
        $return = $this->getProductKeyVal($color, $group, $combination, $groups_attr, $p);
        return $return;
    }

    public function makeArray($groups, $colors, $combination)
    {
        return array(
            'groups' => $groups,
            'colors' => $colors,
            'combinations' => $combination
        );
    }

    public function getProductKeyVal($colors, $groups, $combination, $groups_attr, $p)
    {
        if (is_array($groups_attr) && $groups_attr) {
            foreach ($groups_attr as $key) {
                if (!isset($groups[$key['id_attribute_group']])) {
                    $groups[$key['id_attribute_group']] = array(
                        'group_name' => $key['group_name'],
                        'default' => -1,
                        'group_type' => $key['group_type'],
                        'name' => $key['group_name'],
                    );
                }
                if (isset($key['is_color_group'])
                    && $key['is_color_group']
                    && (isset($key['attribute_color']) && $key['attribute_color'])
                    || (file_exists(_PS_COL_IMG_DIR_ . $key['id_attribute'] . '.jpg'))) {
                    $colors[$key['id_attribute']]['name'] = $key['attribute_name'];
                    $colors[$key['id_attribute']]['value'] = $key['attribute_color'];
    
                    if (!isset($colors[$key['id_attribute']]['attributes_quantity'])) {
                        $colors[$key['id_attribute']]['attributes_quantity'] = 0;
                    }
                    $colors[$key['id_attribute']]['attributes_quantity'] =
                    $colors[$key['id_attribute']]['attributes_quantity']+(int) $key['quantity'];
                }
                $combination[$key['id_product_attribute']]['attributes'][] =
                (int) $key['id_attribute'];

                $priceDisplay = Product::getTaxCalculationMethod(0);
                if (!$priceDisplay || $priceDisplay == 2) {
                    $combination_price = $p->getPrice(true, $key['id_product_attribute']);
                } else {
                    $combination_price = $p->getPrice(false, $key['id_product_attribute']);
                }
                
                $g_attr = $key['id_attribute_group'];

                $groups[$g_attr]['attributes'][$key['id_attribute']] = $key['attribute_name'];
                if ($key['default_on'] && $groups[$key['id_attribute_group']]['default'] == -1) {
                    $groups[$key['id_attribute_group']]['default'] = (int) $key['id_attribute'];
                }
                if (!isset($groups[$key['id_attribute_group']]['attributes_quantity'][$key['id_attribute']])) {
                    $groups[$key['id_attribute_group']]['attributes_quantity'][$key['id_attribute']] = 0;
                }
                $attr_idd = $key['id_attribute_group'];
                $groups[$attr_idd]['attributes_quantity'][$key['id_attribute']] =
                $groups[$attr_idd]['attributes_quantity'][$key['id_attribute']]+(int) $key['quantity'];

                $combination[$key['id_product_attribute']]['quantity'] = (int) $key['quantity'];
                $combination[$key['id_product_attribute']]['price'] = $combination_price;
                $combination[$key['id_product_attribute']]['minimal_quantity'] = (int) $key['minimal_quantity'];
            }
        }
        $array = $this->makeArray($groups, $colors, $combination);
        return $array;
    }

    public function getExtraFields($all_products)
    {
        if (empty($all_products)) {
            return $all_products = null;
        }
        $imagesArray = array();

        foreach ($all_products as $k => $value) {
            unset($imagesArray);
            $id_product = $value['id_product'];
            $p = new Product($value['id_product']);

            $id_image = Product::getCover($id_product);
            $image = new Image($id_image['id_image']);
            $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
            $all_products[$k]['cover_image_url'] = $cover;
            $temp_images = $p->getImages((int) $this->context->language->id);

            foreach ($temp_images as $key => $we) {
                $key = $key;
                $image = new Image($we['id_image']);
                $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $imagesArray[] = $image_url;
            }

            $link = new Link();
            $url = $link->getProductLink($id_product);
            if (empty($imagesArray)) {
                $imagesArray = '';
            }
            
            $stock_status = StockAvailable::outOfStock($id_product);
            // $p_qty = StockAvailable::getQuantityAvailableByProduct($id_product);
            // if ($p_qty < 1) {
            $default_stock = Configuration::get('PS_ORDER_OUT_OF_STOCK');

            if ($stock_status == 2) {
                $stock_status = Configuration::get('PS_ORDER_OUT_OF_STOCK');
            }
            $all_products[$k]['order_status'] = $stock_status;
            $all_products[$k]['images_link'] = $imagesArray;
            $all_products[$k]['url'] = $url;
            $all_products[$k]['default_currency_sign'] = $this->context->currency->sign;
            $all_products[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
            $all_products[$k]['default_currency_name'] = $this->context->currency->name;
        }
        return $all_products;
    }

    public function getExtraFieldsNew($all_products)
    {
        if (empty($all_products)) {
            return $all_products = null;
        }
        $imagesArray = array();
        $feat = array();
        foreach ($all_products as $k => $value) {
            foreach ($value['features'] as $key_fe => $value_fe) {
                $key_fe = $key_fe;
                $feat[] = $value_fe;
            }
            $all_products[$k]['features'] = $feat;

            unset($imagesArray);
            $id_product = $value['id_product'];
            $p = new Product($value['id_product']);

            $id_image = Product::getCover($id_product);
            $image = new Image($id_image['id_image']);
            $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
            $all_products[$k]['cover_image_url'] = $cover;
            $temp_images = $p->getImages((int) $this->context->language->id);

            foreach ($temp_images as $key => $we) {
                $key = $key;
                $image = new Image($we['id_image']);
                $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $imagesArray[] = $image_url;
            }

            $link = new Link();
            $url = $link->getProductLink($id_product);
            if (empty($imagesArray)) {
                $imagesArray = '';
            }
            $stock_status = StockAvailable::outOfStock($id_product);
            $default_stock = Configuration::get('PS_ORDER_OUT_OF_STOCK');

            if ($stock_status == 2) {
                $stock_status = Configuration::get('PS_ORDER_OUT_OF_STOCK');
            }
            $all_products[$k]['order_status'] = $stock_status;
            $all_products[$k]['images_link'] = $imagesArray;
            $all_products[$k]['url'] = $url;
            $all_products[$k]['default_currency_sign'] = $this->context->currency->sign;
            $all_products[$k]['default_currency_iso_code'] = $this->context->currency->iso_code;
            $all_products[$k]['default_currency_name'] = $this->context->currency->name;
        }
        return $all_products;
    }
}
