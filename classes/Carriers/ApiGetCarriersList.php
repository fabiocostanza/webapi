<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCarriersList extends Core
{
    public function getData()
    {
        $this->initContext();
        if (!(Tools::getValue('id_cart'))) {
            $this->writeLog('id_cart Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_cart Not Found - Class GetCarriersList')
            );
        } else {
            $id_cart = Tools::getValue('id_cart');
            $result = $this->getCarriersList($id_cart);
            $this->response['response'] = array(
                'status' => 'success',
                'message' => 'data populated',
                'data' => $result
            );
        }

        return $this->fetchJSONResponse();
    }
    
    public function getCarriersList($idCart)
    {
        $cart = new Cart($idCart, $this->context->language->id);
        $cart->id_currency = $this->context->currency->id;
        $cart->save();
        $options = $cart->getDeliveryOptionList();
        
        $result = array();
        if (!empty($options)) {
            foreach ($options as $key => $optionList) {
                foreach ($optionList as $idCarrier => $option) {
                    $idCarrier = $idCarrier;
                    foreach ($option['carrier_list'] as $key => $val) {
                        $val = $val;
                        $result[] = $option['carrier_list'][$key];
                    }
                }
            }
            foreach ($result as $k => $module) {
                $module = $module;
                $id_currency_default = Configuration::get('PS_CURRENCY_DEFAULT');
                $iso = Currency::getCurrency($id_currency_default);
                $result[$k]['currency_iso'] = $iso['iso_code'];
            }
        } else {
            $result = null;
        }
        return $result;
    }
}
