<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCmsPagesContent extends Core
{
    
    public function getData()
    {
        $this->initContext();
        if (! (int)Tools::getValue('id_cms')) {
            $this->writeLog('id_cms not Found e.g &id_cms=1');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' =>$this->l('id_cms not Found - class GetCmsPagesContent'),
                'data' => null
            );
        } else {
            $id_language = (int)Tools::getValue('id_language');
            if (! $id_language) {
                $id_language = $this->context->language->id;
            }
            $id_cms = (int)Tools::getValue('id_cms');
            $this->response['response'] = array(
                'status' => 'success',
                'message' =>$this->l('data populated'),
                'data' => $this->getCmsPage($id_cms, $id_language)
            );
        }
        return $this->fetchJSONResponse();
    }
    public function getCmsPage($id_cms, $id_language)
    {
        $result = CMS::getCMSContent($id_cms, $id_language);
        return $result;
    }
}
