<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCustomerAddressByEmail extends Core
{
    public function getData()
    {
        $this->initContext();
        $email = Tools::getValue('email', '');
        if (!Validate::isEmail($email)) {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Email address is not valid')
            );
            $this->writeLog('Email address is not valid');
        } else {
            $this->customerAddress($email);
        }
        return $this->fetchJSONResponse();
    }

    public function customerAddress($email)
    {
        if (Customer::customerExists(strip_tags($email), false, false)) {
            $obj = new Customer();
            $tmp_customer = $obj->getByEmail($email, null, false);
            $customer = new Customer($tmp_customer->id);
            $this->context->customer = $customer;
            $sum = 0;
            $addresses_plus = array();
            $addresses = $this->context->customer->getAddresses($this->context->language->id);

            if (!empty($addresses)) {
                foreach ($addresses as $detail) {
                    $address = new Address($detail['id_address']);
                    $default_address = $address->id;
                    $addresses_plus[$sum]['id_shipping_address'] = $address->id;
                    $addresses_plus[$sum]['firstname'] = $address->firstname;
                    $addresses_plus[$sum]['lastname'] = $address->lastname;
                    $addresses_plus[$sum]['mobile_no'] = (!empty($address->phone_mobile)) ?
                        $address->phone_mobile . "," . $address->phone : $address->phone . "," . $address->phone_mobile;
                    $addresses_plus[$sum]['mobile_no'] = rtrim($addresses_plus[$sum]['mobile_no'], ',');
                    $addresses_plus[$sum]['company'] = $address->company;
                    $addresses_plus[$sum]['address_1'] = $address->address1;
                    $addresses_plus[$sum]['address_2'] = $address->address2;
                    $addresses_plus[$sum]['city'] = $address->city;
                    if ($address->id_state != 0) {
                        $addresses_plus[$sum]['state'] = State::getNameById($address->id_state);
                    } else {
                        $addresses_plus[$sum]['state'] = "";
                    }
                    $addresses_plus[$sum]['country'] = Country::getNameById(
                        $this->context->language->id,
                        $address->id_country
                    );
                    $addresses_plus[$sum]['postcode'] = $address->postcode;
                    $addresses_plus[$sum]['alias'] = $address->alias;
                    unset($address);
                    ++$sum;
                }
            }
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('data populated'),
                'default_customer_address' => $default_address,
                'data' => $addresses_plus
            );
        } else {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer not exist of this email')
            );
            $this->writeLog('Customer not exist of this email');
        }
    }
}
