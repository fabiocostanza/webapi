<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiCustomerRegister extends Core
{
    public function getData()
    {
        $this->initContext();
        $context = Context::getContext();
        $context = $context;
        if (!(Tools::getValue('email'))) {
            $this->writeLog('Customer Email Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer Email Not Found - Class CustomerRegister')
            );
        } elseif (!(Tools::getValue('password'))) {
            $this->writeLog('Customer Password Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer Password Not Found - Class CustomerRegister')
            );
        } elseif (!(Tools::getValue('first_name'))) {
            $this->writeLog('Customer first_name Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer first_name Not Found - Class CustomerRegister')
            );
        } elseif (!(Tools::getValue('last_name'))) {
            $this->writeLog('Customer last_name Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer last_name Not Found - Class CustomerRegister')
            );
        } else {
            $email = Tools::getValue('email');
            $password = Tools::getValue('password');
            $first_name = Tools::getValue('first_name');
            $last_name = Tools::getValue('last_name');
            $active = Tools::getValue('enable');
        
            if (!$active) {
                $active = true;
            } else {
                $active = false;
            }
            if (Validate::isEmail($email)) {
                $record = Customer::getCustomersByEmail($email);
                if (!empty($record)) {
                    $this->writeLog('Customer Already Exists');
                    $this->response['response'] = array(
                        'status' => 'failure',
                        'message' => $this->l('Customer Already Exists - Class CustomerRegister')
                    );
                } else {
                    if (Tools::getValue('id_gender')) {
                        $id_gender = Tools::getValue('id_gender');
                    } else {
                        $this->writeLog('id_gender Not Found, default set id_gender=1');
                        $id_gender = '1';
                    }
                    $this->validateRegister($email, $password, $first_name, $last_name, $id_gender, $active);
                }
            } else {
                $this->writeLog('Customer Email Not Valid');
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer Email Not Valid - Class CustomerRegister')
                );
            }
        }
        return $this->fetchJSONResponse();
    }
    
    public function validateRegister($email, $password, $first_name, $last_name, $id_gender, $active)
    {
        $customer = new Customer();
        $customer->firstname = $first_name;
        $customer->lastname = $last_name;
        $customer->email = $email;
        $customer->id_gender = $id_gender;
        $customer->passwd= Tools::encrypt($password);
        $secure_key = md5(uniqid(rand(), true));
        $secure_key = $secure_key;
        $customer->id_shop = (int) $this->context->shop->id;
        $customer->id_shop_group = (int) $this->context->shop->id_shop_group;
        $customer->id_lang = $this->context->language->id;
        $customer->is_guest = 0;
        $customer->active = $active;
        if ($customer->add()) {
            if (!$this->sendEmail($customer)) {
                $this->writeLog('Account created but Email not send to customer');
            }
            $this->context = Context::getContext();
            $this->context->customer = $customer;
            $this->context->cookie->id_customer = (int) $customer->id;
            $this->context->cookie->customer_lastname = $customer->lastname;
            $this->context->cookie->customer_firstname = $customer->firstname;
            $this->context->cookie->passwd = $customer->passwd;
            $this->context->cookie->logged = 1;
            $customer->logged = 1;
            $id_cart = Cart::lastNoneOrderedCart($this->context->customer->id);
            //$cart_total = Cart::getNbProducts($id_cart);
            $this->context->cart = new Cart($id_cart);
            $products = $this->context->cart->getProducts();
            $this->context->cookie->email = $customer->email;
            $this->context->cart->id_currency = $this->context->currency->id;
            $this->context->cart->secure_key = $customer->secure_key;
            $this->context->cart->save();
            $shop = new Shop((int) Configuration::get('PS_SHOP_DEFAULT'), $this->context->language->id);
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
            $array_record = array(
                'customer_info' => $customer,
                'id_cart' => $this->context->cart->id,
                'nb_products' => $this->context->cart->nbProducts(),
                'cart_products' => $products,
                'id_currency' => (int) Configuration::get('PS_CURRENCY_DEFAULT'),
                'id_shop' => $shop->id,
                'id_shop_group' => $shop->id_shop_group,
            );
            //$cart_detail = new Cart($this->context->cart->id);
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('Account has been created'),
                'data' => $array_record,
            );
        } else {
            $this->writeLog('Account not created due to unknown error');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Account not created - Class CustomerRegister')
            );
        }
        return $this->response;
    }

    protected function sendEmail($customer)
    {
        if (!Configuration::get('PS_CUSTOMER_CREATION_EMAIL')) {
            return true;
        }
        return Mail::Send(
            $this->context->language->id,
            'account',
            Mail::l('Welcome!'),
            array(
                '{firstname}' => $customer->firstname,
                '{lastname}' => $customer->lastname,
                '{email}' => $customer->email,
            ),
            $customer->email,
            $customer->firstname . ' ' . $customer->lastname
        );
    }
}
