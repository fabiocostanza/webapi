<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiAddCustomerAddress extends Core
{
    public function getData()
    {
        $this->initContext();
        
        if ((int)Tools::getValue('id_customer')) {
            $id_customer = (int)Tools::getValue('id_customer');
            $exists = Customer::customerIdExistsStatic($id_customer);
            if ($exists) {
                if (Tools::getValue('id_address')) {
                    $id_address = Tools::getValue('id_address');
                } else {
                    $id_address = 0;
                }
                if (Tools::getValue('first_name') &&
                    Tools::getValue('last_name') &&
                    Tools::getValue('address1') &&
                    Tools::getValue('city') &&
                    Tools::getValue('zip') &&
                    Tools::getValue('id_country')) {
                    if (Validate::isName(Tools::getValue('first_name')) &&
                        Validate::isName(Tools::getValue('last_name')) &&
                        Validate::isAddress(Tools::getValue('address1')) &&
                        Validate::isCityName(Tools::getValue('city')) &&
                        Validate::isPostCode(Tools::getValue('zip')) &&
                        (int)Tools::getValue('id_country')) {
                        $first_name = Tools::getValue('first_name');
                        $last_name = Tools::getValue('last_name');
                        $city = Tools::getValue('city');
                        $address1 = Tools::getValue('address1');
                        $id_state = Tools::getValue('id_state');
                        if (!$id_state) {
                            $id_state = 0;
                        }

                        $zip = Tools::getValue('zip');
                        $id_country = Tools::getValue('id_country');

                        if (Tools::getValue('alias')) {
                            $alias = Tools::getValue('alias');
                        } else {
                            $alias = Tools::getValue('first_name');
                        } if (Tools::getValue('company')) {
                            $company = Tools::getValue('company');
                        } else {
                            $company = '';
                        } if (Tools::getValue('address_complement')) {
                            $address_complement = Tools::getValue('address_complement');
                        } else {
                            $address_complement = '';
                        } if (Tools::getValue('phone')) {
                            $phone = Tools::getValue('phone');
                        } else {
                            $phone = '';
                        } if (Tools::getValue('phone_mobile')) {
                            $phone_mobile = Tools::getValue('phone_mobile');
                        } else {
                            $phone_mobile = '';
                        }
                        $this->saveCustomerAddress(
                            $first_name,
                            $last_name,
                            $city,
                            $address1,
                            $id_state,
                            $zip,
                            $id_country,
                            $alias,
                            $company,
                            $address_complement,
                            $phone,
                            $id_customer,
                            $id_address,
                            $phone_mobile
                        );
                    } else {
                        $this->response['response'] = array(
                            'status' => 'failure',
                            'message' => $this->l('Required Fileds are not valid')
                        );
                    }
                } else {
                    $this->response['response'] = array(
                        'status' => 'failure',
                        'message' => $this->l('Required fields missing')
                    );
                }
            } else {
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('id_customer not valid')
                );
                $this->writeLog('id_customer not valid Class AddCustomerAddress');
            }
        } else {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_customer not Found')
            );
        }
        
        return $this->fetchJSONResponse();
    }

    public function saveCustomerAddress(
        $first_name,
        $last_name,
        $city,
        $address1,
        $id_state,
        $zip,
        $id_country,
        $alias,
        $company,
        $address_complement,
        $phone,
        $id_customer,
        $id_address,
        $phone_mobile
    ) {
        if ($id_address == 0) {
             $address = new Address();
        } else {
            $address = new Address($id_address);
        }
        
        $this->context->customer = new Customer($id_customer);
        $address->id_customer = $id_customer;

        $address->firstname = $first_name;
        $address->lastname = $last_name;
        $address->city = $city;
        $address->address1 = $address1;
        $address->id_state = $id_state;
        $address->postcode = $zip;

        $address->alias = $alias;
        $address->company = $company;

        $address->address2 = $address_complement;
        $address->phone = $phone;
        $address->phone_mobile = $phone_mobile;

        $address->id_country = $id_country;
        $address->country = Country::getNameById($this->context->language->id, $id_country);

        if ($address->save()) {
            $all_address = $this->context->customer->getAddresses($this->context->language->id);
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('Address successfully Added'),
                'data' => $all_address
            );
        } else {
            $this->writeLog('There is error in address submition');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Try Later')
            );
        }
        return $this->response;
    }
}
