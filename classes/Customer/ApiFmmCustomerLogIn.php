<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiCustomerLogIn extends Core
{
    public function getData()
    {
        $this->initContext();
        $context = Context::getContext();
        $context=$context;
        if (!(Tools::getValue('email'))) {
            $this->writeLog('Customer Email Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer Email Not Found - Class CustomerLogIn')
            );
        } elseif (!(Tools::getValue('password'))) {
            $this->writeLog('Customer Password Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Customer Password Not Found - Class CustomerLogIn')
            );
        } else {
            $email = Tools::getValue('email');
            $password = Tools::getValue('password');
            if (Validate::isEmail($email)) {
                if (!Validate::isPasswd($password)) {
                    $this->writeLog('Password Not Valid');
                    $this->response['response'] = array(
                        'status' => 'failure',
                        'message' => $this->l('Password Not Valid - Class CustomerLogIn')
                    );
                } else {
                    $record = Customer::getCustomersByEmail($email);
                    if (!empty($record)) {
                        $this->validateLogIn($email, $password);
                    } else {
                        $this->writeLog('Customer Not Exists');
                        $this->response['response'] = array(
                            'status' => 'failure',
                            'message' => $this->l('Customer Not Exists - Class CustomerLogIn')
                        );
                    }
                }
            } else {
                $this->writeLog('Customer Email Not Valid');
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer Email Not Valid - Class CustomerLogIn')
                );
            }
        }
        return $this->fetchJSONResponse();
    }

    public function validateLogIn($email, $password)
    {
        $this->context = Context::getContext();
        $customer = new Customer();
        $validate = $customer->getByEmail($email, $password);
        if (isset($validate->active) && !$validate->active) {
            $this->writeLog('Customer Account Not Active');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Account Not Active - Class CustomerLogIn')
            );
        } elseif (!$validate || !$customer->id) {
            $this->writeLog('Customer Account Not Availble');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Account Not Availble - Class CustomerLogIn')
            );
        } else {
            $this->context->cookie->id_customer = (int)($customer->id);
            $this->context->cookie->customer_lastname = $customer->lastname;
            $this->context->cookie->customer_firstname = $customer->firstname;
            $this->context->cookie->logged = 1;
            $customer->logged = 1;
            $this->context->cookie->is_guest = $customer->isGuest();
            $this->context->cookie->passwd = $customer->passwd;
            $this->context->cookie->email = $customer->email;
            $this->context->customer = $customer;
            $id_cart = Cart::lastNoneOrderedCart($this->context->customer->id);
            if (!$id_cart) {
                $id_cart = (int) $this->context->cart->id;
            }
            $cart_total = Cart::getNbProducts($id_cart);
            $cart_total = $cart_total;
            $this->context->cart = new Cart($id_cart);
            $products = $this->context->cart->getProducts();
            $this->context->cart->id_currency = $this->context->currency->id;
            $this->context->cart->id_customer = (int) $customer->id;
            $this->context->cart->secure_key = $customer->secure_key;
            $this->context->cart->save();
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
            $shop = new Shop((int) Configuration::get('PS_SHOP_DEFAULT'), $this->context->language->id);
            $all_address = $customer->getAddresses($this->context->language->id);
            
            $array_record = array(
                'customer_info' => $customer,
                'customer_address' => $all_address,
                'id_cart' => $id_cart,
                'id_address_delivery' => $this->context->cart->id_address_delivery,
                'id_address_invoice' => $this->context->cart->id_address_invoice,
                'nb_products' => $this->context->cart->nbProducts(),
                'cart_products' => $products,
                'id_currency' => $this->context->currency->id,
                'currency_iso_code' => $this->context->currency->iso_code,
                'id_language' => $this->context->language->id,
                'language_name' => $this->context->language->name,
                'id_shop' => $shop->id,
                'id_shop_group' => $shop->id_shop_group,
            );
            $cart_detail = new Cart($this->context->cart->id);
            $cart_detail=$cart_detail;
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('Login Successfully'),
                'data' => $array_record
            );
        }
        return $this->response;
    }
}
