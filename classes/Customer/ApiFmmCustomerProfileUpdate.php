<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiCustomerProfileUpdate extends Core
{
    public function getData()
    {
        $this->initContext();

        $errors = array();
        $error_count=0;
        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
            $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=context');
        } else {
            $id_language = Tools::getValue('id_language');
            $id_language = $id_language;
        }
        if (!(int) Tools::getValue('id_gender')) {
            array_push($errors, 'id_gender');
            $error_count=1;
            $this->writeLog('id_gender not Found');
        }
        if (! Tools::getValue('firstname')) {
            array_push($errors, 'firstname');
            $error_count=1;
            $this->writeLog('firstname not Found');
        }
        if (! Tools::getValue('lastname')) {
            array_push($errors, 'lastname');
            $error_count=1;
            $this->writeLog('lastname not Found');
        }
        if (Tools::getValue('dob')) {
            $dob = Tools::getValue('dob');
            $dob = $dob;
        }
        if (! Tools::getValue('password')) {
            array_push($errors, 'password');
            $error_count=1;
            $this->writeLog('password not Found');
        } else {
            $customer = new Customer();
            $validate = $customer->getByEmail(Tools::getValue('email'), Tools::getValue('password'));
            if (! $validate) {
                array_push($errors, 'email/password not valid');
                $error_count=1;
                $this->writeLog('email/password not Valid');
            }
        }
        if (!(int) Tools::getValue('id_customer')) {
            array_push($errors, 'id_customer');
            $error_count=1;
            $this->writeLog('id_customer not Found');
        } else {
            $customer = new Customer(Tools::getValue('id_customer'));
            if (!Validate::isLoadedObject($customer)) {
                array_push($errors, 'id_customer not valid');
                $error_count=1;
                $this->writeLog('id_customer not Valid');
            }
        }
        if (!Tools::getValue('email')) {
            array_push($errors, 'email');
            $error_count=1;
            $this->writeLog('email not Found');
        } else {
            $email = Tools::getValue('email');
            if (! Tools::getValue('new_email')) {
                $new_email = $email;
            } else {
                if (!Validate::isEmail(Tools::getValue('new_email'))) {
                    $new_email = Tools::getValue('email');
                } else {
                    $new_email = Tools::getValue('new_email');
                    $new_email = $new_email;
                }
            }
            if (!Validate::isEmail($email)) {
                array_push($errors, 'email not valid');
                $error_count=1;
                $this->writeLog('email not Valid');
            } else {
                $customer = Customer::customerExists(($email), false, false);
                if (!$customer) {
                    array_push($errors, 'Customer with this email not exist');
                    $error_count=1;
                    $this->writeLog('Customer with this email not exist');
                }
            }
        }

        if ($error_count == 1) {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Required fields are missing'),
                'missings' => $errors
            );
        } else {
            $id_customer = Tools::getValue('id_customer');
            $customer = new Customer($id_customer);
            $email = Tools::getValue('email');
            $lastname = Tools::getValue('lastname');
            $firstname = Tools::getValue('firstname');
            $id_gender = Tools::getValue('id_gender');
            $password = Tools::getValue('password');
            
            
            $customer->firstname = $firstname;
            $customer->lastname = $lastname;
            $customer->passwd = Tools::encrypt($password);
            $customer->id_gender = $id_gender;
            if (Tools::getValue('new_password')) {
                $new_pas = Tools::getValue('new_password');
                $new_password = Tools::encrypt($new_pas);
                $customer->passwd = $new_password;
            }
            if (Tools::getValue('new_email')) {
                $new_mail = Tools::getValue('new_email');
                $customer->email = $new_mail;
            }
            if ($customer->update(true)) {
                $this->context->customer = $customer;
                $this->context->cookie->logged = 1;
                $this->context->cookie->id_customer = $customer->id;
                $this->context->cookie->customer_firstname = $customer->firstname;
                $this->context->cookie->customer_lastname = $customer->lastname;
                $this->context->cookie->email = $customer->email;
                $this->context->cookie->passwd = $customer->passwd;
                $this->response['response'] = array(
                    'status' => 'success',
                    'message' => $this->l('Customer Information updated successfully'),
                    'data' => $customer
                );
            } else {
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer Information cannot be updated, please check your data'),
                    'data' => null
                );
            }
        }
        
        return $this->fetchJSONResponse();
    }
}
