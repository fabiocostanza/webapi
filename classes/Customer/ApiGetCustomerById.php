<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCustomerById extends Core
{
    public function getAll()
    {
        $result = Customer::getCustomers();
        return $result;
    }

    public function getData()
    {
        $this->getCustomerById();
        return $this->fetchJSONResponse();
    }

    public function getCustomerById()
    {
        if ((int)Tools::getValue('id_customer')) {
            $id_customer = Tools::getValue('id_customer');
            $exists = Customer::customerIdExistsStatic($id_customer);
            if ($exists) {
                $result = new Customer($id_customer);
                $this->response['customer_response'] = $result;
            } else {
                $this->writeLog('Customer Not exists');
                $this->response['customer_response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer Not exists - Class GetCustomerById')
                );
            }
        } elseif (Tools::getValue('id_customer')) {
            $this->writeLog('id_customer Not Valid');
            $this->response['customer_response'] = array(
                'status' => 'failure',
                'message' => $this->l(' id_customer Not exists - Class GetCustomerById')
            );
        } else {
            $this->writeLog(' id_customer Not set Error e.g &id_customer=1');
            $this->response['customer_response'] = array(
                'status' => 'failure',
                'message' => $this->l(' id_customer not set Error - Class GetCustomerById')
            );
        }
    }
}
