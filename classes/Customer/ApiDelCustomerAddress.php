<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiDelCustomerAddress extends Core
{
    public function getData()
    {
        $this->context = $this->initContext();
        if ((int)Tools::getValue('id_customer') &&
            (int)Tools::getValue('id_address') &&
            (int)Tools::getValue('id_cart')) {
            $id_customer = (int)Tools::getValue('id_customer');
            $exists = Customer::customerIdExistsStatic($id_customer);
            if ($exists) {
                $id_address = (int)Tools::getValue('id_address');
                $address = new Address($id_address);
                if (!Validate::isLoadedObject($address)) {
                    $this->response['response'] = array(
                        'status' => 'failure',
                        'message' => $this->l('id_address not valid')
                    );
                    $this->writeLog('id_address not valid Class DelCustomerAddress');
                } else {
                    if ($address->id_customer != $id_customer) {
                        $this->response['response'] = array(
                            'status' => 'failure',
                            'message' => $this->l('id_address not valid')
                        );
                        $this->writeLog('id_address not valid for this customer Class DelCustomerAddress');
                    } else {
                        $this->context = Context::getContext();
                        $id_address = (int)Tools::getValue('id_address');
                        $id_customer = Tools::getValue('id_customer');
                        $customer = new Customer($id_customer);
                        $id_cart = (int)Tools::getValue('id_cart');
                        $this->context->customer = $customer;
                        $this->context->cookie->id_customer = (int) $customer->id;
                        $this->context->cookie->customer_lastname = $customer->lastname;
                        $this->context->cookie->customer_firstname = $customer->firstname;
                        $this->context->cookie->passwd = $customer->passwd;
                        $this->context->cart = new Cart($id_cart);
                        $this->context->cookie->id_cart = (int) $this->context->cart->id;
                        $this->context->cart->id_currency = $this->context->currency->id;
                        $this->context->cart->secure_key = $customer->secure_key;
                        $this->context->cart->save();

                        $persister = new CustomerAddressPersister(
                            $this->context->customer,
                            $this->context->cart,
                            Tools::getToken(true, $this->context)
                        );
                        $result = $persister->delete(
                            new Address($id_address, $this->context->language->id),
                            Tools::getToken(true, $this->context)
                        );

                        $addresses = $this->context->customer->getAddresses($this->context->language->id);

                        if ($result) {
                            $this->response['response'] = array(
                                'status' => 'success',
                                'message' => $this->l('address successfully deleted'),
                                'data' => $addresses
                            );
                        } else {
                            $this->response['response'] = array(
                                'status' => 'failure',
                                'message' => $this->l('Could not delete address try later'),
                                'data' => null
                            );
                            $this->writeLog('Could not delete address try later');
                        }
                    }
                }
            } else {
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('id_customer not valid')
                );
                $this->writeLog('id_customer not valid Class DelCustomerAddress');
            }
        } else {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_customer/ id_address/ id_cart required'),
                'data' => null
            );
        }
        
        return $this->fetchJSONResponse();
    }
}
