<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCustomerOrders extends Core
{
    public function getData()
    {
        if ((int)Tools::getValue('id_customer')) {
            $id_customer = Tools::getValue('id_customer');
            $exists = Customer::customerIdExistsStatic($id_customer);
            if ($exists) {
                $result = $this->getCustomerOrdersById($id_customer);
                $this->response['response'] = array(
                    'status' => 'success',
                    'message' => 'data populated',
                    'data' => $result
                );
            } else {
                $this->writeLog('Customer Not exists');
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer Not exists - Class GetCustomerOrders')
                );
            }
        } elseif (Tools::getValue('id_customer')) {
            $this->writeLog('id_customer Not Valid');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_customer Not exists - Class GetCustomerOrders')
            );
        } else {
            $this->writeLog('Customer ID Not set Error e.g &id_customer=1');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('id_customer not set - Class GetCustomerOrders')
            );
        }
        return $this->fetchJSONResponse();
    }

    public function getCustomerOrdersById($id_customer)
    {
        $orders = Order::getCustomerOrders($id_customer);
        foreach ($orders as $key => $we) {
            $id_carrier = $we['id_carrier'];
            $carrier = new Carrier($id_carrier);
            $c_name = $carrier->delay;
            $orders[$key]['carrier_name'] = $c_name[1];
            $id_currency = $we['id_currency'];
            $iso_currency = Currency::getCurrency($id_currency);
            $orders[$key]['currency_code'] = $iso_currency['iso_code'];
        }
        return $orders;
    }
}
