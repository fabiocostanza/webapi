<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCustomerBannedStatus extends Core
{
    private $address = null;
    private $country = null;

    public function getData()
    {
        $this->isBanned();

        return $this->fetchJSONResponse();
    }

    public function isBanned()
    {
        if ((int)Tools::getValue('id')) {
            $id = Tools::getValue('id');
            $exists = Customer::customerIdExistsStatic($id);
            if ($exists) {
                $result = Customer::isBanned($id);
                if (!empty($result)) {
                    $this->response['customer_banned'] = '1';
                } else {
                    $this->response['customer_banned'] = '0';
                }
            } else {
                $this->writeLog($this->l('Customer Not exists'));
                $this->response['customer_response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer Not exists - Class GetCustomerStatus')
                );
            }
        } elseif (Tools::getValue('id')) {
            $this->writeLog('Customer ID Not Exists');
                $this->response['customer_response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer ID Not exists - Class GetCustomerStatus')
                );
        } else {
            $this->writeLog('Customer ID not set e.g &id=1');
                $this->response['customer_response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Id not Set Error - Class GetCustomerStatus')
                );
        }
    }
}
