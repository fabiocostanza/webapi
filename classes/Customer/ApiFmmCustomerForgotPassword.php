<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiCustomerForgotPassword extends Core
{
    public function getData()
    {
        $this->initContext();
        $context = Context::getContext();
        $context = $context;
        if (!(Tools::getValue('email'))) {
            $this->writeLog('Email Not Found');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Email Not Found - Class CustomerForgotPassword')
            );
        } else {
            $email = Tools::getValue('email');
            if (Validate::isEmail($email)) {
                $record = Customer::getCustomersByEmail($email);
                if (!empty($record)) {
                    $customer = new Customer();
                    $customer->getByemail($email);
                    if (!$customer->active) {
                        $this->writeLog('Customer Not Active');
                        $this->response['response'] = array(
                            'status' => 'failure',
                            'message' => $this->l('Customer Not Active - Class CustomerForgotPassword')
                        );
                    } else {
                        $this->forgotPasswordMail($customer, $email);
                    }
                } else {
                    $this->writeLog('Customer Not Exists');
                    $this->response['response'] = array(
                        'status' => 'failure',
                        'message' => $this->l('Customer Not Exists - Class CustomerForgotPassword')
                    );
                }
            } else {
                $this->writeLog('Customer Email Not Valid');
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer Email Not Valid - Class CustomerForgotPassword')
                );
            }
        }
        return $this->fetchJSONResponse();
    }

    public function forgotPasswordMail($customer, $email)
    {
        $email = $email;
        $parms = array(
            '{email}' => $customer->email,
            '{lastname}' => $customer->lastname,
            '{firstname}' => $customer->firstname,
            '{url}' => $this->context->link->getPageLink(
                'password',
                true,
                null,
                'token=' . $customer->secure_key . '&id_customer=' . (int) $customer->id
            )
        );
        if (Mail::Send(
            $this->context->language->id,
            'password_query',
            Mail::l('Password confirmation'),
            $parms,
            $customer->email,
            $customer->firstname . ' ' . $customer->lastname
        )) {
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('Confirmation email has been sent')
            );
        } else {
            $this->writeLog('Mail not send due to unknown error');
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('There is issue in mail sending - Class CustomerForgotPassword')
            );
        }
        return $this->response;
    }
}
