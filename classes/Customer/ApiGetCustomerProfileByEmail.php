<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCustomerProfileByEmail extends Core
{
    public function getData()
    {
        $this->initContext();
        $this->customerProfile();
        return $this->fetchJSONResponse();
    }

    public function customerProfile()
    {
        $email = Tools::getValue('email', '');
        if (!Validate::isEmail($email)) {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Email address is not valid Class GetCustomerProfileByEmail')
            );
            $this->writeLog('Email address is not valid - Class GetCustomerProfileByEmail');
            return false;
        } else {
            if (Customer::customerExists(strip_tags($email))) {
                $obj = new Customer();
                $tmp_customer = $obj->getByEmail($email);

                $customer = new Customer($tmp_customer->id);
            
                $customer_record = array();
                $genders_record = Gender::getGenders();
                $gender_index = 0;
                foreach ($genders_record as $gender) {
                    $customer_record['gender'][$gender_index] = array(
                        'id' => $gender->id,
                        'name' => 'gender',
                        'label' => $gender->name
                    );
                    $gender_index++;
                }
                $customer_record['firstname'] = $customer->firstname;
                $customer_record['lastname'] = $customer->lastname;
                $customer_record['email'] = $customer->email;
                $customer_record['dob'] = $customer->birthday;
                $customer_record['gender'] = $customer->id_gender;
                $this->response['response'] = array(
                    'status' => 'success',
                    'data' => $customer_record
                );
                return true;
            } else {
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Customer of this email not exists - Class GetCustomerProfileByEmail')
                );
                $this->writeLog('Customer of this email not exists - Class GetCustomerProfileByEmail');
                return false;
            }
        }
    }
}
