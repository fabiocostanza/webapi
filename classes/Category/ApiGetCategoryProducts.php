<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCategoryProducts extends Core
{
    public function getData()
    {
        $this->initContext();
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        if (! (int)Tools::getValue('id_category')) {
            $this->writeLog('id_category not Found e.g &id_category=3');
            $this->response['category_result'] = array(
                'status' => 'failure',
                'message' =>$this->l('id_category not Found - class GetCategoryProducts')
            );
        } else {
            $id_category = Tools::getValue('id_category');
            
            if (!(int) Tools::getValue('id_language')) {
                $id_language = $this->context->language->id;
                $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=null');
            } else {
                $id_language = Tools::getValue('id_language');
            }
        
            $this->response['category_result'] = $this->getCategoriesProducts($id_category, $id_language);
        }
        return $this->fetchJSONResponse();
    }

    public function getCategoriesProducts($id_category, $id_language)
    {
        $category = new Category($id_category, $id_language);
        $nb = 10000;
        $products = $category->getProducts($id_language, 1, ($nb ? $nb : 10));
        return $products;
    }
}
