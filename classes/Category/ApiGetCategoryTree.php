<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetCategoryTree extends Core
{
    public function getData()
    {
        $this->initContext();

        if (!(int) Tools::getValue('id_language')) {
            $id_lang_default = $this->context->language->id;
        } else {
            $id_lang_default = Tools::getValue('id_language');
        }
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        $range = '';
        $depth = Configuration::get('BLOCK_CATEG_MAX_DEPTH');
        
        $id_result = array();
        $parent_cat = array();
        $record = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT c.id_parent, c.id_category, cl.name, cl.description, cl.link_rewrite, c.active
            FROM `' . _DB_PREFIX_ . 'category` c
            INNER JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (c.`id_category` =
                cl.`id_category` AND cl.`id_lang` = ' .
                (int)$id_lang_default . Shop::addSqlRestrictionOnLang('cl') . ')
            INNER JOIN `' . _DB_PREFIX_ . 'category_shop` cs ON (cs.`id_category` =
                c.`id_category` AND cs.`id_shop` = ' . (int)$this->context->shop->id . ')
            WHERE (c.`active` = 1 OR c.`id_category` = ' .
            (int)Configuration::get('PS_HOME_CATEGORY') . ')
            AND c.`id_category` != ' . (int)Configuration::get('PS_ROOT_CATEGORY') . '
            ' . ((int)$depth != 0 ? ' AND `level_depth` <= ' . (int)$depth : '') . '
            ' . $range . '
            AND c.id_category IN (
                SELECT id_category
                FROM `' . _DB_PREFIX_ . 'category_group`
                WHERE `id_group` IN (' . pSQL(implode(', ', Customer::getGroupsStatic((int)$this->context->customer->id))) . ')
            )
            ORDER BY `level_depth` ASC, ' .
            (Configuration::get('BLOCK_CATEG_SORT') ? 'cl.`name`' : 'cs.`position`') . ' ' .
            (Configuration::get('BLOCK_CATEG_SORT_WAY') ? 'DESC' : 'ASC'));
            
        foreach ($record as &$row) {
            $parent_cat[$row['id_parent']][] = &$row;
            $id_result[$row['id_category']] = &$row;
        }
        $category = new Category((int)Configuration::get('PS_HOME_CATEGORY'), $id_lang_default);
        $catTree = $this->categoriesTree(
            $parent_cat,
            $id_result,
            $depth,
            ($category ? $category->id : null)
        );

        $this->response['response'] = array(
                'status' => 'success',
                'message' => 'data populated',
                'data' => $catTree['children']
            );
        return $this->fetchJSONResponse();
    }
    
    public function categoriesTree($parent_cat, $id_result, $depth, $id_category = null, $currentDepth = 0)
    {
        if (is_null($id_category)) {
            $id_category = $this->context->shop->getCategory();
        }
        $childs = array();
        if (isset($parent_cat[$id_category]) &&
            count($parent_cat[$id_category]) &&
            ($depth == 0 || $currentDepth < $depth)) {
            foreach ($parent_cat[$id_category] as $subcate) {
                $childs[] = $this->categoriesTree(
                    $parent_cat,
                    $id_result,
                    $depth,
                    $subcate['id_category'],
                    $currentDepth + 1
                );
            }
        }
        if (isset($id_result[$id_category])) {
            $name = $id_result[$id_category]['name'];
            $desc = $id_result[$id_category]['description'];
            $link = $this->context->link->getCategoryLink($id_category, $id_result[$id_category]['link_rewrite']);
            $image = $this->context->link->getCatImageLink(
                $id_result[$id_category]['link_rewrite'],
                $id_category
            );
        } else {
            $link = $name = $desc = $image = '';
        }

        $return = array(
            'id' => $id_category,
            'name' => $name,
            'desc' => $desc,
            'link' => $link,
            'image' => $image,
            'children' => $childs
        );
        return $return;
    }
}
