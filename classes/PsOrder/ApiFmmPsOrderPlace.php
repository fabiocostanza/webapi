<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiPsOrderPlace extends Core
{
    public function getData()
    {
        $this->initContext();
        $errors = array();
        $error_count=0;
        if ((int) Tools::getValue('id_currency')) {
            $id_currency = Tools::getValue('id_currency');
            $result = Currency::getCurrency($id_currency);

            $currency_obj = new Currency($id_currency);

            $this->context->currency->id = $id_currency;
            $this->context->currency->name = $result['name'];
            $this->context->currency->iso_code = $result['iso_code'];
            $this->context->currency->sign = $currency_obj->sign;
        }
        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
            $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=context');
        } else {
            $id_language = Tools::getValue('id_language');
            $id_language = $id_language;
        }
        if (! (int) Tools::getValue('id_currency')) {
            array_push($errors, 'id_currency');
            $error_count=1;
            $this->writeLog('id_currency not Found');
        } else {
            $id_currency = Tools::getValue('id_currency');
            $curr = new Currency($id_currency);
            if (!Validate::isLoadedObject($curr)) {
                array_push($errors, 'id_currency not valid');
                $error_count=1;
                $this->writeLog('id_currency not Valid');
            }
        }
        if (!(int) Tools::getValue('id_cart')) {
            array_push($errors, 'id_cart');
            $error_count=1;
            $this->writeLog('id_cart not Found');
        } else {
            $cart = new Cart(Tools::getValue('id_cart'));
            if (!Validate::isLoadedObject($cart)) {
                array_push($errors, 'id_cart not valid');
                $error_count=1;
                $this->writeLog('id_cart not Valid');
            }
            $id_cart = (int) Tools::getValue('id_cart');
            $this->setCartContext($id_cart);
            if ($this->context->cart->OrderExists() == true) {
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('invalid_cart'),
                    'data' => null
                );
                return $this->fetchJSONResponse();
            }
        }
        if (!(int) Tools::getValue('id_customer')) {
            array_push($errors, 'id_customer');
            $error_count=1;
            $this->writeLog('id_customer not Found');
        } else {
            $customer = new Customer(Tools::getValue('id_customer'));
            if (!Validate::isLoadedObject($customer)) {
                array_push($errors, 'id_customer not valid');
                $error_count=1;
                $this->writeLog('id_customer not Valid');
            }
        }
        if (!(int) Tools::getValue('id_carrier')) {
            array_push($errors, 'id_carrier');
            $error_count=1;
            $this->writeLog('id_carrier not Found');
        } else {
            $id_carrier = new Carrier(Tools::getValue('id_carrier'));
            if (! Validate::isLoadedObject($id_carrier)) {
                array_push($errors, 'id_carrier');
                $error_count=1;
                $this->writeLog('id_carrier not Valid');
            }
        }
        if (!Tools::getValue('payment')) {
            array_push($errors, 'payment');
            $error_count=1;
            $this->writeLog('payment not Found');
        }
        if (! Tools::getValue('module_name')) {
            array_push($errors, 'module_name');
            $error_count=1;
            $this->writeLog('module_name not Found');
        }
        if (!(float) Tools::getValue('total_paid')) {
            array_push($errors, 'total_paid');
            $error_count=1;
            $this->writeLog('total_paid not Found');
        }
        if (!Tools::getValue('email')) {
            array_push($errors, 'email');
            $error_count=1;
            $this->writeLog('email not Found');
        } else {
            $email = Tools::getValue('email');
            if (!Validate::isEmail($email)) {
                array_push($errors, 'email not valid');
                $error_count=1;
                $this->writeLog('email not Valid');
            } else {
                $customer = Customer::customerExists(($email), false, false);
                if (!$customer) {
                    array_push($errors, 'Customer with this email not exist');
                    $error_count=1;
                    $this->writeLog('Customer with this email not exist');
                }
            }
        }
        if ($error_count == 1) {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Required fields are missing'),
                'missings' => $errors
            );
        } else {
            $id_cart = (int)Tools::getValue('id_cart');
            $id_currency = (int)Tools::getValue('id_currency');
            $id_customer = (int)Tools::getValue('id_customer');
            $id_carrier = (int)Tools::getValue('id_carrier');
           
            Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'cart` SET `id_carrier` = '
                . (int) $id_carrier . ' WHERE `id_cart` = ' . (int) $id_cart);

            Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'cart` SET `id_currency` = '
                . (int) $id_currency . ' WHERE `id_cart` = ' . (int) $id_cart);

            
            $payment = Tools::getValue('payment');
            $module = Tools::getValue('module_name');
            $total_paid = (int)Tools::getValue('total_paid');
            $email = Tools::getValue('email');

            $this->setCustomerContext($id_customer);
            $this->setCartContext($id_cart);
            if ($module == 'ps_checkpayment') {
                $order_status = Configuration::get('PS_OS_CHEQUE');
            } elseif ($module == 'ps_wirepayment') {
                $order_status = Configuration::get('PS_OS_BANKWIRE');
            } elseif ($module == 'ps_cashondelivery') {
                $order_status = Configuration::get('PS_OS_PREPARATION');
            } else {
                $order_status = Configuration::get('PS_OS_PAYMENT');//accept payment
            }

            $cart = new Cart($id_cart);
            $address_obj = new Address($cart->id_address_delivery);
            $delivery_option = array();
            $delivery_option[$cart->id_address_delivery] =  $id_carrier. ",";
            $cart->setDeliveryOption($delivery_option);
            $cart->id_currency = $id_currency;
            $cart->update();
            $this->context->cart->id_currency = $id_currency;
            
            if (Validate::isLoadedObject($cart)) {
                $customer = new Customer($id_customer);
                $this->context->country = new Country(
                    $address_obj->id_country,
                    (int)$this->context->cookie->id_lang
                );
                $paymentModule = Module::getInstanceByName($module);
                $paymentModule->validateOrder(
                    $id_cart,
                    $order_status,
                    $total_paid,
                    $payment,
                    null,
                    array(),
                    null,
                    false,
                    $customer->secure_key
                );

                $order_detail = new OrderDetail();
                $order_summery =  $order_detail->getList($paymentModule->currentOrder);
               
                foreach ($order_summery as $k => $value) {
                    $id_product = $value['product_id'];
                    $p = new Product($value['product_id']);
                    $p = $p;
                    $id_image = Product::getCover($id_product);
                    $image = new Image($id_image['id_image']);
                    $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                    $order_summery[$k]['cover_image_url'] = $cover;
                    
                    $link = new Link();
                    $url = $link->getProductLink($id_product);
                    $order_summery[$k]['url'] = $url;
                }

                $id_order = $paymentModule->currentOrder;
                $com_order = new Order($id_order);

                $id_carrier = $com_order->id_carrier;
                $carrier = new Carrier($id_carrier, $id_language);
                $c_name = $carrier->delay;
                $com_order->carrier_name = $c_name;
                $order_state = new OrderState($com_order->current_state, $id_language);
                $id_address_del = $com_order->id_address_delivery;
                $addresses_del = new Address($id_address_del);

                $id_address_inv = $com_order->id_address_invoice;
                $addresses_inv = new Address($id_address_inv);
                $com_order->address_delivery = $addresses_del;
                $com_order->address_invoice = $addresses_inv;
                $com_order->order_state = $order_state;
                $this->context->cart->id_currency = $this->context->currency->id;
                $this->context->cart->add();
                if ($this->context->cart->id) {
                    $this->context->cookie->id_cart = (int) $this->context->cart->id;
                    $id_cart = (int) $this->context->cart->id;
                }
                $this->response['response'] = array(
                    'status' => 'success',
                    'message' => $this->l('Your order is confirmed'),
                    'id_cart' => $id_cart,
                    'data' => $com_order,
                    'summery' => $order_summery
                );
            } else {
                $this->response['response'] = array(
                    'status' => 'failure',
                    'message' => $this->l('Error while creating order, Try Later'),
                    'data' => null
                );
            }
        }

        return $this->fetchJSONResponse();
    }
    
    public function setCustomerContext($id_customer)
    {
        $customer = new Customer($id_customer);
        $this->context->customer = $customer;
        $this->context->cookie->id_customer = (int) $customer->id;
        $this->context->cookie->customer_lastname = $customer->lastname;
        $this->context->cookie->customer_firstname = $customer->firstname;
        $this->context->cookie->passwd = $customer->passwd;
        $this->context->cookie->logged = 1;
        $this->context->cookie->email = $customer->email;
        $this->context->cookie->is_guest = $customer->is_guest;
        return true;
    }

    public function setCartContext($id_cart)
    {
        $this->context->cart = new Cart($id_cart, false, null, null, $this->context);
        $this->context->cart->secure_key = $this->context->customer->secure_key;
        $this->context->cart->save();
        $this->context->cart->autosetProductAddress();
        return true;
    }
}
