<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiPsOrderDetail extends Core
{
    public function getData()
    {
        $this->initContext();
        $errors = array();
        $error_count=0;
        if (!(int) Tools::getValue('id_language')) {
            $id_language = $this->context->language->id;
            $this->writeLog('id_language not Found e.g &id_language=2. Api set default id_language=context');
        } else {
            $id_language = Tools::getValue('id_language');
            $id_language = $id_language;
        }
        if (! (int) Tools::getValue('id_order')) {
            array_push($errors, 'id_order');
            $error_count=1;
            $this->writeLog('id_order not Found');
        }
        if ($error_count == 1) {
            $this->response['response'] = array(
                'status' => 'failure',
                'message' => $this->l('Required fields are missing'),
                'missings' => $errors
            );
        } else {
            $id_order = (int)Tools::getValue('id_order');
            $result = OrderDetail::getList($id_order);
            $order_summery = new Order($id_order);
            $order_state = new OrderState($order_summery->current_state, $id_language);
           
            foreach ($result as $k => $value) {
                $id_product = $value['product_id'];
                $p = new Product($value['product_id']);
                $p = $p;
                $id_image = Product::getCover($id_product);
                $image = new Image($id_image['id_image']);
                $cover = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
                $result[$k]['cover_image_url'] = $cover;
                
                $link = new Link();
                $url = $link->getProductLink($id_product);
                $result[$k]['url'] = $url;
            }
            $id_address_del = $order_summery->id_address_delivery;
            $addresses_del = new Address($id_address_del);

            $id_address_inv = $order_summery->id_address_invoice;
            $addresses_inv = new Address($id_address_inv);

            $id_carrier = $order_summery->id_carrier;
            $carrier = new Carrier($id_carrier);
            $c_name = $carrier->delay;

            $id_currency = $order_summery->id_currency;
            $iso_currency = Currency::getCurrency($id_currency);
            $order_summery->currency_code = $iso_currency['iso_code'];
            $order_summery->carrier_name = $c_name[1];
            $order_summery->address_delivery = $addresses_del;
            $order_summery->address_invoice = $addresses_inv;
            $order_summery->order_state = $order_state;
            
            $this->response['response'] = array(
                'status' => 'success',
                'message' => $this->l('data populated'),
                'data' => $result,
                'order_summery' => $order_summery,
            );
        }

        return $this->fetchJSONResponse();
    }
}
