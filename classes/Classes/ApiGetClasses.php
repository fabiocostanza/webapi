<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/webapi/classes/Core.php');

class ApiGetClasses extends Core
{
    public function getData()
    {
        $result = array();
        array_push($result, '----------Carrier------------');
        array_push($result, 'GetCarriersList');
        array_push($result, '----------Cart---------------');
        array_push($result, 'AddCartData');
        array_push($result, 'DelCartData');
        array_push($result, 'CartDataUpdate');
        array_push($result, 'GetCartData');
        array_push($result, 'SetCartDataAddressDelivery');
        array_push($result, 'SetCartDataAddressInvoice');

        array_push($result, '----------Cart Rule-----------');

        array_push($result, 'AddCartRule');
        array_push($result, 'DelCartRule');
        array_push($result, 'GetCartRuleByCustomerId');
        array_push($result, 'GetCartRuleById');

        array_push($result, '----------Category------------');

        array_push($result, 'GetCategoryChildren');
        array_push($result, 'GetCategoryDetails');
        array_push($result, 'GetCategoryHome');
        array_push($result, 'GetCategoryName');
        array_push($result, 'GetCategoryNested');
        array_push($result, 'GetCategoryProducts');
        array_push($result, 'GetCategoryTree');

        array_push($result, '------------------------------');

        array_push($result, 'GetClasses');
        array_push($result, '-----------CMS----------------');

        array_push($result, 'GetCmsPagesContent');
        array_push($result, 'GetCmsPagesList');
        array_push($result, '-----------Country------------');
        array_push($result, 'GetCountryList');
        array_push($result, '-----------Currency-----------');
        array_push($result, 'CurrencyConvertPrice');
        array_push($result, 'GetCurrencyById');
        array_push($result, 'GetCurrencyByIdShop');
        array_push($result, 'GetCurrencyList');
        array_push($result, '-----------Customer-----------');
        array_push($result, 'AddCustomerAddress');
        array_push($result, 'DelCustomerAddress');
        array_push($result, 'CustomerForgotPassword');
        array_push($result, 'CustomerLogIn');
        array_push($result, 'CustomerLogOut');
        array_push($result, 'CustomerProfileUpdate');
        array_push($result, 'CustomerRegister');
        array_push($result, 'GetCustomer');
        array_push($result, 'GetCustomerAddressByEmail');
        array_push($result, 'GetCustomerBannedStatus');
        array_push($result, 'GetCustomerByEmail');
        array_push($result, 'GetCustomerById');
        array_push($result, 'GetCustomerExistsStatus');
        array_push($result, 'GetCustomerOrders');
        array_push($result, 'GetCustomerProfileByEmail');
        array_push($result, '-----------Language-----------');
        array_push($result, 'LanguageUpdate');
        array_push($result, 'GetLanguageById');
        array_push($result, 'GetLanguageList');
        array_push($result, '-----------Payments-----------');
        array_push($result, 'GetPaymentsOptionList');
        array_push($result, '-----------Products-----------');
        array_push($result, 'GetProducts');
        array_push($result, 'GetProductsAttributeInformation');
        array_push($result, 'GetProductsBestSales');
        array_push($result, 'GetProductsById');
        array_push($result, 'GetProductsCategories');
        array_push($result, 'GetProductsCoverImage');
        array_push($result, 'GetProductsDefaultAttribute');
        array_push($result, 'GetProductsNew');
        array_push($result, 'GetProductsPopular');
        array_push($result, 'GetProductsPretty');
        array_push($result, 'GetProductsPriceDrop');
        array_push($result, 'GetProductsPriceStatic');
        array_push($result, 'GetProductsQuantity');
        array_push($result, 'GetProductsSimple');
        array_push($result, '-----------Order--------------');
        array_push($result, 'PsOrderDetail');
        array_push($result, 'PsOrderPlace');
        array_push($result, '-----------Search-------------');
        array_push($result, 'PsSearch');
        array_push($result, '-----------Settings-----------');
        array_push($result, 'GetSettings');
        array_push($result, '------------------------------');

        $this->response['response'] = array(
            'status' => 'success',
            'message' => 'data_populated',
            'data' => $result
        );
        return $this->fetchJSONResponse();
    }
}
