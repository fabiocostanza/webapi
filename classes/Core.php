<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

abstract class Core extends ModuleFrontController
{
    public $response;
    public $log_var;
    public function __construct()
    {
        $this->setLog();
        $this->context = Context::getContext();
        $id_language = Configuration::get('PS_LANG_DEFAULT');
        $this->context->cookie->id_language = $id_language;
        $id_currency = Configuration::get('PS_CURRENCY_DEFAULT');
        $this->context->cookie->id_currency = $id_currency;
        $id_country = Configuration::get('PS_COUNTRY_DEFAULT');
        $this->context->cookie->id_country = $id_country;
    }

    public function initContext()
    {
        $this->context = Context::getContext();
    }
    
    public function target()
    {
        $time = microtime(true);
        $response = $this->getData();

        return array(
            'type' => 'json',
            'content' => $response,
            'header' => array(
                'Access-Time: ' . time(),
                'Content-Type: text/json',
                'Powered-By: PrestaShop  Webservice API',
                'Execution-Time: ' . round($time - $_SERVER["REQUEST_TIME_FLOAT"], 4)
            )
        );
    }

    public function setLog()
    {
        $log_dir =  _PS_MODULE_DIR_ . 'webapi/log/';
        if (!is_dir($log_dir)) {
            if (mkdir($log_dir, 0777)) {
                mkdir(_PS_MODULE_DIR_ . 'webapi/log/', 0777, true);
                $log_dir = $log_dir . date('Y-m-d', time());
            }
        } else {
            $log_dir = $log_dir . date('Y-m-d', time());
        }
        $this->log_var = fopen($log_dir . 'log.txt', 'a+');
    }

    
    public function writeLog($error = '')
    {
        //date_default_timezone_set('Asia/Karachi');
        $time = date("h:i:sa");
        $date = date('Y-m-d');
        $log_time = $date.'-'.$time;

        fwrite($this->log_var, $log_time ."\t" . 'Alert' . "\t" . $error . "\n");
    }

    protected function fetchJSONResponse()
    {
        $response = array();
        if (!empty($this->response)) {
            $response = $this->response;
        }
        header('Content-Type: application/json');
        return $response;
    }

    abstract public function getData();
}
