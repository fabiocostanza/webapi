<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ .'ps_webapi` (
    `id_webapi` int(11) NOT NULL AUTO_INCREMENT,
        `key`            TEXT,
        `description`    TEXT,
        `status`         int(10) DEFAULT 1,
    PRIMARY KEY  (`id_webapi`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ .'ps_webapi_trans` (
    `id_trans` int(11) NOT NULL AUTO_INCREMENT,
    `active`                tinyint(1) default \'0\',
    PRIMARY KEY  (`id_trans`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'ps_webapi_trans_lang(
        `id_trans`               int(11) NOT NULL,
        `id_lang`               int(11) NOT NULL DEFAULT 1,
        `home`            varchar(255) default NULL,
        `categories`            varchar(255) default NULL,
        `cart`            varchar(255) default NULL,
        `account`            varchar(255) default NULL,
        `viewAll`            varchar(255) default NULL,
        `search`            varchar(255) default NULL,
        `allCategories`            varchar(255) default NULL,
        `newProducts`            varchar(255) default NULL,
        `bestSellers`            varchar(255) default NULL,
        `popularProducts`            varchar(255) default NULL,
        `saleProducts`            varchar(255) default NULL,
        `signIn`            varchar(255) default NULL,
        `signOut`            varchar(255) default NULL,
        `profile`            varchar(255) default NULL,
        `address`            varchar(255) default NULL,
        `vouchers`            varchar(255) default NULL,
        `ordersHistory`            varchar(255) default NULL,
        `currency`            varchar(255) default NULL,
        `version`            varchar(255) default NULL,
        `login`            varchar(255) default NULL,
        `forgotPassword`            varchar(255) default NULL,
        `signUp`            varchar(255) default NULL,
        `haveNotAnAccount`            varchar(255) default NULL,
        `password`            varchar(255) default NULL,
        `newPassword`            varchar(255) default NULL,
        `confirmPassword`            varchar(255) default NULL,
        `submit`            varchar(255) default NULL,
        `firstName`            varchar(255) default NULL,
        `lastName`            varchar(255) default NULL,
        `register`            varchar(255) default NULL,
        `haveAnAccount`            varchar(255) default NULL,
        `buy`            varchar(255) default NULL,
        `shippingAddress`            varchar(255) default NULL,
        `shippingMethods`            varchar(255) default NULL,
        `paymentMethods`            varchar(255) default NULL,
        `placeOrder`            varchar(255) default NULL,
        `confirmation`            varchar(255) default NULL,
        `addToCart`            varchar(255) default NULL,
        `outOfStock`            varchar(255) default NULL,
        `quantity`            varchar(255) default NULL,
        `contactName`            varchar(255) default NULL,
        `gender`            varchar(255) default NULL,
        `dateOfBirth`            varchar(255) default NULL,
        `myProfile`            varchar(255) default NULL,
        `update`            varchar(255) default NULL,
        `addNewAddress`            varchar(255) default NULL,
        `alias`            varchar(255) default NULL,
        `company`            varchar(255) default NULL,
        `vatNumber`            varchar(255) default NULL,
        `addressComplement`            varchar(255) default NULL,
        `zipPostalCode`            varchar(255) default NULL,
        `phone`            varchar(255) default NULL,
        `city`            varchar(255) default NULL,
        `country`            varchar(255) default NULL,
        `myOrders`            varchar(255) default NULL,
        `order`            varchar(255) default NULL,
        `orderDetails`            varchar(255) default NULL,
        `billingAddress`            varchar(255) default NULL,
        `continueShopping`            varchar(255) default NULL,
        `proceedToCheckout`            varchar(255) default NULL,
        `close`            varchar(255) default NULL,
        `languageName`            varchar(255) default NULL,
        `email`            varchar(255) default NULL,
        `voucherCode`            varchar(255) default NULL,
        `in_stock`            varchar(255) default NULL,
        `total_tax_incl`            varchar(255) default NULL,
        `total_vouchers`            varchar(255) default NULL,
        `total_shipping`            varchar(255) default NULL,
        `total_products`            varchar(255) default NULL,
        `order_is_confirmed`            varchar(255) default NULL,
        `tax_incl`            varchar(255) default NULL,
        `eror`            varchar(255) default NULL,
        `incorrect`            varchar(255) default NULL,
        `please_enter_your`            varchar(255) default NULL,
        `something_went_wrong`            varchar(255) default NULL,
        `try_again`            varchar(255) default NULL,
        `please_select`            varchar(255) default NULL,
        `congratulations`            varchar(255) default NULL,
        `added_into_cart`            varchar(255) default NULL,
        `order_reference`            varchar(255) default NULL,
        `order_status`            varchar(255) default NULL,
        `check_back_later`            varchar(255) default NULL,
        `there_are_no_item`            varchar(255) default NULL,
        `session_expired`            varchar(255) default NULL,
        `account_registered`            TEXT default NULL,
        `reset_mail_send`            TEXT default NULL,
        `placed_on`            TEXT default NULL,
        `valid_till`            TEXT default NULL,
        `voucher_value`            TEXT default NULL,
        `minimum_purchase`            TEXT default NULL,
        `noof_vouchers`            TEXT default NULL,
        PRIMARY KEY             (`id_trans`,`id_lang`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
