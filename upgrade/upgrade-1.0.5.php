<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_0_5($module)
{
    $module = $module;
    if (columnExist('ps_webapi_trans_lang', 'eror') &&
        columnExist('ps_webapi_trans_lang', 'incorrect') &&
        columnExist('ps_webapi_trans_lang', 'please_enter_your') &&
        columnExist('ps_webapi_trans_lang', 'try_again') &&
        columnExist('ps_webapi_trans_lang', 'please_select') &&
        columnExist('ps_webapi_trans_lang', 'congratulations') &&
        columnExist('ps_webapi_trans_lang', 'added_into_cart') &&
        columnExist('ps_webapi_trans_lang', 'order_reference') &&
        columnExist('ps_webapi_trans_lang', 'order_status') &&
        columnExist('ps_webapi_trans_lang', 'check_back_later') &&
        columnExist('ps_webapi_trans_lang', 'something_went_wrong') &&
        columnExist('ps_webapi_trans_lang', 'there_are_no_item')) {
        return true;
    } else {
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'ps_webapi_trans_lang`
            ADD `eror`       varchar(255),
            ADD `incorrect`       varchar(255),
            ADD `please_enter_your`       varchar(255),
            ADD `try_again`       varchar(255),
            ADD `please_select`       varchar(255),
            ADD `congratulations`       varchar(255),
            ADD `added_into_cart`       varchar(255),
            ADD `order_reference`       varchar(255),
            ADD `order_status`       varchar(255),
            ADD `check_back_later`       varchar(255),
            ADD `something_went_wrong`       varchar(255),
            ADD `there_are_no_item`      varchar(255)');
        return true;
    }
    return false;
}

function columnExist($table, $column_name)
{
    $columns = Db::getInstance()->ExecuteS('SELECT COLUMN_NAME FROM information_schema.columns
        WHERE table_schema = "'._DB_NAME_.'" AND table_name = "'._DB_PREFIX_.pSQL($table).'"');
    if (isset($columns) && $columns) {
        foreach ($columns as $column) {
            if ($column['COLUMN_NAME'] == $column_name) {
                return true;
            }
        }
    }
    return false;
}
