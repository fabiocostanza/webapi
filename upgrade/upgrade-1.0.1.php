<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_0_1($module)
{
    $module = $module;
    Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ .'ps_webapi_trans` (
        `id_trans` int(11) NOT NULL AUTO_INCREMENT,
        `active`                tinyint(1) default \'0\',
        PRIMARY KEY  (`id_trans`)
        ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;
    ');

    Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'ps_webapi_trans_lang(
        `id_trans`               int(11) NOT NULL,
        `id_lang`               int(11) NOT NULL DEFAULT 1,
        `home`            varchar(255) default NULL,
        `categories`            varchar(255) default NULL,
        `cart`            varchar(255) default NULL,
        `account`            varchar(255) default NULL,
        `viewAll`            varchar(255) default NULL,
        `search`            varchar(255) default NULL,
        `allCategories`            varchar(255) default NULL,
        `newProducts`            varchar(255) default NULL,
        `bestSellers`            varchar(255) default NULL,
        `popularProducts`            varchar(255) default NULL,
        `saleProducts`            varchar(255) default NULL,
        `signIn`            varchar(255) default NULL,
        `signOut`            varchar(255) default NULL,
        `profile`            varchar(255) default NULL,
        `address`            varchar(255) default NULL,
        `vouchers`            varchar(255) default NULL,
        `ordersHistory`            varchar(255) default NULL,
        `currency`            varchar(255) default NULL,
        `version`            varchar(255) default NULL,
        `login`            varchar(255) default NULL,
        `forgotPassword`            varchar(255) default NULL,
        `signUp`            varchar(255) default NULL,
        `haveNotAnAccount`            varchar(255) default NULL,
        `password`            varchar(255) default NULL,
        `newPassword`            varchar(255) default NULL,
        `confirmPassword`            varchar(255) default NULL,
        `submit`            varchar(255) default NULL,
        `firstName`            varchar(255) default NULL,
        `lastName`            varchar(255) default NULL,
        `register`            varchar(255) default NULL,
        `haveAnAccount`            varchar(255) default NULL,
        `buy`            varchar(255) default NULL,
        `shippingAddress`            varchar(255) default NULL,
        `shippingMethods`            varchar(255) default NULL,
        `paymentMethods`            varchar(255) default NULL,
        `placeOrder`            varchar(255) default NULL,
        `confirmation`            varchar(255) default NULL,
        `addToCart`            varchar(255) default NULL,
        `outOfStock`            varchar(255) default NULL,
        `quantity`            varchar(255) default NULL,
        `contactName`            varchar(255) default NULL,
        `gender`            varchar(255) default NULL,
        `dateOfBirth`            varchar(255) default NULL,
        `myProfile`            varchar(255) default NULL,
        `update`            varchar(255) default NULL,
        `addNewAddress`            varchar(255) default NULL,
        `alias`            varchar(255) default NULL,
        `company`            varchar(255) default NULL,
        `vatNumber`            varchar(255) default NULL,
        `addressComplement`            varchar(255) default NULL,
        `zipPostalCode`            varchar(255) default NULL,
        `phone`            varchar(255) default NULL,
        `city`            varchar(255) default NULL,
        `country`            varchar(255) default NULL,
        `myOrders`            varchar(255) default NULL,
        `order`            varchar(255) default NULL,
        `orderDetails`            varchar(255) default NULL,
        `billingAddress`            varchar(255) default NULL,
        `continueShopping`            varchar(255) default NULL,
        `proceedToCheckout`            varchar(255) default NULL,
        `close`            varchar(255) default NULL,
        `languageName`            varchar(255) default NULL,

        PRIMARY KEY             (`id_trans`,`id_lang`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
    ');
    return true;
}
