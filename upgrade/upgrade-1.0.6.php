<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_0_6($module)
{
    $module = $module;
    if (columnExist('ps_webapi_trans_lang', 'session_expired')) {
    } else {
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'ps_webapi_trans_lang`
            ADD `session_expired`      varchar(255)');
    }
    if (columnExist('ps_webapi_trans_lang', 'account_registered') &&
        columnExist('ps_webapi_trans_lang', 'reset_mail_send') &&
        columnExist('ps_webapi_trans_lang', 'placed_on') &&
        columnExist('ps_webapi_trans_lang', 'valid_till') &&
        columnExist('ps_webapi_trans_lang', 'voucher_value') &&
        columnExist('ps_webapi_trans_lang', 'minimum_purchase') &&
        columnExist('ps_webapi_trans_lang', 'noof_vouchers')) {
    } else {
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'ps_webapi_trans_lang`
            ADD `account_registered`       text,
            ADD `reset_mail_send`       text,
            ADD `placed_on`       text,
            ADD `valid_till`       text,
            ADD `voucher_value`       text,
            ADD `minimum_purchase`       text,
            ADD `noof_vouchers`      text');
    }
    return true;
}

function columnExist($table, $column_name)
{
    $columns = Db::getInstance()->ExecuteS('SELECT COLUMN_NAME FROM information_schema.columns
        WHERE table_schema = "'._DB_NAME_.'" AND table_name = "'._DB_PREFIX_.pSQL($table).'"');
    if (isset($columns) && $columns) {
        foreach ($columns as $column) {
            if ($column['COLUMN_NAME'] == $column_name) {
                return true;
            }
        }
    }
    return false;
}
