<?php
/**
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Fabio C.
*  @copyright 2017 Fabio C.
*  @license   Fabio C.
*  @version   1.0.0
*/

class ApiModelTrans extends ObjectModel
{
    public $id_trans;
    public $home;
    public $cart;
    public $account;
    public $categories;
    public $active;
    public $viewAll;
    public $search;
    public $newProducts;
    public $allCategories;
    public $bestSellers;
    public $popularProducts;
    public $saleProducts;
    public $signOut;
    public $signIn;
    public $profile;
    public $address;
    public $vouchers;
    public $ordersHistory;
    public $currency;
    public $version;
    public $login;
    public $forgotPassword;
    public $signUp;
    public $haveNotAnAccount;
    public $password;
    public $newPassword;
    public $confirmPassword;
    public $submit;
    public $firstName;
    public $lastName;
    public $register;
    public $haveAnAccount;
    public $buy;
    public $shippingAddress;
    public $shippingMethods;
    public $paymentMethods;
    public $placeOrder;
    public $confirmation;
    public $addToCart;
    public $outOfStock;
    public $quantity;
    public $contactName;
    public $gender;
    public $dateOfBirth;
    public $myProfile;
    public $update;
    public $addNewAddress;
    public $alias;
    public $company;
    public $vatNumber;
    public $addressComplement;
    public $zipPostalCode;
    public $phone;
    public $city;
    public $country;
    public $myOrders;
    public $order;
    public $orderDetails;
    public $billingAddress;
    public $continueShopping;
    public $proceedToCheckout;
    public $close;
    public $languageName;
    public $voucherCode;
    public $email;
    public $total_tax_incl;
    public $in_stock;
    public $total_vouchers;
    public $total_shipping;
    public $total_products;
    public $order_is_confirmed;
    public $tax_incl;
    public $eror;
    public $incorrect;
    public $please_enter_your;
    public $something_went_wrong;
    public $try_again;
    public $please_select;
    public $congratulations;
    public $added_into_cart;
    public $order_reference;
    public $order_status;
    public $check_back_later;
    public $there_are_no_item;
    public $session_expired;
    public $account_registered;
    public $reset_mail_send;
    public $placed_on;
    public $valid_till;
    public $voucher_value;
    public $minimum_purchase;
    public $noof_vouchers;

    public static $definition = array(
        'table' => 'ps_webapi_trans',
        'primary' => 'id_trans',
        'multilang' => true,
        'fields' => array(
            'active' => array('type' => self::TYPE_BOOL),
            'home' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'categories' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'cart' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'account' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'viewAll' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'search' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'allCategories' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'newProducts' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'bestSellers' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'popularProducts' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'saleProducts' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'signIn' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'signOut' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'profile' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'address' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'vouchers' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'ordersHistory' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'currency' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'version' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'login' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'forgotPassword' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'signUp' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'haveNotAnAccount' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'password' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'newPassword' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'confirmPassword' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'submit' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'firstName' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'lastName' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'register' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'haveAnAccount' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'buy' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'shippingAddress' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'shippingMethods' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'paymentMethods' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'placeOrder' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'confirmation' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'addToCart' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'outOfStock' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'quantity' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'contactName' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'gender' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'dateOfBirth' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'myProfile' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'update' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'addNewAddress' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'alias' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'company' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'vatNumber' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'addressComplement' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'zipPostalCode' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'phone' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'city' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'country' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'myOrders' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'order' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'orderDetails' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'billingAddress' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'continueShopping' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'proceedToCheckout' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'close' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'voucherCode' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'email' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'languageName' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'total_tax_incl' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'in_stock' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'total_vouchers' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'total_shipping' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'total_products' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'order_is_confirmed' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'tax_incl' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'eror' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'incorrect' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'please_enter_your' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'something_went_wrong' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'try_again' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'please_select' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'congratulations' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'added_into_cart' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'order_reference' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'order_status' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'check_back_later' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'there_are_no_item' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'session_expired' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'account_registered' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'reset_mail_send' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'placed_on' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'valid_till' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'voucher_value' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'minimum_purchase' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
            'noof_vouchers' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),

        ),
    );

    public function __construct($id_trans = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id_trans, $id_lang, $id_shop);
        $this->context = Context::getContext();
    }

    public static function getLastId()
    {
        $sql = new DbQuery();
        $sql->select('id_trans');
        $sql->from('ps_webapi_trans');
        $sql->orderBy('id_trans DESC LIMIT 1');
        return Db::getInstance()->executeS($sql);
    }
}
