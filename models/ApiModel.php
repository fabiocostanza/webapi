<?php
/**
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Fabio C.
*  @copyright 2017 Fabio C.
*  @license   Fabio C.
*  @version   1.0.0
*/

class ApiModel extends ObjectModel
{
    public $key;
    public $id_webapi;
    public $access_get;
    public $access_update;
    public $access_delete;
    public $status;
    public $description;

    public static $definition = array(
        'table' => 'ps_webapi',
        'primary' => 'id_webapi',
        'fields' => array(
            'key'            => array('type' => self::TYPE_STRING, 'required' => true),
            'access_get'     => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'access_update'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'access_delete'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'status'         => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'description'    => array('type' => self::TYPE_STRING),
        ),
    );

    public static function getList()
    {
        $sql = true;
        $sql = Db::getInstance()->executeS('
            SELECT *
            FROM `'._DB_PREFIX_.'ps_webapi`');

        return $sql;
    }

    public static function getKeys()
    {
        $sql = true;
        $sql = Db::getInstance()->executeS('
            SELECT `key`
            FROM `'._DB_PREFIX_.'ps_webapi`');

        return $sql;
    }

    public static function insertRecord($key, $description, $status)
    {
        return Db::getInstance()->insert('ps_webapi', array('key'=>pSQL($key),
            'description'=>pSQL($description), 'status'=>pSQL($status)));
    }

    public static function updateRecord($id, $key, $description, $status)
    {
        return Db::getInstance()->update('ps_webapi', array('key'=>pSQL($key),
            'description'=>pSQL($description), 'status'=>pSQL($status)), 'id_webapi = '.pSQL((int)$id));
    }

    public static function deleteRecord($id)
    {
        $sql = 'DELETE FROM '._DB_PREFIX_.'ps_webapi WHERE `id_webapi` = '.pSQL($id);
        return Db::getInstance()->execute($sql);
    }
}
