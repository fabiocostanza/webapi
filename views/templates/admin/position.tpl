{*
*  Web Api
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* @author    Fabio C.
* @copyright Copyright 2019 © Fabio C.
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  Fabio C.
* @package   WebApi
*}


<div class="panel" style="border-color: cadetblue;">
<div class="col-lg-6 form-group margin-form">
   <label class="form-group control-label col-lg-5">{l s='Position 1' mod='webapi'}</label>
   <div class="col-lg-5">
      <div class="col-lg-5">
         <select class="form-control fixed-width-xxl " name="position_one" id="position_one">
            <option value="1" {if isset($position_one) && $position_one && $position_one == '1'}selected="selected"{/if}>{l s='Main Slider' mod='webapi'}</option>
            <option value="2"  {if isset($position_one) && $position_one && $position_one == '2'}selected="selected"{/if} >{l s='Top Categories' mod='webapi'}</option>
            <option value="3"  {if isset($position_one) && $position_one && $position_one == '3'}selected="selected"{/if} >{l s='New Products' mod='webapi'}</option>
            <option value="4"  {if isset($position_one) && $position_one && $position_one == '4'}selected="selected"{/if} >{l s='Popular Products' mod='webapi'}</option>
            <option value="5"  {if isset($position_one) && $position_one && $position_one == '5'}selected="selected"{/if} >{l s='Best Sale Products' mod='webapi'}</option>
            <option value="6"  {if isset($position_one) && $position_one && $position_one == '6'}selected="selected"{/if} >{l s='Grid Banner' mod='webapi'}</option>
            <option value="7"  {if isset($position_one) && $position_one && $position_one == '7'}selected="selected"{/if} >{l s='On Sale Products' mod='webapi'}</option>
         </select>
      </div>
      <div class="clearfix"></div>
   </div>
</div>



<div class="col-lg-6 form-group margin-form">
   <label class="form-group control-label col-lg-5">{l s='Position 2' mod='webapi'}</label>
   <div class="col-lg-5">
      <div class="col-lg-5">
         <select class="form-control fixed-width-xxl " name="position_two" id="position_two">
            <option value="1" {if isset($position_two) && $position_two && $position_two == '1'}selected="selected"{/if}>{l s='Main Slider' mod='webapi'}</option>
            <option value="2"  {if isset($position_two) && $position_two && $position_two == '2'}selected="selected"{/if} >{l s='Top Categories' mod='webapi'}</option>
            <option value="3"  {if isset($position_two) && $position_two && $position_two == '3'}selected="selected"{/if} >{l s='New Products' mod='webapi'}</option>
            <option value="4"  {if isset($position_two) && $position_two && $position_two == '4'}selected="selected"{/if} >{l s='Popular Products' mod='webapi'}</option>
            <option value="5"  {if isset($position_two) && $position_two && $position_two == '5'}selected="selected"{/if} >{l s='Best Sale Products' mod='webapi'}</option>
            <option value="6"  {if isset($position_two) && $position_two && $position_two == '6'}selected="selected"{/if} >{l s='Grid Banner' mod='webapi'}</option>
            <option value="7"  {if isset($position_two) && $position_two && $position_two == '7'}selected="selected"{/if} >{l s='On Sale Products' mod='webapi'}</option>
         </select>
      </div>
      <div class="clearfix"></div>
   </div>
</div>



<div class="col-lg-6 form-group margin-form">
   <label class="form-group control-label col-lg-5">{l s='Position 3' mod='webapi'}</label>
   <div class="col-lg-5">
      <div class="col-lg-5">
         <select class="form-control fixed-width-xxl " name="position_three" id="position_three">
            <option value="1" {if isset($position_three) && $position_three && $position_three == '1'}selected="selected"{/if}>{l s='Main Slider' mod='webapi'}</option>
            <option value="2"  {if isset($position_three) && $position_three && $position_three == '2'}selected="selected"{/if} >{l s='Top Categories' mod='webapi'}</option>
            <option value="3"  {if isset($position_three) && $position_three && $position_three == '3'}selected="selected"{/if} >{l s='New Products' mod='webapi'}</option>
            <option value="4"  {if isset($position_three) && $position_three && $position_three == '4'}selected="selected"{/if} >{l s='Popular Products' mod='webapi'}</option>
            <option value="5"  {if isset($position_three) && $position_three && $position_three == '5'}selected="selected"{/if} >{l s='Best Sale Products' mod='webapi'}</option>
            <option value="6"  {if isset($position_three) && $position_three && $position_three == '6'}selected="selected"{/if} >{l s='Grid Banner' mod='webapi'}</option>
            <option value="7"  {if isset($position_three) && $position_three && $position_three == '7'}selected="selected"{/if} >{l s='On Sale Products' mod='webapi'}</option>
         </select>
      </div>
      <div class="clearfix"></div>
   </div>
</div>



<div class="col-lg-6 form-group margin-form">
   <label class="form-group control-label col-lg-5">{l s='Position 4' mod='webapi'}</label>
   <div class="col-lg-5">
      <div class="col-lg-5">
         <select class="form-control fixed-width-xxl " name="position_four" id="position_four">
            <option value="1" {if isset($position_four) && $position_four && $position_four == '1'}selected="selected"{/if}>{l s='Main Slider' mod='webapi'}</option>
            <option value="2"  {if isset($position_four) && $position_four && $position_four == '2'}selected="selected"{/if} >{l s='Top Categories' mod='webapi'}</option>
            <option value="3"  {if isset($position_four) && $position_four && $position_four == '3'}selected="selected"{/if} >{l s='New Products' mod='webapi'}</option>
            <option value="4"  {if isset($position_four) && $position_four && $position_four == '4'}selected="selected"{/if} >{l s='Popular Products' mod='webapi'}</option>
            <option value="5"  {if isset($position_four) && $position_four && $position_four == '5'}selected="selected"{/if} >{l s='Best Sale Products' mod='webapi'}</option>
            <option value="6"  {if isset($position_four) && $position_four && $position_four == '6'}selected="selected"{/if} >{l s='Grid Banner' mod='webapi'}</option>
            <option value="7"  {if isset($position_four) && $position_four && $position_four == '7'}selected="selected"{/if} >{l s='On Sale Products' mod='webapi'}</option>
         </select>
      </div>
      <div class="clearfix"></div>
   </div>
</div>



<div class="col-lg-6 form-group margin-form">
   <label class="form-group control-label col-lg-5">{l s='Position 5' mod='webapi'}</label>
   <div class="col-lg-5">
      <div class="col-lg-5">
         <select class="form-control fixed-width-xxl " name="position_five" id="position_five">
            <option value="1" {if isset($position_five) && $position_five && $position_five == '1'}selected="selected"{/if}>{l s='Main Slider' mod='webapi'}</option>
            <option value="2"  {if isset($position_five) && $position_five && $position_five == '2'}selected="selected"{/if} >{l s='Top Categories' mod='webapi'}</option>
            <option value="3"  {if isset($position_five) && $position_five && $position_five == '3'}selected="selected"{/if} >{l s='New Products' mod='webapi'}</option>
            <option value="4"  {if isset($position_five) && $position_five && $position_five == '4'}selected="selected"{/if} >{l s='Popular Products' mod='webapi'}</option>
            <option value="5"  {if isset($position_five) && $position_five && $position_five == '5'}selected="selected"{/if} >{l s='Best Sale Products' mod='webapi'}</option>
            <option value="6"  {if isset($position_five) && $position_five && $position_five == '6'}selected="selected"{/if} >{l s='Grid Banner' mod='webapi'}</option>
            <option value="7"  {if isset($position_five) && $position_five && $position_five == '7'}selected="selected"{/if} >{l s='On Sale Products' mod='webapi'}</option>
         </select>
      </div>
      <div class="clearfix"></div>
   </div>
</div>



<div class="col-lg-6 form-group margin-form">
   <label class="form-group control-label col-lg-5">{l s='Position 6' mod='webapi'}</label>
   <div class="col-lg-5">
      <div class="col-lg-5">
         <select class="form-control fixed-width-xxl " name="position_six" id="position_six">
            <option value="1" {if isset($position_six) && $position_six && $position_six == '1'}selected="selected"{/if}>{l s='Main Slider' mod='webapi'}</option>
            <option value="2"  {if isset($position_six) && $position_six && $position_six == '2'}selected="selected"{/if} >{l s='Top Categories' mod='webapi'}</option>
            <option value="3"  {if isset($position_six) && $position_six && $position_six == '3'}selected="selected"{/if} >{l s='New Products' mod='webapi'}</option>
            <option value="4"  {if isset($position_six) && $position_six && $position_six == '4'}selected="selected"{/if} >{l s='Popular Products' mod='webapi'}</option>
            <option value="5"  {if isset($position_six) && $position_six && $position_six == '5'}selected="selected"{/if} >{l s='Best Sale Products' mod='webapi'}</option>
            <option value="6"  {if isset($position_six) && $position_six && $position_six == '6'}selected="selected"{/if} >{l s='Grid Banner' mod='webapi'}</option>
            <option value="7"  {if isset($position_six) && $position_six && $position_six == '7'}selected="selected"{/if} >{l s='On Sale Products' mod='webapi'}</option>
         </select>
      </div>
      <div class="clearfix"></div>
   </div>
</div>



<div class="col-lg-6 form-group margin-form">
   <label class="form-group control-label col-lg-5">{l s='Position 7' mod='webapi'}</label>
   <div class="col-lg-5">
      <div class="col-lg-5">
         <select class="form-control fixed-width-xxl " name="position_seven" id="position_seven">
            <option value="1" {if isset($position_seven) && $position_seven && $position_seven == '1'}selected="selected"{/if}>{l s='Main Slider' mod='webapi'}</option>
            <option value="2"  {if isset($position_seven) && $position_seven && $position_seven == '2'}selected="selected"{/if} >{l s='Top Categories' mod='webapi'}</option>
            <option value="3"  {if isset($position_seven) && $position_seven && $position_seven == '3'}selected="selected"{/if} >{l s='New Products' mod='webapi'}</option>
            <option value="4"  {if isset($position_seven) && $position_seven && $position_seven == '4'}selected="selected"{/if} >{l s='Popular Products' mod='webapi'}</option>
            <option value="5"  {if isset($position_seven) && $position_seven && $position_seven == '5'}selected="selected"{/if} >{l s='Best Sale Products' mod='webapi'}</option>
            <option value="6"  {if isset($position_seven) && $position_seven && $position_seven == '6'}selected="selected"{/if} >{l s='Grid Banner' mod='webapi'}</option>
            <option value="7"  {if isset($position_seven) && $position_seven && $position_seven == '7'}selected="selected"{/if} >{l s='On Sale Products' mod='webapi'}</option>
         </select>
      </div>
      <div class="clearfix"></div>
   </div>
</div>
 <div class="clearfix"></div>
<div class="panel-footer">
      <button type="button" class="btn btn-info save_positions pull-right">
      <i class="process-icon-save"></i>
      {l s='Save' mod='webapi'}
      </button>
      <div id="alert_success_position" style="display: none;margin-top: 13px;">
         <div class="growl growl-notice growl-small">
            <div class="growl-title"></div>
            <div class="growl-message" style="margin-left: 20px;">  {l s='Saved Successfully' mod='webapi'}</div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
  $('.save_positions').click(function() {
    var one = $("#position_one").val();
    var two = $("#position_two").val();
    var three = $("#position_three").val();
    var four = $("#position_four").val();
    var five = $("#position_five").val();
    var six = $("#position_six").val();
    var seven = $("#position_seven").val();
    var ajax_url = $("#ajax_url").val();
    var token = $('#token').val();
        $.ajax({
            url: ajax_url,
            method: "post",
            data: {  token:token, one:one, two: two, three: three,four: four,five: five, six: six,seven: seven, action: 'savePosition' },
            dataType: "json",
            success: function(data) {
                $("#alert_success_position").show().delay(1500).fadeOut();
            }
        });
  });
</script>

