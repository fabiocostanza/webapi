{*
*  Web Api
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* @author    Fabio C.
* @copyright Copyright 2019 © Fabio C.
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  Fabio C.
* @package   WebApi
*}

<!-- allow upload -->
<div class="clearfix"></div>
<div class="panel" style="border-color: cadetblue;">
   <form method="POST" action="" enctype="multipart/form-data">
      <div class="col-lg-4">
         <button type="button" id="btn_slider_img" class="btn btn-info btn-lg" >{l s='Slider Images' mod='webapi'}</button>
         <button type="button" id="btn_slider_img_off" class="btn btn-danger btn-lg" >{l s='Close' mod='webapi'}</button>
      </div>
      <div class="col-lg-4">
         <button  id="btn_logo_img" type="button" class="btn btn-info btn-lg" >{l s='Logo Image' mod='webapi'}</button>
         <button type="button" id="btn_logo_img_off" class="btn btn-danger btn-lg" >{l s='Close' mod='webapi'}</button>
      </div>
      <div class="col-lg-4">
         <button type="button" id="btn_banner_img" class="btn btn-info btn-lg" >{l s='Grid Banner Images' mod='webapi'}</button>
         <button type="button"  id="btn_banner_img_off" class="btn btn-danger btn-lg" >{l s='Close' mod='webapi'}</button>
      </div>
      <div id="slider_img_div" class="col-lg-12" style="margin-top: 5%;">
         <div class="col-lg-4 form-group margin-form" style="text-align: center; overflow: hidden;">
            {if $slider1|escape:'htmlall':'UTF-8'}
            <img src="{$image_base_url|escape:'htmlall':'UTF-8'}{$slider1|escape:'htmlall':'UTF-8'}" width="250;" height="150px;">
            {else}
            <img id="logo_img" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;">
            {/if}

         </div>
         <div class="col-lg-4 form-group margin-form" style="text-align: center; overflow: hidden;">
            {if $slider2|escape:'htmlall':'UTF-8'}
            <img src="{$image_base_url|escape:'htmlall':'UTF-8'}{$slider2|escape:'htmlall':'UTF-8'}" width="250px;" height="150px;">
            {else}
            <img id="logo_img" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;">
            {/if}
         </div>
         <div class="col-lg-4 form-group margin-form" style="text-align: center; overflow: hidden;">
            {if $slider3|escape:'htmlall':'UTF-8'}
            <img src="{$image_base_url|escape:'htmlall':'UTF-8'}{$slider3|escape:'htmlall':'UTF-8'}" width="250px;" height="150px;">
            {else}
            <img id="logo_img" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;">
            {/if}
         </div>
         <input type="hidden" name="home_cat_image" id="home_cat_image" value="{$home_cat_image|escape:'htmlall':'UTF-8'}">
         <div class="clearfix"></div>
         <div class="input-group col-lg-4 custom_" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Slider 1 File  ' mod='webapi'}
            <input type="file" name="file" class="form-control-file" id="file" size="60" style="display: none;">
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>
         </div>
         <div class="input-group col-lg-4 custom_" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Slider 2 File  ' mod='webapi'}
            <input type="file" name="file1" class="form-control-file" id="file1" size="60" style="display: none;"> 
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>          
         </div>
         <div class="input-group col-lg-4 custom_" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Slider 3 File  ' mod='webapi'}
            <input type="file" name="file2" class="form-control-file" id="file2" size="60" style="display: none;"> 
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>          
         </div>
      </div>
      <div class="clearfix"></div>
      <div id="logo_img_div" style="margin-top: 5%;">
         <div class="col-lg-8 form-group margin-form" style="text-align: center; overflow: hidden;">
            {if $logo_img|escape:'htmlall':'UTF-8'}
            <img id="logo_img_src" src="{$logo_base_url|escape:'htmlall':'UTF-8'}{$logo_img|escape:'htmlall':'UTF-8'}" width="" height="150px;">
            {else}
            <img id="logo_img_sr" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;">
            {/if}
            <button type="button" class="btn btn-danger delete_logo_image">Delete</button>
            <p class="logo_success" style="display: none;color: green;">{l s='Successfully removed' mod='webapi'}</p>
         </div>
         <div class="input-group col-lg-4" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Logo File  ' mod='webapi'}
            <input type="file" name="logo" class="form-control-file" id="logo" size="60" style="display: none;"> 
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>          
         </div>
         <div class="clearfix"></div>
      </div>

      <div id="banner_img_div" style="margin-top: 5%; margin-left: 10%;">

         <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;margin-right: 2px;">
            <label id="banner_url"> {l s='Enter Banner 1 URL  ' mod='webapi'}</label>
            <input type="text" name="banner_url_1" value="{if $banner_url_1}{$banner_url_1|escape:'htmlall':'UTF-8'}{/if}">
         </div>
         <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;">
            <label id="banner_url"> {l s='Enter Banner 2 URL  ' mod='webapi'}</label>
            <input type="text" name="banner_url_2" value="{if $banner_url_2}{$banner_url_2|escape:'htmlall':'UTF-8'}{/if}">
         </div>
         <div class="clearfix"></div>
         <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;margin-right: 2px;">
            {if $banner_file_1|escape:'htmlall':'UTF-8'}
            <img id="banner_src_1" src="{$banner_base_url|escape:'htmlall':'UTF-8'}{$banner_file_1|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;">
            {else}
            <img id="logo_img" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;">
            {/if}
            <div class="input-group custom_" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Banner 1 File  ' mod='webapi'}
            <input type="file" name="banner_file_1" class="form-control-file" id="banner_file_1" size="60" style="display: none;"> 
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>          
            </div>
            <button type="button" id="1" class="banner_img_1 btn btn-danger delete_grid_image">Delete</button>
            <p class="banner_img_1_success" style="display: none;color: green;">{l s='Successfully removed' mod='webapi'}</p>
         </div>
         <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;">
            {if $banner_file_2|escape:'htmlall':'UTF-8'}
            <img id="banner_src_2" src="{$banner_base_url|escape:'htmlall':'UTF-8'}{$banner_file_2|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;">
            {else}
            <img id="logo_img" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;">
            {/if}
            <div class="input-group custom_" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Banner 2 File  ' mod='webapi'}
            <input type="file" name="banner_file_2" class="form-control-file" id="banner_file_2" size="60" style="display: none;"> 
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>          
            </div>
            <button type="button" id="2" class="banner_img_2 btn btn-danger delete_grid_image">Delete</button>
            <p class="banner_img_2_success" style="display: none;color: green;">{l s='Successfully removed' mod='webapi'}</p>
         </div>
         <div class="clearfix"></div>
         <hr class="line"></hr>
         <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;margin-right: 2px;">
            <label id="banner_url"> {l s='Enter Banner 3 URL  ' mod='webapi'}</label>
            <input type="text" name="banner_url_3" value="{if $banner_url_3}{$banner_url_3|escape:'htmlall':'UTF-8'}{/if}">
         </div>
         <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;">
            <label id="banner_url"> {l s='Enter Banner 4 URL  ' mod='webapi'}</label>
            <input type="text" name="banner_url_4" value="{if $banner_url_4}{$banner_url_4|escape:'htmlall':'UTF-8'}{/if}">
         </div>
         <div class="clearfix"></div>
          <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;margin-right: 2px;">
            {if $banner_file_3|escape:'htmlall':'UTF-8'}
            <img id="banner_src_3" src="{$banner_base_url|escape:'htmlall':'UTF-8'}{$banner_file_3|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;">
            {else}
            <img id="logo_img" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;">
            {/if}
            <div class="input-group custom_" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Banner 3 File  ' mod='webapi'}
            <input type="file" name="banner_file_3" class="form-control-file" id="banner_file_3" size="60" style="display: none;"> 
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>          
            </div>
            <button type="button" id="3" class="banner_img_3 btn btn-danger delete_grid_image">Delete</button>
            <p class="banner_img_3_success" style="display: none;color: green;">{l s='Successfully removed' mod='webapi'}</p>
         </div>
         <div class="col-lg-6 form-group margin-form" style="text-align: center; overflow: hidden;">
            {if $banner_file_4|escape:'htmlall':'UTF-8'}
            <img id="banner_src_4" src="{$banner_base_url|escape:'htmlall':'UTF-8'}{$banner_file_4|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;">
            {else}
            <img id="logo_img" src="{$home_cat_image|escape:'htmlall':'UTF-8'}" width="250;" height="150px;" style="float: left;" >
            {/if}
            <div class="input-group custom_" style="padding-top: 7%;">
            <span class="input-group-addon"><i class="icon-file"></i></span>
            <label id="banner1"> {l s='Upload Banner 4 File  ' mod='webapi'}
            <input type="file" name="banner_file_4" class="form-control-file" id="banner_file_4" size="60" style="display: none;"> 
            <i class="icon-folder-open" style="padding-left: inherit;"></i></label>          
            </div>
            <button type="button" id="4" class="banner_img_4 btn btn-danger delete_grid_image">Delete</button>
            <p class="banner_img_4_success" style="display: none;color: green;">{l s='Successfully removed' mod='webapi'}</p>
         </div>
      </div>
      <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="panel-footer">
<button type="submit" class="btn btn-default pull-right">
<i class="process-icon-save"></i> {l s='Save' mod='webapi'}
</button>
</div>
</form>
</div>
<style type="text/css">
   .custom_{
   float: inherit !important;
   }
</style>
<script type="text/javascript">
   $('#logo_img_div').hide();
   $('#slider_img_div').hide();
   $('#banner_img_div').hide();
   $('#btn_logo_img_off').hide();
   $('#btn_banner_img_off').hide();
   $('#btn_slider_img_off').hide();
   
   $(document).ready(function() {
      $('#btn_slider_img').click(function() {
         $('#logo_img_div').hide();
         $('#banner_img_div').hide();
         $('#btn_banner_img_off').hide();
         $('#btn_logo_img_off').hide();
         $('#slider_img_div').show();
         $('#btn_slider_img_off').show();
      });
      $('#btn_logo_img').click(function() {
         $('#slider_img_div').hide();
         $('#btn_slider_img_off').hide();
         $('#banner_img_div').hide();
         $('#btn_banner_img_off').hide();
         $('#logo_img_div').show();
         $('#btn_logo_img_off').show();
      });
   
      $('#btn_slider_img_off').click(function() {
         $('#slider_img_div').hide();
         $('#btn_slider_img_off').hide();
      });
      $('#btn_logo_img_off').click(function() {
         $('#logo_img_div').hide();
         $('#btn_logo_img_off').hide();
      });
   
      $('#btn_banner_img_off').click(function() {
         $('#banner_img_div').hide();
         $('#btn_banner_img_off').hide();
      });
   
      $('#btn_banner_img').click(function() {
         $('#slider_img_div').hide();
         $('#btn_slider_img_off').hide();
         $('#logo_img_div').hide();
         $('#btn_logo_img_off').hide();
         $('#banner_img_div').show();
         $('#btn_banner_img_off').show();
      });

   });

   $('.delete_grid_image').click(function() {
      var img_id = $(this).attr("id");
      var ajax_url = $("#ajax_url").val();
      var home_cat_image = $("#home_cat_image").val();
      var token = $('#token').val();
      $.ajax({
            url: ajax_url,
            method: "post",
            data: { token: token, img_id:img_id, action: 'delGridImg' },
            dataType: "json",
            success: function(data) {
                $(".banner_img_"+img_id).hide();
                $("#banner_src_"+img_id).attr("src",home_cat_image);
               $(".banner_img_"+img_id+"_success").show().delay(1500).fadeOut();
            }
      });
    });
   $('.delete_logo_image').click(function() {
      var ajax_url = $("#ajax_url").val();
      var home_cat_image = $("#home_cat_image").val();
      var token = $('#token').val();
      $.ajax({
            url: ajax_url,
            method: "post",
            data: { token: token, action: 'delLogoImg' },
            dataType: "json",
            success: function(data) {
                $(".delete_logo_image").hide();
                $("#logo_img_src").attr("src",home_cat_image);
               $(".logo_success").show().delay(1500).fadeOut();
            }
      });
    });

</script>
<style type="text/css">
   #banner1{
   padding: 10px;
   background: #c8b9b9; 
   display: table;
   color: #fff;
   margin-bottom: auto;
   }
   #banner_url{
   padding: 10px;
   background: #efefef; 
   display: table;
   color: #6e6767;
   margin-bottom: auto;
   }
   .line {
     border: 0;
     background-color: #000;
     height: 3px;
     cursor: pointer;
   }
</style>
