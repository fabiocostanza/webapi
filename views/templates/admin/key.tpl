{*
*  Web Api
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* @author    Fabio C.
* @copyright Copyright 2019 © Fabio C.
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  Fabio C.
* @package   WebApi
*}

<div class="form-wrapper panel" id="edit_form" style="border-color: cadetblue;">
   <div class="form-group">
      <label class="control-label col-lg-3 required">
      <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="Webservice account key.">
      {l s='Key' mod='webapi'}
      </span>
      </label>
      <div class="col-lg-9">
         <div class="row">
            <div class="col-lg-9">
               <input type="text" name="key" id="key" value="" class="">
            </div>
            <div class="col-lg-2">
               <input type="button" class="button" value="Generate" onClick="generate();" tabindex="2">
            </div>
         </div>
      </div>
   </div>
   <div class="form-group hide">
      <input type="hidden" name="id_webapi" id="id_webapi" value="">
   </div>
   <div class="form-group">
      <label class="control-label col-lg-3">
      {l s='URL' mod='webapi'}
      </label>
      <div class="col-lg-9">
         <input type="text" name="url_api" id="url_api" value="" class="" readonly="readonly" placeholder="{$base_dir|escape:'htmlall':'UTF-8'}WebApi">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-lg-3">
      <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="Quick description of the key: who it is for, what permissions it has, etc.">
      {l s='Key description' mod='webapi'}
      </span>
      </label>
      <div class="col-lg-9">
         <textarea name="description" id="description" cols="110" rows="3" class="textarea-autosize" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 75px;"></textarea>
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-lg-3">
      {l s='Status' mod='webapi'}
      </label>
      <div class="col-lg-9">
         <span class="switch prestashop-switch fixed-width-lg">
         <input type="radio" name="status" id="status_on" value="1" checked="check">
         <label for="status_on">Yes</label>
         <input type="radio" name="status" id="status_off" value="0">
         <label for="status_off">No</label>
         <a class="slide-button btn"></a>
         </span>
      </div>
   </div>
   <div class="form-group">
      <button type="button" id="hide_Form" onclick="getRecord()" class="btn btn-danger" style="float: right;margin-right: 10px;">{l s='Close Form' mod='webapi'}</button>
      <button type="button" id="save_form" class="btn btn-success save_form" style="float: right;margin-right: 10px;">{l s='Save Form' mod='webapi'}</button>
      <button type="button" id="reset_form" onclick="resetForm()" class="btn btn-warning save_form" style="float: right;margin-right: 10px;">{l s='Reset Form' mod='webapi'}</button>
      
      <div id="error_alert_1" style="width: 25%;display: none;" class="alert alert-success" role="alert">
        {l s='Successfully Added!' mod='webapi'}
      </div>
      <div id="error_alert_0" style="width: 25%;display: none;" class="alert alert-danger" role="alert">
        {l s='You Must Fill All Field(s).' mod='webapi'}
      </div>
      <div id="error_alert_2" style="width: 25%;display: none;" class="alert alert-success" role="alert">
        {l s='Successfully Updated!' mod='webapi'}
      </div>

   </div>
</div>
<div class="panel col-lg-12" id="update_list_ajax" style="border-color: cadetblue;">
   <input type="hidden" name="ajax_url" id="ajax_url" value="{$ajax_url|escape:'htmlall':'UTF-8'}">
   <div class="table-responsive-row clearfix">
      <table id="table-webapi" class="table webapi">
         <thead>
            <tr class="nodrag nodrop">
               <th class="">
                  <span>
                  {l s='ID' mod='webapi'}
                  </span>
               </th>
               <th>
                  <span>
                  {l s='Key' mod='webapi'}
                  </span>
               </th>
               <th>
                  <span>
                  {l s='Status' mod='webapi'}
                  </span>
               </th>
               <th>
                  <span>
                  {l s='Description' mod='webapi'}
                  </span>
               </th>
               <th>
                  <span>
                  <a href="javascript:addNewRecord();">
                  <button type="button" class="btn btn-success btn-xs">
                  <i class="material-icons" style="padding-top: 3px;">add_circle_outline</i>
                  </button>  
                  </a>
                  <button type="button" class="btn btn-xs update_list" style="    margin-left: 15px;">
                  <i class="material-icons" style="padding-top: 3px;">autorenew</i>
                  </button>  
                  </span>
               </th>
            </tr>
         </thead>
         <tbody>
            {foreach from=$record key=myId item=i}
            <tr class=" odd" id="id_{$i.id_webapi|escape:'htmlall':'UTF-8'}">
               <td>
                  {$i.id_webapi|escape:'htmlall':'UTF-8'}
               </td>
               <td>
                  {$i.key|escape:'htmlall':'UTF-8'}
               </td>
               <td>
                  {if $i.status == '1'}
                  <i class="material-icons action-enabled ">check</i>
                  {else}
                  <i class="material-icons action-disabled">clear</i>
                  {/if} 
               </td>
               <td>
                  {$i.description|escape:'htmlall':'UTF-8'}
               </td>
               <td>
                  <button type="button" id="{$i.id_webapi|escape:'htmlall':'UTF-8'}" key="{$i.key|escape:'htmlall':'UTF-8'}" status="{$i.status|escape:'htmlall':'UTF-8'}" description="{$i.description|escape:'htmlall':'UTF-8'}" class="btn btn-info edit_Form_data">{l s='Edit' mod='webapi'}</button>
                  <button type="button" id="{$i.id_webapi|escape:'htmlall':'UTF-8'}" class="btn btn-danger delete_Form_data">{l s='Delete' mod='webapi'}</button>
               </td>
            </tr>
            {/foreach}
         </tbody>
      </table>
   </div>
</div>
<style type="text/css">
   #edit_form{
   display: none;
   }
</style>
<script type="text/javascript">
   var aSelected = [];
   var asInitVals = new Array();
   var all_label = "{l s='All' mod='webapi' js=1}";
   var check_all_label = "{l s='Check All' mod='webapi' js=1}";
   var uncheck_all_label = "{l s='Uncheck All' mod='webapi' js=1}";
   var currentFormTab = "{if isset($smarty.post.currentFormTab)}{$smarty.post.currentFormTab|escape:'htmlall':'UTF-8'}{else}settings{/if}";
   var del_conf_text = "{l s='Are you sure you want to delete?' mod='webapi' js=1}";
   
   $('.web_api_tab').hide();
   $('.tab-page').removeClass('selected');
   $('#web_api_' + currentFormTab).show();
   $('#web_api_link_' + currentFormTab).addClass('selected');
   
   
   function addNewRecord() {
    $("input[type=text], textarea").val("");
     $("#id_webapi").val('');
     $('#edit_form').show();
     
   }
   
   function resetForm() {
     $("input[type=text], textarea").val("");
     $("#id_webapi").val('');
     
   }
   
   
   function displayNavTab(tab) {
       $('.web_api_tab').hide();
       $('.tab-page').removeClass('selected');
       $('#web_api_' + tab).show();
       $('#web_api_link_' + tab).addClass('selected');
       $('#currentFormTab').val(tab);
   }
   
   function htmlEncode(input) {
     return String(input).replace(/&amp;/g, '&');
   }
   
   
   function getRecord(id) {
     $('#edit_form').hide();
   
   }
   function generate() {
        $("#key").val(randomPassword(32));
     }
     function randomPassword(length) {
         var chars = "ABCDEFGHIJKLMNOP1234567890";
         var pass = "";
         for (var x = 0; x < length; x++) {
             var i = Math.floor(Math.random() * chars.length);
             pass += chars.charAt(i);
         }
         return pass;
     }
   $(document).ready(function() {
     $('.edit_Form_data').click(function() {
        var id = $(this).attr("id");
        var key = $(this).attr("key");
        var status = $(this).attr("status");
        var description = $(this).attr("description");
        $('#edit_form').show();
        $("#id_webapi").val(id);
        $("#key").val(key);
        
        $("#description").val(description);
        if (status == 1) {
           jQuery("#status_on").attr('checked', true);
        } else {
           jQuery("#status_off").attr('checked', true);
        }
     });
   
   
     $('.delete_Form_data').click(function() {
        var id = $(this).attr("id");
        var ajax_url = $("#ajax_url").val();
        var token = $('#token').val();
   
           $.ajax({
              url: ajax_url,
              method: "post",
              data: { token: token, id: id, action: 'deleteRecord' },
              dataType: "json",
              success: function(data) {
                 if (data == true) {
                   var ptr = $('#'+id).closest("tr").remove();
               }
              }
           });
   
     });
   
     $('.update_list').click(function() {
       var ajax_url = $("#ajax_url").val();
        var token = $('#token').val();
           $.ajax({
              url: ajax_url,
              method: "post",
              data: { token: token, action: 'update_list' },
              dataType: "json",
              success: function(data) {
                 $('#update_list_ajax').html(data);
              }
           });
   
     });
   
     $('.save_form').click(function() {
        var id = $("#id_webapi").val();
        var key = $("#key").val();
        var description = $("#description").val();
        var status = $("input[name='status']:checked").val();
        var ajax_url = $("#ajax_url").val();
        var token = $('#token').val();
           $.ajax({
              url: ajax_url,
              method: "post",
              data: {  token: token, id: id, key: key, description: description, status: status, action: 'updateRecord' },
              dataType: "json",
              success: function(data) {
                if (data == 1) {
                    $("#error_alert_1").show().delay(1500).fadeOut();
                } else if (data == 2) {
                  $("#error_alert_2").show().delay(1500).fadeOut();
                } else if (data == 0) {
                  $("#error_alert_0").show().delay(1500).fadeOut();
                }
              }
           });
   
     });
   });
   
</script>
