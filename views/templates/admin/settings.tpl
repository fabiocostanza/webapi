{*
*  Web Api
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* @author    Fabio C.
* @copyright Copyright 2019 © Fabio C.
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  Fabio C.
* @package   WebApi
*}

<div class="panel" style="border-color: cadetblue;">
   <div class="col-lg-6 form-group margin-form">
      <!-- <label class="form-group control-label col-lg-5">{l s='Enable Error Log' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="error_log_status" id="error_log_status_on" value="1" {if isset($error_log_status_on) AND $error_log_status_on == 1}checked="checked"{/if} disabled>
            <label for="error_log_status_on">Yes</label>
            <input type="radio" name="error_log_status" id="error_log_status_off" value="0" {if isset($error_log_status_on) AND $error_log_status_on == 0}checked="checked"{/if} disabled>
            <label for="error_log_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div> -->
   </div>
   <div class="col-lg-6 form-group margin-form" style="overflow: hidden;">
      <label class="form-group control-label col-lg-5">{l s='Theme Color' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <input type="color" class="input-color" name="theme_color" value="{$theme_color|escape:'htmlall':'UTF-8'}" id="theme_color" />
         </div>
         <div class="clearfix"></div>
      </div>

   </div>
   <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable Slider' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="slider_status" id="slider_status_on" value="1" {if isset($slider_status_on) AND $slider_status_on == 1}checked="checked"{/if}>
            <label for="slider_status_on">Yes</label>
            <input type="radio" name="slider_status" id="slider_status_off" value="0" {if isset($slider_status_on) AND $slider_status_on == 0}checked="checked"{/if}>
            <label for="slider_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="col-lg-6 form-group margin-form" style="float: left;">
      <div class="col-lg-11 text-center">
         <button type="button" class="btn btn-success" style="width: 140px;background-color: #2eacce;">
         <a class="tab-page" id="web_api_link_images" href="javascript:displayNavTab('images');" style="color: white;">
         {l s='Upload Slider Files' mod='webapi'}
         </a>
         </button>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable Mobile Logo' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="logo_status" id="logo_status_on" value="1" {if isset($logo_status_on) AND $logo_status_on == 1}checked="checked"{/if}>
            <label for="logo_status_on">Yes</label>
            <input type="radio" name="logo_status" id="logo_status_off" value="0" {if isset($logo_status_on) AND $logo_status_on == 0}checked="checked"{/if}>
            <label for="logo_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="col-lg-6 form-group margin-form" style="float: left;">
      <div class="col-lg-11 text-center">
         <button type="button" class="btn btn-success" style="width: 140px;background-color: #2eacce;">
         <a class="tab-page" id="web_api_link_images" href="javascript:displayNavTab('images');" style="color: white;">
         {l s='Upload Logo File' mod='webapi'}
         </a>
         </button>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable New Products' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="new_products_status" id="new_products_status_on" value="1" {if isset($new_products_status_on) AND $new_products_status_on == 1}checked="checked"{/if}>
            <label for="new_products_status_on">Yes</label>
            <input type="radio" name="new_products_status" id="new_products_status_off" value="0" {if isset($new_products_status_on) AND $new_products_status_on == 0}checked="checked"{/if}>
            <label for="new_products_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable Popular Products' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="popular_products_status" id="popular_products_status_on" value="1" {if isset($popular_products_status_on) AND $popular_products_status_on == 1}checked="checked"{/if}>
            <label for="popular_products_status_on">Yes</label>
            <input type="radio" name="popular_products_status" id="popular_products_status_off" value="0" {if isset($popular_products_status_on) AND $popular_products_status_on == 0}checked="checked"{/if}>
            <label for="popular_products_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable Best Sale Products' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="best_products_status" id="best_products_status_on" value="1" {if isset($best_products_status_on) AND $best_products_status_on == 1}checked="checked"{/if}>
            <label for="best_products_status_on">Yes</label>
            <input type="radio" name="best_products_status" id="best_products_status_off" value="0" {if isset($best_products_status_on) AND $best_products_status_on == 0}checked="checked"{/if}>
            <label for="best_products_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable ON Sale Products' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="onsale_products_status" id="onsale_products_status_on" value="1" {if isset($onsale_products_status_on) AND $onsale_products_status_on == 1}checked="checked"{/if}>
            <label for="onsale_products_status_on">Yes</label>
            <input type="radio" name="onsale_products_status" id="onsale_products_status_off" value="0" {if isset($onsale_products_status_on) AND $onsale_products_status_on == 0}checked="checked"{/if}>
            <label for="onsale_products_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
<div class="clearfix"></div>
    <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable Grid Banner' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="grid_banner_status" id="grid_banner_status_on" value="1" {if isset($grid_banner_status_on) AND $grid_banner_status_on == 1}checked="checked"{/if}>
            <label for="grid_banner_status_on">Yes</label>
            <input type="radio" name="grid_banner_status" id="grid_banner_status_off" value="0" {if isset($grid_banner_status_on) AND $grid_banner_status_on == 0}checked="checked"{/if}>
            <label for="grid_banner_status_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="col-lg-6 form-group margin-form" style="float: left;">
      <div class="col-lg-11 text-center">
         <button type="button" class="btn btn-success" style="width: 140px;background-color: #2eacce;">
         <a class="tab-page" id="web_api_link_images" href="javascript:displayNavTab('images');" style="color: white;">
         {l s='Upload Banners Files' mod='webapi'}
         </a>
         </button>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="col-lg-6 form-group margin-form">
      <!-- <label class="form-group control-label col-lg-5">{l s='Advance Category Screen' mod='webapi'}</label>
      <div class="col-lg-5">
         <div class="col-lg-5">
            <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="advance_category_slider" id="advance_category_slider_on" value="1" {if isset($advance_category_slider_on) AND $advance_category_slider_on == 1}checked="checked"{/if}>
            <label for="advance_category_slider_on">Yes</label>
            <input type="radio" name="advance_category_slider" id="advance_category_slider_off" value="0" {if isset($advance_category_slider_on) AND $advance_category_slider_on == 0}checked="checked"{/if}>
            <label for="advance_category_slider_off">No</label>
            <a class="slide-button btn"></a>
            </span>
         </div>
         <div class="clearfix"></div>
      </div> -->
   </div>

   <div class="clearfix"></div>
   <div class="col-lg-6 form-group margin-form">
      <label class="form-group control-label col-lg-5">{l s='Enable Category Slider' mod='webapi'}</label>
      <input type="hidden" name="ajax_url" id="ajax_url" value="{$ajax_url|escape:'htmlall':'UTF-8'}">
      <div class="col-lg-7">
         <span class="switch prestashop-switch fixed-width-lg">
         <input type="radio" name="cat_slider_status" id="cat_slider_status_on" value="1" {if isset($cat_slider_status_on) AND $cat_slider_status_on == 1}checked="checked"{/if}>
         <label for="cat_slider_status_on">Yes</label>
         <input type="radio" name="cat_slider_status" id="cat_slider_status_off" value="0" {if isset($cat_slider_status_on) AND $cat_slider_status_on == 0}checked="checked"{/if}>
         <label for="cat_slider_status_off">No</label>
         <a class="slide-button btn"></a>
         </span>
      </div>
   </div>
   <div class="col-lg-6 form-group margin-form" style="float: left;">
      <div class="col-lg-11 text-center">
         <button type="button" id="select_categories" class="btn btn-success" style="width: 140px;background-color: #2eacce;">{l s='Update Categories List' mod='webapi'}</button>
         <button type="button" id="close_categories" class="btn btn-danger" >{l s='Close Categories List' mod='webapi'}</button>
      </div>
   </div>
   <div class="clearfix"></div>

    <div class="clearfix"></div>
   <div class="category_tree_box">
      {$category_tree} <!-- html content -->
   </div>
   <div class="panel-footer">
      <button type="button" class="btn btn-info save_settings pull-right">
      <i class="process-icon-save"></i>
      {l s='Save' mod='webapi'}
      </button>
      <div id="_alert_success" style="display: none;margin-top: 13px;">
         <div class="growl growl-notice growl-small">
            <div class="growl-title"></div>
            <div class="growl-message" style="margin-left: 20px;">  {l s='Saved Successfully' mod='webapi'}</div>
         </div>
      </div>
   </div>
</div>
<input type="hidden" name="ajax_url" id="ajax_url" value="{$ajax_url|escape:'htmlall':'UTF-8'}">
<input type="hidden" name="token" id="token" value="{$token|escape:'htmlall':'UTF-8'}">
<style type="text/css">
   .input-color{
   margin-top: 5px;
   }
   #api_logo{
   margin-top: 4px;
   }
   #theme_color{
   margin-top: 1px;
   width: 150%;
   height: 30px;
   }
   .inputfile_logo {
   width: 0.1px;
   height: 0.1px;
   opacity: 0;
   overflow: hidden;
   position: absolute;
   z-index: -1;
   }
   .inputfile_logo + label {
   margin-top: 6px;
   font-size: 1.25em;
   font-weight: 700;
   color: white;
   background-color: #1acad1;
   display: inline-block;
   width: 100%;
   text-align: center;
   }
   .inputfile_logo:focus + label,
   .inputfile_logo + label:hover {
   background-color: #1b66e7;
   }
   .inputfile_logo + label {
   cursor: pointer; /* "hand" cursor */
   }
   
</style>
<script type="text/javascript">
   $('.category_tree_box').hide();
   $('#close_categories').hide();
   
   $(document).ready(function() {
    $('#select_categories').click(function() {
       $('.category_tree_box').show();
       $('#select_categories').hide();
       $('#close_categories').show();
    });
   
      $('#close_categories').click(function() {
          $('.category_tree_box').hide();
          $('#select_categories').show();
          $('#close_categories').hide();
       });
   
   });
   
    $('.save_settings').click(function() {
   
       var cat_slider_status = $("input[name='cat_slider_status']:checked").val();
       var favorite = [];
       $.each($("input[type='checkbox']:checked"), function(){           
          favorite.push($(this).val());
       });
   
        var theme_Color = $("#theme_color").val();
        // var logo_file = $(".inputfile_logo").val();
        var error_log_status = $("input[name='error_log_status']:checked").val();
        var slider_status = $("input[name='slider_status']:checked").val();
        var logo_status = $("input[name='logo_status']:checked").val();
        var grid_banner_status = $("input[name='grid_banner_status']:checked").val();
   
        var new_products_status = $("input[name='new_products_status']:checked").val();
        var popular_products_status = $("input[name='popular_products_status']:checked").val();
        var best_products_status = $("input[name='best_products_status']:checked").val();
        var onsale_products_status = $("input[name='onsale_products_status']:checked").val();
        var advance_category_slider = $("input[name='advance_category_slider']:checked").val();
   
        var ajax_url = $("#ajax_url").val();
        var token = $('#token').val();
        $.ajax({
            url: ajax_url,
            method: "post",
            data: { cat_slider_status:cat_slider_status, favorite: favorite, best_products_status: best_products_status,onsale_products_status: onsale_products_status,advance_category_slider: advance_category_slider,popular_products_status: popular_products_status, new_products_status: new_products_status,theme_Color: theme_Color, error_log_status: error_log_status,slider_status: slider_status,logo_status: logo_status, grid_banner_status: grid_banner_status,token: token, action: 'saveSettings' },
            dataType: "json",
            success: function(data) {
              if (data == 1) {
                $("#_alert_success").show().delay(1500).fadeOut();
              }
            }
        });       
    
    });
    
</script>
