{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
<form method="POST" action="" enctype="multipart/form-data">
{foreach from=$languages item=language}
  {if $languages|count > 1}
      <div class="translatable-field lang-{$language.id_lang|escape:'htmlall':'UTF-8'}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
  {/if}
  
<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Home' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="home_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="home_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($home)}{$home[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Categories' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="categories_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="categories_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($categories)}{$categories[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Cart' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="cart_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="cart_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($cart)}{$cart[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Account' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="account_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="account_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($account)}{$account[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='View All' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="viewAll_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="viewAll_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($viewAll)}{$viewAll[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Search' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="search_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="search_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($search)}{$search[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='All Categories' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="allCategories_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="allCategories_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($allCategories)}{$allCategories[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='New Products' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="newProducts_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="newProducts_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($newProducts)}{$newProducts[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Best Sellers' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="bestSellers_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="bestSellers_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($bestSellers)}{$bestSellers[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Popular Products' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="popularProducts_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="popularProducts_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($popularProducts)}{$popularProducts[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Sale Products' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="saleProducts_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="saleProducts_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($saleProducts)}{$saleProducts[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Sign In' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="signIn_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="signIn_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($signIn)}{$signIn[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Sign Out' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="signOut_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="signOut_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($signOut)}{$signOut[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Profile' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="profile_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="profile_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($profile)}{$profile[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Address' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="address_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="address_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($address)}{$address[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='vouchers' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="vouchers_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="vouchers_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($vouchers)}{$vouchers[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Orders History' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="ordersHistory_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="ordersHistory_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($ordersHistory)}{$ordersHistory[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Currency' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="currency_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="currency_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($currency)}{$currency[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Version' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="version_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="version_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($version)}{$version[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Login' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="login_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="login_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($login)}{$login[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Forgot Password' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="forgotPassword_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="forgotPassword_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($forgotPassword)}{$forgotPassword[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='sign Up' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="signUp_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="signUp_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($signUp)}{$signUp[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='have Not An Account' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="haveNotAnAccount_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="haveNotAnAccount_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($haveNotAnAccount)}{$haveNotAnAccount[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Password' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="password_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="password_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($password)}{$password[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='New Password' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="newPassword_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="newPassword_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($newPassword)}{$newPassword[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Confirm Password' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="confirmPassword_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="confirmPassword_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($confirmPassword)}{$confirmPassword[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Submit' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="submit_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="submit_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($submit)}{$submit[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='First Name' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="firstName_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="firstName_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($firstName)}{$firstName[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Last Name' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="lastName_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="lastName_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($lastName)}{$lastName[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Register' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="register_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="register_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($register)}{$register[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Have An Account' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="haveAnAccount_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="haveAnAccount_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($haveAnAccount)}{$haveAnAccount[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Buy' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="buy_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="buy_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($buy)}{$buy[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>
<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Shipping Address' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="shippingAddress_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="shippingAddress_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($shippingAddress)}{$shippingAddress[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>
<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Shipping Methods' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="shippingMethods_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="shippingMethods_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($shippingMethods)}{$shippingMethods[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Payment Methods' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="paymentMethods_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="paymentMethods_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($paymentMethods)}{$paymentMethods[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>
<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Place Order' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="placeOrder_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="placeOrder_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($placeOrder)}{$placeOrder[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Confirmation' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="confirmation_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="confirmation_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($confirmation)}{$confirmation[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Add To Cart' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="addToCart_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="addToCart_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($addToCart)}{$addToCart[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Out Of Stock' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="outOfStock_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="outOfStock_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($outOfStock)}{$outOfStock[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Quantity' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="quantity_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="quantity_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($quantity)}{$quantity[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Contact Name' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="contactName_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="contactName_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($contactName)}{$contactName[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Gender' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="gender_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="gender_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($gender)}{$gender[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Date Of Birth' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="dateOfBirth_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="dateOfBirth_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($dateOfBirth)}{$dateOfBirth[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='My Profile' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="myProfile_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="myProfile_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($myProfile)}{$myProfile[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>
<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Update' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="update_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="update_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($update)}{$update[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Add New Address' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="addNewAddress_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="addNewAddress_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($addNewAddress)}{$addNewAddress[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Alias' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="alias_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="alias_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($alias)}{$alias[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Company' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="company_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="company_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($company)}{$company[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Vat Number' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="vatNumber_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="vatNumber_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($vatNumber)}{$vatNumber[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Address Complement' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="addressComplement_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="addressComplement_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($addressComplement)}{$addressComplement[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Zip Postal Code' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="zipPostalCode_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="zipPostalCode_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($zipPostalCode)}{$zipPostalCode[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Phone' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="phone_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="phone_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($phone)}{$phone[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='City' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="city_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="city_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($city)}{$city[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Country' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="country_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="country_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($country)}{$country[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='My Orders' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="myOrders_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="myOrders_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($myOrders)}{$myOrders[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Order' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="order_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="order_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($order)}{$order[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Order Details' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="orderDetails_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="orderDetails_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($orderDetails)}{$orderDetails[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Billing Address' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="billingAddress_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="billingAddress_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($billingAddress)}{$billingAddress[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>
<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Continue Shopping' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="continueShopping_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="continueShopping_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($continueShopping)}{$continueShopping[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Proceed To Checkout' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="proceedToCheckout_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="proceedToCheckout_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($proceedToCheckout)}{$proceedToCheckout[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Close' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="close_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="close_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($close)}{$close[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Language' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="languageName_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="languageName_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($languageName)}{$languageName[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Email' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="email_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="email_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($email)}{$email[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Voucher Code' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="voucherCode_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="voucherCode_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($voucherCode)}{$voucherCode[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='In stock' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="in_stock_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="in_stock_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($in_stock)}{$in_stock[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Total (tax incl)' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="total_tax_incl_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="total_tax_incl_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($total_tax_incl)}{$total_tax_incl[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Tax incl' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="tax_incl_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="tax_incl_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($tax_incl)}{$tax_incl[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Your order is confirmed' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="order_is_confirmed_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="order_is_confirmed_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($order_is_confirmed)}{$order_is_confirmed[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Total Products' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="total_products_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="total_products_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($total_products)}{$total_products[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Total Shipping' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="total_shipping_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="total_shipping_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($total_shipping)}{$total_shipping[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Total Vouchers' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="total_vouchers_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="total_vouchers_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($total_vouchers)}{$total_vouchers[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Please enter your' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="please_enter_your_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="please_enter_your_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($please_enter_your)}{$please_enter_your[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Incorrect' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control"  type="text" name="incorrect_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="incorrect_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($incorrect)}{$incorrect[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Error' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="eror_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="eror_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($eror)}{$eror[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Something Went Wrong!' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="something_went_wrong_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="something_went_wrong_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($something_went_wrong)}{$something_went_wrong[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Please Try Again' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="try_again_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="try_again_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($try_again)}{$try_again[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>
<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Please Select' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="please_select_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="please_select_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($please_select)}{$please_select[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Congratulations' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="congratulations_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="congratulations_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($congratulations)}{$congratulations[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Product is successfully added into cart' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="added_into_cart_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="added_into_cart_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($added_into_cart)}{$added_into_cart[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Order Reference' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="order_reference_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="order_reference_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($order_reference)}{$order_reference[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Order Status' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="order_status_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="order_status_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($order_status)}{$order_status[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Please check back later' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="check_back_later_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="check_back_later_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($check_back_later)}{$check_back_later[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Currently there are no item' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="there_are_no_item_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="there_are_no_item_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($there_are_no_item)}{$there_are_no_item[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Session Expired' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="session_expired_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="session_expired_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($session_expired)}{$session_expired[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Account Registered' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="account_registered_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="account_registered_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($account_registered)}{$account_registered[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Reset Email Sent' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="reset_mail_send_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="reset_mail_send_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($reset_mail_send)}{$reset_mail_send[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Placed On' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="placed_on_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="placed_on_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($placed_on)}{$placed_on[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Valid Till' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="valid_till_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="valid_till_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($valid_till)}{$valid_till[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Voucher Value' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="voucher_value_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="voucher_value_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($voucher_value)}{$voucher_value[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='Minimum Purchase' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="minimum_purchase_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="minimum_purchase_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($minimum_purchase)}{$minimum_purchase[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>


<div class="form-group row col-lg-6">
   <label for="example-text-input" class="col-lg-12 col-form-label">{l s='No Of Vouchers' mod='webapi'}</label>
   <div class="col-lg-10">
    
      <input class="form-control" type="text" name="noof_vouchers_{$language.id_lang|escape:'htmlall':'UTF-8'}"  id="noof_vouchers_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if !empty($noof_vouchers)}{$noof_vouchers[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" >
   </div>
   {if $languages|count > 1}
          <div class="col-lg-2">
              <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                  {$language.iso_code|escape:'htmlall':'UTF-8'}
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                  {foreach from=$languages item=lang}
                  <li><a class="currentLang" data-id="{$lang.id_lang|escape:'htmlall':'UTF-8'}" href="javascript:hideOtherLanguage({$lang.id_lang|escape:'javascript'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a></li>
                  {/foreach}
              </ul>
          </div>
      {/if}
</div>

{if $languages|count > 1}
      </div>
{/if}
{/foreach}
<div class="clearfix"></div>


<input type="hidden" name="save_trans" value="1">
<div class="panel-footer">
      <div class="panel-footer">
      <button type="submit" class="btn btn-default pull-right">
      <i class="process-icon-save"></i> {l s='Save' mod='webapi'}
      </button>
</div>
   </div>
</form>

</div>
