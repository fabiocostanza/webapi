{*
*  Web Api
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* @author    Fabio C.
* @copyright Copyright 2019 © Fabio C.
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  Fabio C.
* @package   WebApi
*}

<div class="table-responsive-row clearfix">
   <input type="hidden" name="ajax_url" id="ajax_url" value="{$ajax_url|escape:'htmlall':'UTF-8'}">
   <table id="table-webapi" class="table webapi">
      <thead>
         <tr class="nodrag nodrop">
            <th class="">
               <span>
               {l s='ID' mod='webapi'}
               </span>
            </th>
            <th>
               <span>
               {l s='Key' mod='webapi'}
               </span>
            </th>
            <th>
               <span>
               {l s='Status' mod='webapi'}
               </span>
            </th>
            <th>
               <span>
               {l s='Description' mod='webapi'}
               </span>
            </th>
            <th>
               <span>
               <a href="javascript:addNewRecord();">
               <button type="button" class="btn btn-success btn-xs">
               <i class="material-icons" style="padding-top: 3px;">add_circle_outline</i>
               </button>  
               </a>
               <button type="button" class="btn btn-xs update_list" style="    margin-left: 15px;">
               <i class="material-icons" style="padding-top: 3px;">autorenew</i>
               </button>  
               </span>
            </th>
         </tr>
      </thead>
      <tbody>
         {foreach from=$record key=myId item=i}
         <tr class=" odd" id="id_{$i.id_webapi|escape:'htmlall':'UTF-8'}">
            <td>
               {$i.id_webapi|escape:'htmlall':'UTF-8'}
            </td>
            <td>
               {$i.key|escape:'htmlall':'UTF-8'}
            </td>
            <td>
               {if $i.status == '1'}
               <i class="material-icons action-enabled ">check</i>
               {else}
               <i class="material-icons action-disabled">clear</i>
               {/if} 
            </td>
            <td>
               {$i.description|escape:'htmlall':'UTF-8'}
            </td>
            <td>
               <button type="button" id="{$i.id_webapi|escape:'htmlall':'UTF-8'}" key="{$i.key|escape:'htmlall':'UTF-8'}" status="{$i.status|escape:'htmlall':'UTF-8'}" description="{$i.description|escape:'htmlall':'UTF-8'}" class="btn btn-info edit_Form_data">{l s='Edit' mod='webapi'}</button>
               <button type="button" id="{$i.id_webapi|escape:'htmlall':'UTF-8'}" class="btn btn-danger delete_Form_data">{l s='Delete' mod='webapi'}</button>
            </td>
         </tr>
         {/foreach}
      </tbody>
   </table>
</div>
<script type="text/javascript">
   $('.update_list').click(function() {
       var token = $('#token').val();
       var ajax_url = $("#ajax_url").val();
           $.ajax({
              url: ajax_url,
              method: "post",
              data: { action: 'update_list', token: token },
              dataType: "json",
              success: function(data) {
                 $('#update_list_ajax').html(data);
              }
           });
   
     });
   
   
     $('.edit_Form_data').click(function() {
        var id = $(this).attr("id");
        var key = $(this).attr("key");
        var status = $(this).attr("status");
        var description = $(this).attr("description");
        $('#edit_form').show();
        $("#id_webapi").val(id);
        $("#key").val(key);
        
        $("#description").val(description);
        if (status == 1) {
           jQuery("#status_on").attr('checked', true);
        } else {
           jQuery("#status_off").attr('checked', true);
        }
     });
   
     $('.delete_Form_data').click(function() {
         var id = $(this).attr("id");
         var token = $('#token').val();
         var ajax_url = $("#ajax_url").val();
           $.ajax({
              url: ajax_url,
              method: "post",
              data: { token: token,id: id, action: 'deleteRecord' },
              dataType: "json",
              success: function(data) {
               if (data == true) {
                   var ptr = $('#'+id).closest("tr").remove();
               }
              }
           });
   
     });
   
</script>

