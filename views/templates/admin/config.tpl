{*
*  Web Api
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
* @author    Fabio C.
* @copyright Copyright 2019 © Fabio C.
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  Fabio C.
* @package   WebApi
*}

{if $slider_error}
<div class="bootstrap">
   <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {l s='Valid Extentions (jpg,png,jpeg,gif)' mod='webapi'}
   </div>
</div>
{/if}
{if $slider_message} <!-- array HTML -->
<div class="bootstrap">
   <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {foreach from=$slider_message item=key}
      {$key|escape:'htmlall':'UTF-8'} {l s='Successfully Updated' mod='webapi'} <br>
      {/foreach}
   </div>
</div>
{/if}
{if $translations}
<div class="bootstrap">
   <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {l s='Translation Successfully Updated' mod='webapi'} <br>
   </div>
</div>
{/if}
<fieldset>
   <div class="col-lg-2" id="api_id">
      <div class="productTabs">
         <ul class="tab">
            <li class="tab-row">

               <a class="tab-page" id="web_api_link_settings" href="javascript:displayNavTab('settings');">
                  <i style="font-size: xx-small;" class="material-icons">settings</i>
               {l s='Api Settings' mod='webapi'}
               </a>
            </li>
            <li class="tab-row">
               <a class="tab-page" id="web_api_link_key" href="javascript:displayNavTab('key');">
                  <i style="font-size: xx-small;" class="material-icons">vpn_key</i>
               {l s='Key Configuration' mod='webapi'}
               </a>
            </li>
            <li class="tab-row">
               <a class="tab-page" id="web_api_link_images" href="javascript:displayNavTab('images');">
                  <i style="font-size: xx-small;" class="material-icons">burst_mode</i>
               {l s='Images Settings' mod='webapi'}
               </a>
            </li>
            <li class="tab-row">
               <a class="tab-page" id="web_api_link_position" href="javascript:displayNavTab('position');">
                  <i style="font-size: xx-small;" class="material-icons">swap_vert</i>
               {l s='Position Settings' mod='webapi'}
               </a>
            </li>
            <li class="tab-row">
               <a class="tab-page" id="web_api_link_translation" href="javascript:displayNavTab('translation');">
                  <i style="font-size: xx-small;" class="material-icons">translate</i>
               {l s='Translations' mod='webapi'}
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div name="webapi_form" id="webapi_form" class="col-lg-10 panel form-horizontal">
      <div id="webapi_general">
         <h3 class="tab"><img width="24px" src="{$smarty.const.__PS_BASE_URI__|escape:'htmlall':'UTF-8'}modules/webapi/logo.png"/> {l s='Api ' mod='webapi'}</h3>
         <div class="separation"></div>
      </div>
      <input type="hidden" id="currentFormTab" name="currentFormTab" value="settings" />
      <div id="web_api_settings" class="web_api_tab">
         {include file=$settings|escape:'htmlall':'UTF-8'}
      </div>
      <div id="web_api_key" class="web_api_tab">
         {include file=$key|escape:'htmlall':'UTF-8'}
      </div>
      <div id="web_api_position" class="web_api_tab">
         {include file=$position|escape:'htmlall':'UTF-8'}
      </div>
      <div id="web_api_translation" class="web_api_tab">
         {include file=$translation|escape:'htmlall':'UTF-8'}
      </div>
      <div id="web_api_images" class="web_api_tab">
         {include file=$images|escape:'htmlall':'UTF-8'}
      </div>
      

      <!-- /settings -->
      <div class="separation"></div>
      <!-- <div class="panel-footer">
         <button class="btn btn-default pull-right" name="saveConfiguration" type="submit">
             <i class="process-icon-save"></i>
             {l s='Save' mod='webapi'}
         </button>
         </div> -->
   </div>
</fieldset>
<script type="text/javascript">
   var aSelected = [];
   var asInitVals = new Array();
   var all_label = "{l s='All' mod='webapi' js=1}";
   var check_all_label = "{l s='Check All' mod='webapi' js=1}";
   var uncheck_all_label = "{l s='Uncheck All' mod='webapi' js=1}";
   var currentFormTab = "{if isset($smarty.post.currentFormTab)}{$smarty.post.currentFormTab|escape:'htmlall':'UTF-8'}{else}settings{/if}";
   var del_conf_text = "{l s='Are you sure you want to delete?' mod='webapi' js=1}";
   
   $('.web_api_tab').hide();
   $('.tab-page').removeClass('selected');
   $('#web_api_' + currentFormTab).show();
   $('#web_api_link_' + currentFormTab).addClass('selected');
   
   
   
   function displayNavTab(tab) {
       $('.web_api_tab').hide();
       $('.tab-page').removeClass('selected');
       $('#web_api_' + tab).show();
       $('#web_api_link_' + tab).addClass('selected');
       $('#currentFormTab').val(tab);
   }
   
   function htmlEncode(input) {
       return String(input).replace(/&amp;/g, '&');
   }
</script>
{literal}
<style type="text/css">
   /*== PS 1.6 ==*/
   .bootstrap #api_id ul.tab { list-style:none; padding:0; margin:0}
   .bootstrap #api_id ul.tab li a {background-color: white;border: 1px solid #DDDDDD;display: block;margin-bottom: -1px;padding: 10px 15px;}
   .bootstrap #api_id ul.tab li a { display:block; color:#555555; text-decoration:none}
   .bootstrap #api_id ul.tab li a.selected { color:#fff; background:#00AFF0}
   .bootstrap #webapi_form .language_flags { display:none}
</style>
{/literal}
