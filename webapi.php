<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(dirname(__FILE__).'/models/ApiModel.php');
include_once(dirname(__FILE__).'/models/ApiModelTrans.php');
class WebApi extends Module
{
    protected $config_form = false;
    private $tab_parent_class = null;
    private $tab_class  = 'AdminFCApi';
    private $tab_module = 'webapi';
    public function __construct()
    {
        $this->name = 'webapi';
        $this->tab = 'administration';
        $this->version = '2.0';
        $this->author = 'Fabio C.';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = 'efcbaa0e0bf9d6968a3fe596d9ec1659';
        $this->author_address = '0xcC5e76A6182fa47eD831E43d80Cd0985a14BB095';
        parent::__construct();

        $this->displayName = $this->l('App');
        $this->description = $this->l('Servizi API per chiamate Rest.');
        $this->confirmUninstall = $this->l('Are you want to install the module');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        mkdir(_PS_IMG_DIR_.'webapi', 0777, true);
        mkdir(_PS_IMG_DIR_.'webapi/slider', 0777, true);
        mkdir(_PS_IMG_DIR_.'webapi/logo', 0777, true);
        mkdir(_PS_IMG_DIR_.'webapi/banner', 0777, true);
        mkdir(_PS_MODULE_DIR_ . 'webapi/log/WebApi/', 0777, true);
        include(dirname(__FILE__).'/sql/install.php');
        return parent::install() &&
        Configuration::updateValue('WEB_API_THEME_COLOR', '#F3403A') &&
        Configuration::updateValue('WEB_API_ERROR_LOG', 1) &&
        Configuration::updateValue('WEB_API_LOGO', 0) &&
        Configuration::updateValue('WEB_API_POSITION', '1,2,3,4,5,6,7') &&
        Configuration::updateValue('WEB_API_BANNER_FILE_1', 0) &&
        Configuration::updateValue('WEB_API_BANNER_FILE_2', 0) &&
        Configuration::updateValue('WEB_API_BANNER_FILE_3', 0) &&
        Configuration::updateValue('WEB_API_BANNER_FILE_4', 0) &&
        Configuration::updateValue('WEB_API_BANNER_URL_1', '') &&
        Configuration::updateValue('WEB_API_BANNER_URL_2', '') &&
        Configuration::updateValue('WEB_API_BANNER_URL_3', '') &&
        Configuration::updateValue('WEB_API_BANNER_URL_4', '') &&
        Configuration::updateValue('WEB_API_SLIDER1', 0) &&
        Configuration::updateValue('WEB_API_SLIDER2', 0) &&
        Configuration::updateValue('WEB_API_SLIDER3', 0) &&
        Configuration::updateValue('WEB_API_CAT_BOX', 0) &&
        Configuration::updateValue('WEB_API_NEW_PRODUCTS_STATUS', 1) &&
        Configuration::updateValue('WEB_API_POPULAR_PRODUCTS_STATUS', 1) &&
        Configuration::updateValue('WEB_API_ONSALE_PRODUCTS_STATUS', 1) &&
        Configuration::updateValue('WEB_API_ADVANCE_CATEGORY_SLIDER_STATUS', 0) &&
        Configuration::updateValue('WEB_API_BEST_PRODUCTS_STATUS', 1) &&
        Configuration::updateValue('WEB_API_SLIDER_STATUS', 1) &&
        Configuration::updateValue('WEB_API_CAT_SLIDER_STATUS', 0) &&
        Configuration::updateValue('WEB_API_LOGO_STATUS', 1) &&
        Configuration::updateValue('WEB_API_GRID_BANNER_STATUS', 1) &&
        $this->registerHook('ModuleRoutes');
    }

    public function uninstall()
    {
        if (file_exists(_PS_IMG_DIR_.'webapi')) {
            rename(_PS_IMG_DIR_.'webapi', _PS_IMG_DIR_.'webapi'.time());
        }
        if (file_exists(_PS_MODULE_DIR_ . 'webapi/log/WebApi')) {
            rename(_PS_MODULE_DIR_ . 'webapi/log/WebApi', _PS_MODULE_DIR_ . 'webapi/log/WebApi'.time());
        }

        include(dirname(__FILE__).'/sql/uninstall.php');
        return parent::uninstall() &&
        Configuration::deleteByName('WEB_API_THEME_COLOR') &&
        Configuration::deleteByName('WEB_API_SLIDER1') &&
        Configuration::deleteByName('WEB_API_SLIDER2') &&
        Configuration::deleteByName('WEB_API_SLIDER3') &&
        Configuration::deleteByName('WEB_API_BANNER_FILE_1') &&
        Configuration::deleteByName('WEB_API_BANNER_FILE_2') &&
        Configuration::deleteByName('WEB_API_BANNER_FILE_3') &&
        Configuration::deleteByName('WEB_API_BANNER_FILE_4') &&
        Configuration::deleteByName('WEB_API_BANNER_URL_1') &&
        Configuration::deleteByName('WEB_API_BANNER_URL_2') &&
        Configuration::deleteByName('WEB_API_BANNER_URL_3') &&
        Configuration::deleteByName('WEB_API_BANNER_URL_4') &&
        Configuration::deleteByName('WEB_API_LOGO') &&
        Configuration::deleteByName('WEB_API_POSITION') &&
        Configuration::deleteByName('WEB_API_NEW_PRODUCTS_STATUS') &&
        Configuration::deleteByName('WEB_API_POPULAR_PRODUCTS_STATUS') &&
        Configuration::deleteByName('WEB_API_ONSALE_PRODUCTS_STATUS') &&
        Configuration::deleteByName('WEB_API_ADVANCE_CATEGORY_SLIDER_STATUS') &&
        Configuration::deleteByName('WEB_API_BEST_PRODUCTS_STATUS') &&
        Configuration::deleteByName('WEB_API_CAT_BOX') &&
        Configuration::deleteByName('WEB_API_SLIDER_STATUS') &&
        Configuration::deleteByName('WEB_API_CAT_SLIDER_STATUS') &&
        Configuration::deleteByName('WEB_API_LOGO_STATUS') &&
        Configuration::deleteByName('WEB_API_GRID_BANNER_STATUS') &&
        Configuration::deleteByName('WEB_API_ERROR_LOG');
    }

    private function addTab($tab_class, $id_parent)
    {
        $tab = new Tab();
        $tab->class_name = $tab_class;
        $tab->id_parent = $id_parent;
        $tab->module = $this->tab_module;
        $tab->name[(int)(Configuration::get('PS_LANG_DEFAULT'))] = $this->l('Web APi');
        $tab->add();

        $subtab = new Tab();
        $subtab->class_name = 'AdminFCApi';
        $subtab->id_parent = Tab::getIdFromClassName($tab_class);
        $subtab->module = $this->tab_module;
        $subtab->name[(int)(Configuration::get('PS_LANG_DEFAULT'))] = $this->l('Manage Theme');
        $subtab->icon = 'stay_primary_portrait';
        $subtab->add();

        return true;
    }

    private function removeTab($tab_class)
    {
        $idTab = Tab::getIdFromClassName($tab_class);
        if ($idTab != 0) {
            $tab = new Tab($idTab);
            $tab->delete();
            return true;
        }

        $idTab1 = Tab::getIdFromClassName('AdminFCApi');
        if ($idTab1 != 0) {
            $tab = new Tab($idTab1);
            $tab->delete();
            return true;
        }

        return false;
    }

    private function existsTab($tab_class)
    {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT id_tab AS id
            FROM `'._DB_PREFIX_.'tab` t WHERE LOWER(t.`class_name`) = \''.pSQL($tab_class).'\'');
        if (count($result) == 0) {
            return false;
        }
        return true;
    }

    /**
     * Load the configuration form
     */

    public function getContent()
    {
        $slider_message = array();
        $slider_error =0;
        $slider_message = $this->checkPostProcess();

        $category_tree = '';
        $field_values = $this->getConfigurationValues();

        $root = Category::getRootCategory();

        $tree = new HelperTreeCategories('associated-categories-tree', $this->l('Select Slider Categories'));
        $tree->setRootCategory($root->id);
        $tree->setUseCheckBox(true);
        $tree->setUseSearch(true);
        if (!empty($field_values) && !empty($field_values['category'])) {
            $selected_categories = explode(',', $field_values['category']);
            $tree->setSelectedCategories($selected_categories);
        }

        $category_tree = $tree->render();

        $settings = _PS_MODULE_DIR_.'webapi/views/templates/admin/settings.tpl';
        $position = _PS_MODULE_DIR_.'webapi/views/templates/admin/position.tpl';
        $accounts = _PS_MODULE_DIR_.'webapi/views/templates/admin/accounts.tpl';

        $key = _PS_MODULE_DIR_.'webapi/views/templates/admin/key.tpl';
        $translation = _PS_MODULE_DIR_.'webapi/views/templates/admin/translation.tpl';
        $images = _PS_MODULE_DIR_.'webapi/views/templates/admin/images.tpl';

        $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
        $this->context->smarty->assign('base_dir', _PS_BASE_URL_.__PS_BASE_URI__);
        $this->context->smarty->assign('base_dir_ssl', _PS_BASE_URL_SSL_.__PS_BASE_URI__);
        $this->context->smarty->assign('force_ssl', $force_ssl);

        //$slider_status = Configuration::get('WEB_API_SLIDER_STATUS');
        $slider1 = Configuration::get('WEB_API_SLIDER1');
        $slider2 = Configuration::get('WEB_API_SLIDER2');
        $slider3 = Configuration::get('WEB_API_SLIDER3');
        $logo_img = Configuration::get('WEB_API_LOGO');

        $banner_file_1 = Configuration::get('WEB_API_BANNER_FILE_1');
        $banner_file_2 = Configuration::get('WEB_API_BANNER_FILE_2');
        $banner_file_3 = Configuration::get('WEB_API_BANNER_FILE_3');
        $banner_file_4 = Configuration::get('WEB_API_BANNER_FILE_4');

        $banner_url_1 = Configuration::get('WEB_API_BANNER_URL_1');
        $banner_url_2 = Configuration::get('WEB_API_BANNER_URL_2');
        $banner_url_3 = Configuration::get('WEB_API_BANNER_URL_3');
        $banner_url_4 = Configuration::get('WEB_API_BANNER_URL_4');

        $position_list= Configuration::get('WEB_API_POSITION');
        $selected_products = explode(',', $position_list);

        if ($selected_products) {
            $position_one =$selected_products[0];
            $position_two =$selected_products[1];
            $position_three =$selected_products[2];
            $position_four =$selected_products[3];
            $position_five =$selected_products[4];
            $position_six =$selected_products[5];
            $position_seven =$selected_products[6];

            $this->context->smarty->assign('position_one', $position_one);
            $this->context->smarty->assign('position_two', $position_two);
            $this->context->smarty->assign('position_three', $position_three);
            $this->context->smarty->assign('position_four', $position_four);
            $this->context->smarty->assign('position_five', $position_five);
            $this->context->smarty->assign('position_six', $position_six);
            $this->context->smarty->assign('position_seven', $position_seven);
        }

        $this->context->smarty->assign('image_base_url', _PS_BASE_URL_._PS_IMG_.'webapi/slider/');
        $this->context->smarty->assign('banner_base_url', _PS_BASE_URL_._PS_IMG_.'webapi/banner/');
        $this->context->smarty->assign('logo_base_url', _PS_BASE_URL_._PS_IMG_.'webapi/logo/');

        $this->context->smarty->assign(
            'home_cat_image',
            _PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/views/img/empty-img.png'
        );

        $this->context->smarty->assign('category_tree', $category_tree);

        $this->context->smarty->assign('banner_file_1', $banner_file_1);
        $this->context->smarty->assign('banner_file_2', $banner_file_2);
        $this->context->smarty->assign('banner_file_3', $banner_file_3);
        $this->context->smarty->assign('banner_file_4', $banner_file_4);

        $this->context->smarty->assign('banner_url_1', $banner_url_1);
        $this->context->smarty->assign('banner_url_2', $banner_url_2);
        $this->context->smarty->assign('banner_url_3', $banner_url_3);
        $this->context->smarty->assign('banner_url_4', $banner_url_4);

        $this->context->smarty->assign('slider1', $slider1);
        $this->context->smarty->assign('slider2', $slider2);
        $this->context->smarty->assign('slider3', $slider3);
        $this->context->smarty->assign('logo_img', $logo_img);

        //$this->context->smarty->assign('slider_status_on', $slider_status);
        $this->context->smarty->assign('record', $this->getListContent());


        $this->context->smarty->assign('settings', $settings);
        $this->context->smarty->assign('accounts', $accounts);
        $this->context->smarty->assign('position', $position);

        $this->context->smarty->assign('slider_message', $slider_message);
        $this->context->smarty->assign('slider_error', $slider_error);

        $this->context->smarty->assign('key', $key);
        $this->context->smarty->assign('translation', $translation);
        $this->context->smarty->assign('images', $images);
        $ajax_url = $this->context->link->getModuleLink('webapi', 'ajax');
        $this->context->smarty->assign('ajax_url', $ajax_url);

        $coo = new Cookie('psFront');
        $id_employee = (int)$coo->__get('id_employee');
        $token = Tools::getAdminToken('ajax'.(Tab::getIdFromClassName('ajax')).$id_employee);
        $this->context->smarty->assign('token', $token);

        $theme_color = Configuration::get('WEB_API_THEME_COLOR');
        $file = Configuration::get('WEB_API_LOGO');
        $error_log_status_on = Configuration::get('WEB_API_ERROR_LOG');
        $cat_slider_status_on = Configuration::get('WEB_API_CAT_SLIDER_STATUS');
        $slider_status_on = Configuration::get('WEB_API_SLIDER_STATUS');
        $logo_status_on = Configuration::get('WEB_API_LOGO_STATUS');
        $grid_banner_status_on = Configuration::get('WEB_API_GRID_BANNER_STATUS');

        $new_products_status = Configuration::get('WEB_API_NEW_PRODUCTS_STATUS');
        $popular_products_status = Configuration::get('WEB_API_POPULAR_PRODUCTS_STATUS');
        $onsale_products_status = Configuration::get('WEB_API_ONSALE_PRODUCTS_STATUS');
        $advance_category_slider = Configuration::get('WEB_API_ADVANCE_CATEGORY_SLIDER_STATUS');
        $best_products_status = Configuration::get('WEB_API_BEST_PRODUCTS_STATUS');

        $this->context->smarty->assign('new_products_status_on', $new_products_status);
        $this->context->smarty->assign('popular_products_status_on', $popular_products_status);
        $this->context->smarty->assign('onsale_products_status_on', $onsale_products_status);
        $this->context->smarty->assign('advance_category_slider_on', $advance_category_slider);
        $this->context->smarty->assign('best_products_status_on', $best_products_status);
        $this->context->smarty->assign('defaultFormLanguage', (int)$this->context->employee->id_lang);
        $this->context->smarty->assign('languages', $this->context->controller->getLanguages());
        $this->context->smarty->assign('theme_color', $theme_color);
        $this->context->smarty->assign('error_log_status_on', $error_log_status_on);
        $this->context->smarty->assign('cat_slider_status_on', $cat_slider_status_on);
        $this->context->smarty->assign('slider_status_on', $slider_status_on);
        $this->context->smarty->assign('logo_status_on', $logo_status_on);
        $this->context->smarty->assign('grid_banner_status_on', $grid_banner_status_on);
        $this->context->smarty->assign('file', $file);

        $this->assignTranslation();
        return $this->display(__FILE__, 'views/templates/admin/config.tpl');
        //return $this->fetch('module:webapi/views/templates/admin/config.tpl');
    }

    protected function getListContent()
    {
        $record = new ApiModel;
        $results = $record->getList();
        return $results;
    }

    public function getConfigurationValues()
    {
        $conf_values = array(
            'category' => Configuration::get('WEB_API_CAT_BOX')
        );
        return $conf_values;
    }

    protected function initList()
    {

        $this->fields_list = array(
            'id_webapi' => array(
                'title' => $this->l('ID'),
                'width' => 20,
            ),
            'key' => array(
                'title' => $this->l('Key'),
                'width' => 100,
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            ),
            'status' => array(
                'title' => $this->l('Status'),
                'align' => 'text-center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false,
                'filter_key' => 'a!active',
            ),
            'access_get' => array(
                'title' => $this->l('Read Access'),
                'align' => 'text-center',
                'active' => 'access_get',
                'type' => 'bool',
                'orderby' => false,
                'filter_key' => 'a!active',
            ),
            'access_update' => array(
                'title' => $this->l('Update Access'),
                'align' => 'text-center',
                'active' => 'access_update',
                'type' => 'bool',
                'orderby' => false,
                'filter_key' => 'a!active',
            ),
            'access_delete' => array(
                'title' => $this->l('Delete Access'),
                'align' => 'text-center',
                'active' => 'access_delete',
                'type' => 'bool',
                'orderby' => false,
                'filter_key' => 'a!active',
            ),
            'description' => array(
                'title' => $this->l('Description'),
                'align' => 'text-center',
                'type' => 'text',
            ),

        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        //get no of rows from table room in db and display here

        $helper->identifier = 'id_webapi';
        $helper->actions = array('edit','delete');
        $helper->show_toolbar = true;

        $helper->toolbar_btn['new'] = array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.
            '&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new Record'),

        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function initForm()
    {
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
                'legend' => array(
                'title' => $this->l('Web Api'),
                'icon' => 'icon-lock'
                ),
                'input' => array(
                    array(
                    'type' => 'textbutton',
                    'label' => $this->l('Key'),
                    'name' => 'key',
                    'id' => 'code',
                    'required' => true,
                    'hint' => $this->l('Webservice account key.'),
                    'button' => array(
                        'label' => $this->l('Generate!'),
                        'attributes' => array(
                            'onclick' => 'gencode(32)'
                        )
                    )
                    ),
                    array(
                    'type' => 'hidden',
                    'label' => $this->l('ID'),
                    'name' => 'id_webapi',
                    ),
                    array(
                    'type' => 'text',
                    'label' => $this->l('URL'),
                    'name' => 'url_api',
                    'readonly' => true,
                    'placeholder' => $this->context->link->getModuleLink('webapi', 'WebApi'),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow Read'),
                        'name' => 'access_get',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'access_get_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'access_get_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow Add/Update'),
                        'name' => 'access_update',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'access_update_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'access_update_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow Delete'),
                        'name' => 'access_delete',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'access_delete_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'access_delete_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                    'type' => 'textarea',
                    'label' => $this->l('Key description'),
                    'name' => 'description',
                    'rows' => 3,
                    'cols' => 110,
                    'hint' => $this->l('Quick description of the key: who it is for, what permissions it has, etc.'),
                    ),
                    array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'status',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            );

        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = 'webapi';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0),
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitWebapiModule';
        $helper->toolbar_btn = array(
            'save' => array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.
            '&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
            'back' => array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list'),
            ),
        );

        return $helper;
    }

    public function hookModuleRoutes()
    {
        return array(
            'module-'.$this->name.'-WebApi' => array(
                'controller' => 'WebApi',
                'rule' => 'WebApi',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => $this->name,
                ),
            )
        );
        //friendly rule
        // 'rule' => 'WebApi/resource{/:resource}{/:module}{/:name}',
        //         'keywords' => array(
        //             'resource' => array('regexp' => '[a-zA-Z0-9]*', 'param' => 'resource'),
        //             'module' => array('regexp' => 'module'),
        //             'name' => array('regexp' => '[_a-zA-Z0-9_-]+', 'param' => 'name'),
        //         ),
    }

    public function checkPostProcess()
    {
        $this->context->smarty->assign('translations', '0');
        $slider_message = array();
        $img_error = array();
        $slider_error =0;
        $banner_error =0;
        if (isset($_FILES['file'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['file']['size'] / 1000000, 3, PS_ROUND_UP);
            $timestamp = time();
            $file_name = $_FILES['file']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                move_uploaded_file(
                    $_FILES['file']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/slider/slider1_'.$file_name.'.'.$extension
                );
                Configuration::updateValue('WEB_API_SLIDER1', 'slider1_'.$file_name.'.'.$extension);
                //array_push($slider_message, "Slider 1");
                $f_sz = Tools::ps_round($_FILES['file']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $file_name = $_FILES['file']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            } else {
                array_push($img_error, "Valid Extentions jpg','png','jpeg','gif'");
                $slider_error =1;
            }
        }
        if (isset($_FILES['file1'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['file1']['size'] / 1000000, 3, PS_ROUND_UP);
            $timestamp = time();
            $file_name = $_FILES['file1']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                $slider2_error = 0;
                $slider2_error = $slider2_error;
                move_uploaded_file(
                    $_FILES['file1']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/slider/slider2_'.$file_name.'.'.$extension
                );
                Configuration::updateValue('WEB_API_SLIDER2', 'slider2_'.$file_name.'.'.$extension);
                //array_push($slider_message, "Slider 2");
                $f_sz = Tools::ps_round($_FILES['file1']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $file_name = $_FILES['file1']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['file1']['name'], PATHINFO_EXTENSION);
            } else {
                $slider_error =1;
            }
        }
        if (isset($_FILES['file2'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['file2']['size'] / 1000000, 3, PS_ROUND_UP);
            $timestamp = time();
            $file_name = $_FILES['file2']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                $slider3_error = 0;
                $slider3_error = $slider3_error;
                move_uploaded_file(
                    $_FILES['file2']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/slider/slider3_'.$file_name.'.'.$extension
                );
                Configuration::updateValue('WEB_API_SLIDER3', 'slider3_'.$file_name.'.'.$extension);
                //array_push($slider_message, "Slider 3");
                $f_sz = Tools::ps_round($_FILES['file2']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $file_name = $_FILES['file2']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['file2']['name'], PATHINFO_EXTENSION);
            } else {
                $slider_error =1;
                $slider_error = $slider_error;
            }
        }

        if (isset($_FILES['logo'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['logo']['size'] / 1000000, 3, PS_ROUND_UP);
            $timestamp = time();
            $file_name = $_FILES['logo']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                $logo = 0;
                move_uploaded_file(
                    $_FILES['logo']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/logo/logo_'.$file_name.'.'.$extension
                );
                Configuration::updateValue('WEB_API_LOGO', 'logo_'.$file_name.'.'.$extension);
                //array_push($slider_message, "Logo");
                $f_sz = Tools::ps_round($_FILES['logo']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $file_name = $_FILES['logo']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
            } else {
                $logo =1;
                $logo = $logo;
            }
        }

        if (isset($_FILES['banner_file_1'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['banner_file_1']['size'] / 1000000, 3, PS_ROUND_UP);
            $timestamp = time();
            $file_name = $_FILES['banner_file_1']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                move_uploaded_file(
                    $_FILES['banner_file_1']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/banner/banner1_'.$file_name.'.'.$extension
                );
                Configuration::updateValue('WEB_API_BANNER_FILE_1', 'banner1_'.$file_name.'.'.$extension);
                //array_push($slider_message, "Banner 1");
                $f_sz = Tools::ps_round($_FILES['banner_file_1']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $file_name = $_FILES['banner_file_1']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['banner_file_1']['name'], PATHINFO_EXTENSION);
            } else {
                $banner_error =1;
            }
        }

        if (isset($_FILES['banner_file_2'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['banner_file_2']['size'] / 1000000, 3, PS_ROUND_UP);
            $timestamp = time();
            $file_name = $_FILES['banner_file_2']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                move_uploaded_file(
                    $_FILES['banner_file_2']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/banner/banner2_'.$file_name.'.'.$extension
                );
                Configuration::updateValue('WEB_API_BANNER_FILE_2', 'banner2_'.$file_name.'.'.$extension);
                //array_push($slider_message, "Banner 2");
                $f_sz = Tools::ps_round($_FILES['banner_file_2']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $file_name = $_FILES['banner_file_2']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['banner_file_2']['name'], PATHINFO_EXTENSION);
            } else {
                $banner_error =1;
            }
        }

        if (isset($_FILES['banner_file_3'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['banner_file_3']['size'] / 1000000, 3, PS_ROUND_UP);
            $timestamp = time();
            $file_name = $_FILES['banner_file_3']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                move_uploaded_file(
                    $_FILES['banner_file_3']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/banner/banner3_'.$file_name.'.'.$extension
                );
                Configuration::updateValue('WEB_API_BANNER_FILE_3', 'banner3_'.$file_name.'.'.$extension);
                //array_push($slider_message, "Banner 3");
                $f_sz = Tools::ps_round($_FILES['banner_file_3']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $timestamp = $timestamp;
                $file_name = $_FILES['banner_file_3']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['banner_file_3']['name'], PATHINFO_EXTENSION);
            } else {
                $banner_error =1;
            }
        }

        if (isset($_FILES['banner_file_4'])) {
            $allowed = array('jpg','png','jpeg','gif','tiff','psd','raw','ico');
            $f_sz = Tools::ps_round($_FILES['banner_file_4']['size'] / 1000000, 3, PS_ROUND_UP);
            $f_sz = $f_sz;
            $timestamp = time();
            $file_name = $_FILES['banner_file_4']['name'];
            $extension = pathinfo($file_name, PATHINFO_EXTENSION);
            $file_name = pathinfo($file_name, PATHINFO_FILENAME);
            if (in_array(Tools::strtolower($extension), $allowed)) {
                move_uploaded_file(
                    $_FILES['banner_file_4']['tmp_name'],
                    _PS_IMG_DIR_.'webapi/banner/banner4_'.$file_name.'.'.$extension
                );
                Configuration::updateValue(
                    'WEB_API_BANNER_FILE_4',
                    'banner4_'.$file_name.'.'.$extension
                );
                //array_push($slider_message, "Banner 4");
                $f_sz = Tools::ps_round($_FILES['banner_file_4']['size'] / 1000000, 3, PS_ROUND_UP);
                $timestamp = time();
                $file_name = $_FILES['banner_file_4']['name'];
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($_FILES['banner_file_4']['name'], PATHINFO_EXTENSION);
            } else {
                $banner_error =1;
                $banner_error = $banner_error;
            }
        }

        if (Tools::getValue('banner_url_1')) {
            Configuration::updateValue('WEB_API_BANNER_URL_1', Tools::getValue('banner_url_1'));
            //array_push($slider_message, "Banner 1 Url");
        }
        if (Tools::getValue('banner_url_2')) {
            Configuration::updateValue('WEB_API_BANNER_URL_2', Tools::getValue('banner_url_2'));
            //array_push($slider_message, "Banner 2 Url");
        }
        if (Tools::getValue('banner_url_3')) {
            Configuration::updateValue('WEB_API_BANNER_URL_3', Tools::getValue('banner_url_3'));
            //array_push($slider_message, "Banner 3 Url");
        }
        if (Tools::getValue('banner_url_4')) {
            Configuration::updateValue('WEB_API_BANNER_URL_4', Tools::getValue('banner_url_4'));
            //array_push($slider_message, "Banner 4 Url");
        }

        if (Tools::getValue('save_trans')) {
            $this->saveTranslations();
            $this->context->smarty->assign('translations', '1');
        }

        return $slider_message;
    }

    public function saveTranslations()
    {
        $lastid = ApiModelTrans::getLastId();
        $save = 1;
        $save = $save;
        if ($lastid) {
            $id_trns = $lastid[0]['id_trans'];
            $object = new ApiModelTrans($id_trns);
            $save = 0;
        } else {
            $object = new ApiModelTrans();
        }

        foreach (Language::getLanguages() as $lang) {
            $object->home[$lang['id_lang']] =
            Tools::getValue('home_'.(int)$lang['id_lang']);
            $object->categories[$lang['id_lang']] =
            Tools::getValue('categories_'.(int)$lang['id_lang']);
            $object->cart[$lang['id_lang']] =
            Tools::getValue('cart_'.(int)$lang['id_lang']);
            $object->account[$lang['id_lang']] =
            Tools::getValue('account_'.(int)$lang['id_lang']);
            $object->viewAll[$lang['id_lang']] =
            Tools::getValue('viewAll_'.(int)$lang['id_lang']);
            $object->search[$lang['id_lang']] =
            Tools::getValue('search_'.(int)$lang['id_lang']);
            $object->allCategories[$lang['id_lang']] =
            Tools::getValue('allCategories_'.(int)$lang['id_lang']);
            $object->newProducts[$lang['id_lang']] =
            Tools::getValue('newProducts_'.(int)$lang['id_lang']);
            $object->bestSellers[$lang['id_lang']] =
            Tools::getValue('bestSellers_'.(int)$lang['id_lang']);
            $object->popularProducts[$lang['id_lang']] =
            Tools::getValue('popularProducts_'.(int)$lang['id_lang']);
            $object->saleProducts[$lang['id_lang']] =
            Tools::getValue('saleProducts_'.(int)$lang['id_lang']);
            $object->signIn[$lang['id_lang']] =
            Tools::getValue('signIn_'.(int)$lang['id_lang']);
            $object->signOut[$lang['id_lang']] =
            Tools::getValue('signOut_'.(int)$lang['id_lang']);
            $object->profile[$lang['id_lang']] =
            Tools::getValue('profile_'.(int)$lang['id_lang']);
            $object->address[$lang['id_lang']] =
            Tools::getValue('address_'.(int)$lang['id_lang']);
            $object->vouchers[$lang['id_lang']] =
            Tools::getValue('vouchers_'.(int)$lang['id_lang']);
            $object->ordersHistory[$lang['id_lang']] =
            Tools::getValue('ordersHistory_'.(int)$lang['id_lang']);
            $object->currency[$lang['id_lang']] =
            Tools::getValue('currency_'.(int)$lang['id_lang']);
            $object->version[$lang['id_lang']] =
            Tools::getValue('version_'.(int)$lang['id_lang']);
            $object->login[$lang['id_lang']] =
            Tools::getValue('login_'.(int)$lang['id_lang']);
            $object->forgotPassword[$lang['id_lang']] =
            Tools::getValue('forgotPassword_'.(int)$lang['id_lang']);
            $object->signUp[$lang['id_lang']] =
            Tools::getValue('signUp_'.(int)$lang['id_lang']);
            $object->haveNotAnAccount[$lang['id_lang']] =
            Tools::getValue('haveNotAnAccount_'.(int)$lang['id_lang']);
            $object->password[$lang['id_lang']] =
            Tools::getValue('password_'.(int)$lang['id_lang']);
            $object->newPassword[$lang['id_lang']] =
            Tools::getValue('newPassword_'.(int)$lang['id_lang']);
            $object->confirmPassword[$lang['id_lang']] =
            Tools::getValue('confirmPassword_'.(int)$lang['id_lang']);
            $object->submit[$lang['id_lang']] =
            Tools::getValue('submit_'.(int)$lang['id_lang']);
            $object->firstName[$lang['id_lang']] =
            Tools::getValue('firstName_'.(int)$lang['id_lang']);
            $object->lastName[$lang['id_lang']] =
            Tools::getValue('lastName_'.(int)$lang['id_lang']);
            $object->register[$lang['id_lang']] =
            Tools::getValue('register_'.(int)$lang['id_lang']);
            $object->haveAnAccount[$lang['id_lang']] =
            Tools::getValue('haveAnAccount_'.(int)$lang['id_lang']);
            $object->buy[$lang['id_lang']] =
            Tools::getValue('buy_'.(int)$lang['id_lang']);
            $object->shippingAddress[$lang['id_lang']] =
            Tools::getValue('shippingAddress_'.(int)$lang['id_lang']);
            $object->shippingMethods[$lang['id_lang']] =
            Tools::getValue('shippingMethods_'.(int)$lang['id_lang']);
            $object->paymentMethods[$lang['id_lang']] =
            Tools::getValue('paymentMethods_'.(int)$lang['id_lang']);
            $object->placeOrder[$lang['id_lang']] =
            Tools::getValue('placeOrder_'.(int)$lang['id_lang']);
            $object->confirmation[$lang['id_lang']] =
            Tools::getValue('confirmation_'.(int)$lang['id_lang']);
            $object->addToCart[$lang['id_lang']] =
            Tools::getValue('addToCart_'.(int)$lang['id_lang']);
            $object->outOfStock[$lang['id_lang']] =
            Tools::getValue('outOfStock_'.(int)$lang['id_lang']);
            $object->quantity[$lang['id_lang']] =
            Tools::getValue('quantity_'.(int)$lang['id_lang']);
            $object->contactName[$lang['id_lang']] =
            Tools::getValue('contactName_'.(int)$lang['id_lang']);
            $object->gender[$lang['id_lang']] =
            Tools::getValue('gender_'.(int)$lang['id_lang']);
            $object->dateOfBirth[$lang['id_lang']] =
            Tools::getValue('dateOfBirth_'.(int)$lang['id_lang']);
            $object->myProfile[$lang['id_lang']] =
            Tools::getValue('myProfile_'.(int)$lang['id_lang']);
            $object->dateOfBirth[$lang['id_lang']] =
            Tools::getValue('dateOfBirth_'.(int)$lang['id_lang']);
            $object->addNewAddress[$lang['id_lang']] =
            Tools::getValue('addNewAddress_'.(int)$lang['id_lang']);
            $object->alias[$lang['id_lang']] =
            Tools::getValue('alias_'.(int)$lang['id_lang']);
            $object->company[$lang['id_lang']] =
            Tools::getValue('company_'.(int)$lang['id_lang']);
            $object->vatNumber[$lang['id_lang']] =
            Tools::getValue('vatNumber_'.(int)$lang['id_lang']);
            $object->addressComplement[$lang['id_lang']] =
            Tools::getValue('addressComplement_'.(int)$lang['id_lang']);
            $object->zipPostalCode[$lang['id_lang']] =
            Tools::getValue('zipPostalCode_'.(int)$lang['id_lang']);
            $object->phone[$lang['id_lang']] =
            Tools::getValue('phone_'.(int)$lang['id_lang']);
            $object->city[$lang['id_lang']] =
            Tools::getValue('city_'.(int)$lang['id_lang']);
            $object->country[$lang['id_lang']] =
            Tools::getValue('country_'.(int)$lang['id_lang']);
            $object->myOrders[$lang['id_lang']] =
            Tools::getValue('myOrders_'.(int)$lang['id_lang']);
            $object->order[$lang['id_lang']] =
            Tools::getValue('order_'.(int)$lang['id_lang']);
            $object->orderDetails[$lang['id_lang']] =
            Tools::getValue('orderDetails_'.(int)$lang['id_lang']);
            $object->billingAddress[$lang['id_lang']] =
            Tools::getValue('billingAddress_'.(int)$lang['id_lang']);
            $object->update[$lang['id_lang']] =
            Tools::getValue('update_'.(int)$lang['id_lang']);
            $object->continueShopping[$lang['id_lang']] =
            Tools::getValue('continueShopping_'.(int)$lang['id_lang']);
            $object->proceedToCheckout[$lang['id_lang']] =
            Tools::getValue('proceedToCheckout_'.(int)$lang['id_lang']);
            $object->close[$lang['id_lang']] =
            Tools::getValue('close_'.(int)$lang['id_lang']);
            $object->email[$lang['id_lang']] =
            Tools::getValue('email_'.(int)$lang['id_lang']);
            $object->languageName[$lang['id_lang']] =
            Tools::getValue('languageName_'.(int)$lang['id_lang']);
            $object->voucherCode[$lang['id_lang']] =
            Tools::getValue('voucherCode_'.(int)$lang['id_lang']);
            $object->total_tax_incl[$lang['id_lang']] =
            Tools::getValue('total_tax_incl_'.(int)$lang['id_lang']);
            $object->in_stock[$lang['id_lang']] =
            Tools::getValue('in_stock_'.(int)$lang['id_lang']);
            $object->total_vouchers[$lang['id_lang']] =
            Tools::getValue('total_vouchers_'.(int)$lang['id_lang']);
            $object->total_shipping[$lang['id_lang']] =
            Tools::getValue('total_shipping_'.(int)$lang['id_lang']);
            $object->total_products[$lang['id_lang']] =
            Tools::getValue('total_products_'.(int)$lang['id_lang']);
            $object->order_is_confirmed[$lang['id_lang']] =
            Tools::getValue('order_is_confirmed_'.(int)$lang['id_lang']);
            $object->tax_incl[$lang['id_lang']] =
            Tools::getValue('tax_incl_'.(int)$lang['id_lang']);
            $object->eror[$lang['id_lang']] =
            Tools::getValue('eror_'.(int)$lang['id_lang']);
            $object->incorrect[$lang['id_lang']] =
            Tools::getValue('incorrect_'.(int)$lang['id_lang']);
            $object->please_enter_your[$lang['id_lang']] =
            Tools::getValue('please_enter_your_'.(int)$lang['id_lang']);
            $object->something_went_wrong[$lang['id_lang']] =
            Tools::getValue('something_went_wrong_'.(int)$lang['id_lang']);
            $object->try_again[$lang['id_lang']] =
            Tools::getValue('try_again_'.(int)$lang['id_lang']);
            $object->please_select[$lang['id_lang']] =
            Tools::getValue('please_select_'.(int)$lang['id_lang']);
            $object->congratulations[$lang['id_lang']] =
            Tools::getValue('congratulations_'.(int)$lang['id_lang']);
            $object->added_into_cart[$lang['id_lang']] =
            Tools::getValue('added_into_cart_'.(int)$lang['id_lang']);
            $object->order_reference[$lang['id_lang']] =
            Tools::getValue('order_reference_'.(int)$lang['id_lang']);
            $object->order_status[$lang['id_lang']] =
            Tools::getValue('order_status_'.(int)$lang['id_lang']);
            $object->check_back_later[$lang['id_lang']] =
            Tools::getValue('check_back_later_'.(int)$lang['id_lang']);
            $object->there_are_no_item[$lang['id_lang']] =
            Tools::getValue('there_are_no_item_'.(int)$lang['id_lang']);
            $object->session_expired[$lang['id_lang']] =
            Tools::getValue('session_expired_'.(int)$lang['id_lang']);
            $object->account_registered[$lang['id_lang']] =
            Tools::getValue('account_registered_'.(int)$lang['id_lang']);
            $object->reset_mail_send[$lang['id_lang']] =
            Tools::getValue('reset_mail_send_'.(int)$lang['id_lang']);
            $object->placed_on[$lang['id_lang']] =
            Tools::getValue('placed_on_'.(int)$lang['id_lang']);
            $object->valid_till[$lang['id_lang']] =
            Tools::getValue('valid_till_'.(int)$lang['id_lang']);
            $object->voucher_value[$lang['id_lang']] =
            Tools::getValue('voucher_value_'.(int)$lang['id_lang']);
            $object->minimum_purchase[$lang['id_lang']] =
            Tools::getValue('minimum_purchase_'.(int)$lang['id_lang']);
            $object->noof_vouchers[$lang['id_lang']] =
            Tools::getValue('noof_vouchers_'.(int)$lang['id_lang']);
        }
        $object->active = 1;
        if ($save = 0) {
            $object->update();
        } else {
            $object->save();
        }
    }

    public function assignTranslation()
    {
        $lastid = ApiModelTrans::getLastId();
        if ($lastid) {
            $id_trns = $lastid[0]['id_trans'];
            $trans_record = new ApiModelTrans($id_trns);


            $this->context->smarty->assign(
                array(
                    'home' => $trans_record->home,
                    'account' => $trans_record->account,
                    'cart' => $trans_record->cart,
                    'categories' => $trans_record->categories,
                    'viewAll' => $trans_record->viewAll,
                    'search' => $trans_record->search,
                    'allCategories' => $trans_record->allCategories,
                    'newProducts' => $trans_record->newProducts,
                    'bestSellers' => $trans_record->bestSellers,
                    'popularProducts' => $trans_record->popularProducts,
                    'saleProducts' => $trans_record->saleProducts,
                    'signIn' => $trans_record->signIn,
                    'signOut' => $trans_record->signOut,
                    'profile' => $trans_record->profile,
                    'address' => $trans_record->address,
                    'vouchers' => $trans_record->vouchers,
                    'ordersHistory' => $trans_record->ordersHistory,
                    'currency' => $trans_record->currency,
                    'version' => $trans_record->version,
                    'login' => $trans_record->login,
                    'forgotPassword' => $trans_record->forgotPassword,
                    'signUp' => $trans_record->signUp,
                    'haveNotAnAccount' => $trans_record->haveNotAnAccount,
                    'password' => $trans_record->password,
                    'newPassword' => $trans_record->newPassword,
                    'confirmPassword' => $trans_record->confirmPassword,
                    'submit' => $trans_record->submit,
                    'firstName' => $trans_record->firstName,
                    'lastName' => $trans_record->lastName,
                    'register' => $trans_record->register,
                    'haveAnAccount' => $trans_record->haveAnAccount,
                    'buy' => $trans_record->buy,
                    'shippingAddress' => $trans_record->shippingAddress,
                    'shippingMethods' => $trans_record->shippingMethods,
                    'paymentMethods' => $trans_record->paymentMethods,
                    'placeOrder' => $trans_record->placeOrder,
                    'confirmation' => $trans_record->confirmation,
                    'addToCart' => $trans_record->addToCart,
                    'outOfStock' => $trans_record->outOfStock,
                    'quantity' => $trans_record->quantity,
                    'contactName' => $trans_record->contactName,
                    'country' => $trans_record->country,
                    'gender' => $trans_record->gender,
                    'dateOfBirth' => $trans_record->dateOfBirth,
                    'myProfile' => $trans_record->myProfile,
                    'update' => $trans_record->update,
                    'addNewAddress' => $trans_record->addNewAddress,
                    'alias' => $trans_record->alias,
                    'company' => $trans_record->company,
                    'vatNumber' => $trans_record->vatNumber,
                    'addressComplement' => $trans_record->addressComplement,
                    'zipPostalCode' => $trans_record->zipPostalCode,
                    'phone' => $trans_record->phone,
                    'city' => $trans_record->city,
                    'phone' => $trans_record->phone,
                    'myOrders' => $trans_record->myOrders,
                    'order' => $trans_record->order,
                    'orderDetails' => $trans_record->orderDetails,
                    'billingAddress' => $trans_record->myOrders,
                    'proceedToCheckout' => $trans_record->proceedToCheckout,
                    'continueShopping' => $trans_record->continueShopping,
                    'close' => $trans_record->close,
                    'languageName' => $trans_record->languageName,
                    'email' => $trans_record->email,
                    'voucherCode' => $trans_record->voucherCode,
                    'total_tax_incl' => $trans_record->total_tax_incl,
                    'in_stock' => $trans_record->in_stock,
                    'total_vouchers' => $trans_record->total_vouchers,
                    'total_shipping' => $trans_record->total_shipping,
                    'total_products' => $trans_record->total_products,
                    'order_is_confirmed' => $trans_record->order_is_confirmed,
                    'tax_incl' => $trans_record->tax_incl,
                    'eror' => $trans_record->eror,
                    'incorrect' => $trans_record->incorrect,
                    'please_enter_your' => $trans_record->please_enter_your,
                    'something_went_wrong' => $trans_record->something_went_wrong,
                    'try_again' => $trans_record->try_again,
                    'please_select' => $trans_record->please_select,
                    'congratulations' => $trans_record->congratulations,
                    'added_into_cart' => $trans_record->added_into_cart,
                    'order_reference' => $trans_record->order_reference,
                    'order_status' => $trans_record->order_status,
                    'check_back_later' => $trans_record->check_back_later,
                    'there_are_no_item' => $trans_record->there_are_no_item,
                    'session_expired' => $trans_record->session_expired,
                    'account_registered' => $trans_record->account_registered,
                    'reset_mail_send' => $trans_record->reset_mail_send,
                    'placed_on' => $trans_record->placed_on,
                    'valid_till' => $trans_record->valid_till,
                    'voucher_value' => $trans_record->voucher_value,
                    'minimum_purchase' => $trans_record->minimum_purchase,
                    'noof_vouchers' => $trans_record->noof_vouchers,

                )
            );
        }
    }
}
